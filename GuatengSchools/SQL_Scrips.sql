--Creating Index
CREATE INDEX idx_official_institution_name ON barcelona.institution(official_institution_name);
CREATE INDEX idx_ref_number ON barcelona.reported_institutions(ref_number);
COMMIT;
SHOW INDEXES FROM barcelona.institution;