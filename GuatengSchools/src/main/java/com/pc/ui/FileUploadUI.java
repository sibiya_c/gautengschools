package com.pc.ui;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.stereotype.Component;

import com.pc.constants.AppConstants;
import com.pc.entities.ImageModel;
import com.pc.framework.AbstractUI;

@Component("fileUploadUI")
@ViewScoped
public class FileUploadUI  extends AbstractUI {


	@PostConstruct
	public void init() {
		
	}
	
	
	public StreamedContent getImageStreamedContent(ImageModel imageModel) throws Exception {
		FacesContext context = FacesContext.getCurrentInstance(); 
		if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			return new DefaultStreamedContent();
		} else { 
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			byte[] image = null;
			try {
				image = imageModel.getPic();
			} catch (Exception e) { 
				displayErrorMssg(e.getMessage());
			} 
			return new DefaultStreamedContent(new ByteArrayInputStream(image),imageModel.getType().trim()); 
		}
	}

	
}
