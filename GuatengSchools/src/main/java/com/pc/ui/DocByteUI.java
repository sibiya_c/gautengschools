package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;
import com.pc.dao.EntityDAOFacade;

import com.pc.entities.DocByte;
import com.pc.framework.AbstractUI;
import com.pc.service.DocByteService;

@Component("docByteUI")
@ViewScoped
public class DocByteUI extends AbstractUI{

	@Autowired
	DocByteService docByteService;
	private ArrayList<DocByte> docByteList;
	private DocByte docByte;
	private LazyDataModel<DocByte> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		docByte = new DocByte();
		loadDocByteInfo();
	}

	public void saveDocByte()
	{
		try {
			docByteService.saveDocByte(docByte);
			displayInfoMssg("Update Successful...!!");
			loadDocByteInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteDocByte()
	{
		try {
			docByteService.deleteDocByte(docByte);
			displayWarningMssg("Update Successful...!!");
			loadDocByteInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<DocByte> findAllDocByte()
	{
		List<DocByte> list=new ArrayList<>();
		try {
			list= docByteService.findAllDocByte();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<DocByte> findAllDocBytePageable()
	{
		Pageable p=null;
		try {
			return docByteService.findAllDocByte(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<DocByte> findAllDocByteSort()
	{
		Sort s=null;
		List<DocByte> list=new ArrayList<>();
		try {
			list =docByteService.findAllDocByte(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		docByte = new DocByte();
	}
	
	public void loadDocByteInfo()
	{
		 dataModel = new LazyDataModel<DocByte>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<DocByte> list = new  ArrayList<DocByte>();
			   
			   @Override 
			   public List<DocByte> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<DocByte>) entityDAOFacade.getResultList(DocByte.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,DocByte.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(DocByte obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public DocByte getRowData(String rowKey) {
			        for(DocByte obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	public DocByte getDocByte() {
		return docByte;
	}

	public void setDocByte(DocByte docByte) {
		this.docByte = docByte;
	}
	
	public ArrayList<DocByte> getDocByteList() {
		return docByteList;
	}

	public void setDocByteList(ArrayList<DocByte> docByteList) {
		this.docByteList =docByteList;
	}
	public LazyDataModel<DocByte> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<DocByte> dataModel) {
		this.dataModel = dataModel;
	}
	

}
