package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;
import com.pc.dao.EntityDAOFacade;

import com.pc.entities.TestAU;
import com.pc.framework.AbstractUI;
import com.pc.service.TestAUService;

@Component("testAUUI")
@ViewScoped
public class TestAUUI extends AbstractUI{

	@Autowired
	TestAUService testAUService;
	private ArrayList<TestAU> testAUList;
	private TestAU testAU;
	private LazyDataModel<TestAU> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		testAU = new TestAU();
		loadTestAUInfo();
	}

	public void saveTestAU()
	{
		try {
			testAUService.saveTestAU(testAU);
			displayInfoMssg("Update Successful...!!");
			loadTestAUInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteTestAU()
	{
		try {
			testAUService.deleteTestAU(testAU);
			displayWarningMssg("Update Successful...!!");
			loadTestAUInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<TestAU> findAllTestAU()
	{
		List<TestAU> list=new ArrayList<>();
		try {
			list= testAUService.findAllTestAU();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<TestAU> findAllTestAUPageable()
	{
		Pageable p=null;
		try {
			return testAUService.findAllTestAU(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TestAU> findAllTestAUSort()
	{
		Sort s=null;
		List<TestAU> list=new ArrayList<>();
		try {
			list =testAUService.findAllTestAU(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		testAU = new TestAU();
	}
	
	public void loadTestAUInfo()
	{
		 dataModel = new LazyDataModel<TestAU>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<TestAU> list = new  ArrayList<TestAU>();
			   
			   @Override 
			   public List<TestAU> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<TestAU>) entityDAOFacade.getResultList(TestAU.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,TestAU.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(TestAU obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public TestAU getRowData(String rowKey) {
			        for(TestAU obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	public TestAU getTestAU() {
		return testAU;
	}

	public void setTestAU(TestAU testAU) {
		this.testAU = testAU;
	}
	
	public ArrayList<TestAU> getTestAUList() {
		return testAUList;
	}

	public void setTestAUList(ArrayList<TestAU> testAUList) {
		this.testAUList =testAUList;
	}
	public LazyDataModel<TestAU> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<TestAU> dataModel) {
		this.dataModel = dataModel;
	}
	

}
