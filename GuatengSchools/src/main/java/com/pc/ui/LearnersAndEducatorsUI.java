package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;
import com.pc.dao.EntityDAOFacade;

import com.pc.entities.LearnersAndEducators;
import com.pc.framework.AbstractUI;
import com.pc.service.LearnersAndEducatorsService;

@Component("learnersAndEducatorsUI")
@ViewScoped
public class LearnersAndEducatorsUI extends AbstractUI{

	@Autowired
	LearnersAndEducatorsService learnersAndEducatorsService;
	private ArrayList<LearnersAndEducators> learnersAndEducatorsList;
	private LearnersAndEducators learnersAndEducators;
	private LazyDataModel<LearnersAndEducators> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		learnersAndEducators = new LearnersAndEducators();
		loadLearnersAndEducatorsInfo();
	}

	public void saveLearnersAndEducators()
	{
		try {
			learnersAndEducatorsService.saveLearnersAndEducators(learnersAndEducators);
			displayInfoMssg("Update Successful...!!");
			loadLearnersAndEducatorsInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteLearnersAndEducators()
	{
		try {
			learnersAndEducatorsService.deleteLearnersAndEducators(learnersAndEducators);
			displayWarningMssg("Update Successful...!!");
			loadLearnersAndEducatorsInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<LearnersAndEducators> findAllLearnersAndEducators()
	{
		List<LearnersAndEducators> list=new ArrayList<>();
		try {
			list= learnersAndEducatorsService.findAllLearnersAndEducators();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<LearnersAndEducators> findAllLearnersAndEducatorsPageable()
	{
		Pageable p=null;
		try {
			return learnersAndEducatorsService.findAllLearnersAndEducators(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<LearnersAndEducators> findAllLearnersAndEducatorsSort()
	{
		Sort s=null;
		List<LearnersAndEducators> list=new ArrayList<>();
		try {
			list =learnersAndEducatorsService.findAllLearnersAndEducators(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		learnersAndEducators = new LearnersAndEducators();
	}
	
	public void loadLearnersAndEducatorsInfo()
	{
		 dataModel = new LazyDataModel<LearnersAndEducators>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<LearnersAndEducators> list = new  ArrayList<LearnersAndEducators>();
			   
			   @Override 
			   public List<LearnersAndEducators> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<LearnersAndEducators>) entityDAOFacade.getResultList(LearnersAndEducators.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,LearnersAndEducators.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(LearnersAndEducators obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public LearnersAndEducators getRowData(String rowKey) {
			        for(LearnersAndEducators obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	public LearnersAndEducators getLearnersAndEducators() {
		return learnersAndEducators;
	}

	public void setLearnersAndEducators(LearnersAndEducators learnersAndEducators) {
		this.learnersAndEducators = learnersAndEducators;
	}
	
	public ArrayList<LearnersAndEducators> getLearnersAndEducatorsList() {
		return learnersAndEducatorsList;
	}

	public void setLearnersAndEducatorsList(ArrayList<LearnersAndEducators> learnersAndEducatorsList) {
		this.learnersAndEducatorsList =learnersAndEducatorsList;
	}
	public LazyDataModel<LearnersAndEducators> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<LearnersAndEducators> dataModel) {
		this.dataModel = dataModel;
	}
	

}
