package com.pc.ui;

import java.awt.Event;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.pc.entities.User;
import com.pc.entities.UserRole;
import com.pc.framework.AbstractUI;
import com.pc.service.UserRoleService;
import com.pc.service.UserService;

@Component
@ManagedBean(name="userUI")
@ViewScoped
public class UserUI extends AbstractUI{

	@Autowired
	UserService userService;
	
	@Autowired
	UserRoleService userRoleService;

	private User user;
	private StreamedContent profileImg;
	private ArrayList<User> userList;

	
	private boolean registeredSuccessful;
	 

	@PostConstruct
	public void init() {
		registeredSuccessful=false;
		user = new User();
		userList=(ArrayList<User>) findAllUser();
	}

	public void saveUser() {
		try {
			userService.saveUser(user);
			displayInfoMssg("User Registered Successful..!!");
			registeredSuccessful=true;
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void registeUser() {
		try {
			if(!checkRsaId(user.getRsaId())) {
				throw new Exception("Invalid RSA ID number");
			}else if(userService.findByEmail(user.getEmail()) != null) {
				//displayWarningMssg("Email Address aready exist, please provide a unique email");
				throw new Exception("Email Address aready exist, please provide a unique email");
			}else if(userService.getUserByRsaId(user.getRsaId()) != null) {
				//displayWarningMssg("ID number already exist, please provide a unique ID number");
				throw new Exception("ID number already exist, please provide a unique ID number");
			}else {
				checkPassword(user.getPassword(), user);
				userService.saveUser(user);
				displayInfoMssg("User Registered Successful..!!");
				reset();
			}
			
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteUser()
	{
		try {
			userService.deleteUser(user);
			displayInfoMssg("User deleted Successful..!!");
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<User> findAllUser()
	{
		List<User> list=new ArrayList<>();
		try {
			list= userService.findAllUser();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<User> findAllUserPageable()
	{
		Pageable p=null;
		try {
			return userService.findAllUser(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	public List<User> findAllUserSort()
	{
		Sort s=null;
		List<User> list=new ArrayList<>();
		try {
			list =userService.findAllUser(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		user = new User();
	}
	
	
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public void handleFileUpload(FileUploadEvent event) {
		profileImg=new DefaultStreamedContent(new ByteArrayInputStream(event.getFile().getContents()), "png",event.getFile().getFileName().trim());
		
		displayInfoMssg("uploade completed...!!! Content Type: "+event.getFile().getContentType()+" File name: "+event.getFile().getFileName());
		//	return new DefaultStreamedContent(new ByteArrayInputStream(image.getSecurityPic()), image.getContentType().trim(), image.getOriginalFname().trim());
		
	}

	public StreamedContent getProfileImg() {
		return profileImg;
	}

	public void setProfileImg(StreamedContent profileImg) {
		this.profileImg = profileImg;
	}

	public ArrayList<User> getUserList() {
		return userList;
	}

	public void setUserList(ArrayList<User> userList) {
		this.userList = userList;
	}

	public boolean isRegisteredSuccessful() {
		return registeredSuccessful;
	}

	public void setRegisteredSuccessful(boolean registeredSuccessful) {
		this.registeredSuccessful = registeredSuccessful;
	}
	
	/**
	 * Check rsa id.
	 *
	 * @param idVal
	 *            the id val
	 * @return true, if successful
	 */
	public static boolean checkRsaId(String idVal) {
		if (idVal == null || (idVal != null && idVal.trim().length() == 0)) return true;
		idVal = idVal.trim();
		if (idVal.length() < 13) return false;
		int checkDigit = ((Integer.valueOf("" + (idVal.charAt(idVal.length() - 1)))).intValue());
		String odd = "0";
		String even = "";
		int evenResult = 0;
		int result;
		for (int c = 1; c <= idVal.length(); c++) {
			if (c % 2 == 0) {
				even += idVal.charAt(c - 1);
			} else {
				if (c == idVal.length()) {
					continue;
				} else {
					odd = "" + (Integer.valueOf("" + odd).intValue() + Integer.valueOf("" + (idVal.charAt(c - 1))));
				}
			}
		}
		String evenS = "" + (Integer.valueOf(even) * 2);
		for (int r = 1; r <= evenS.length(); r++) {
			evenResult += Integer.valueOf("" + evenS.charAt(r - 1));
		}
		result = (Integer.valueOf(odd) + Integer.valueOf(evenResult));
		String resultS = "" + result;
		resultS = "" + (10 - (Integer.valueOf("" + (resultS.charAt(resultS.length() - 1)))).intValue());
		if (resultS.length() > 1) {
			resultS = "" + resultS.charAt(resultS.length() - 1);
		}
		if (Integer.valueOf(resultS) != checkDigit) {
			return false;
		} else {
			return true;
		}
	}
	
	public static void checkPassword(String newPassword, User u) throws Exception {
		if(newPassword !=null && newPassword.isEmpty()==false && !newPassword.equals(""))
		{
			if (newPassword.equals(u.getName()) || newPassword.equals(u.getSurname())) throw new Exception("Password cannot be your Firstname or Surname.");
	
			if (newPassword.length() < 8 || newPassword.length() < 8) throw new Exception("Password must be 8 characters long.");
	
			String regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}";
			// Create a Pattern object
			Pattern r = Pattern.compile(regex);
			Matcher m = r.matcher(newPassword);
			if (!m.find()) throw new Exception("Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character");
		}
	}



	

}
