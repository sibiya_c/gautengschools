package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.Specialisation;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.SpecialisationService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("specialisationUI")
@ViewScoped
public class SpecialisationUI extends AbstractUI{

	@Autowired
	SpecialisationService specialisationService;
	private ArrayList<Specialisation> specialisationList;
	private Specialisation specialisation;
	private LazyDataModel<Specialisation> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		specialisation = new Specialisation();
		loadSpecialisationInfo();
	}

	public void saveSpecialisation()
	{
		try {
			specialisationService.saveSpecialisation(specialisation);
			displayInfoMssg("Update Successful...!!");
			loadSpecialisationInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteSpecialisation()
	{
		try {
			specialisationService.deleteSpecialisation(specialisation);
			displayWarningMssg("Update Successful...!!");
			loadSpecialisationInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Specialisation> findAllSpecialisation()
	{
		List<Specialisation> list=new ArrayList<>();
		try {
			list= specialisationService.findAllSpecialisation();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Specialisation> findAllSpecialisationPageable()
	{
		Pageable p=null;
		try {
			return specialisationService.findAllSpecialisation(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Specialisation> findAllSpecialisationSort()
	{
		Sort s=null;
		List<Specialisation> list=new ArrayList<>();
		try {
			list =specialisationService.findAllSpecialisation(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		specialisation = new Specialisation();
	}
	
	public void loadSpecialisationInfo()
	{
		 dataModel = new LazyDataModel<Specialisation>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<Specialisation> list = new  ArrayList<Specialisation>();
			   
			   @Override 
			   public List<Specialisation> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<Specialisation>) entityDAOFacade.getResultList(Specialisation.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,Specialisation.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(Specialisation obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public Specialisation getRowData(String rowKey) {
			        for(Specialisation obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public Specialisation getSpecialisation() {
		return specialisation;
	}

	public void setSpecialisation(Specialisation specialisation) {
		this.specialisation = specialisation;
	}
	
	public ArrayList<Specialisation> getSpecialisationList() {
		return specialisationList;
	}

	public void setSpecialisationList(ArrayList<Specialisation> specialisationList) {
		this.specialisationList =specialisationList;
	}
	
	public LazyDataModel<Specialisation> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Specialisation> dataModel) {
		this.dataModel = dataModel;
	}

}
