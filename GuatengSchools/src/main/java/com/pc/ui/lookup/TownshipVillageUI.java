package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.TownshipVillage;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.TownshipVillageService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("townshipVillageUI")
@ViewScoped
public class TownshipVillageUI extends AbstractUI{

	@Autowired
	TownshipVillageService townshipVillageService;
	private ArrayList<TownshipVillage> townshipVillageList;
	private TownshipVillage townshipVillage;
	private LazyDataModel<TownshipVillage> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		townshipVillage = new TownshipVillage();
		loadTownshipVillageInfo();
	}

	public void saveTownshipVillage()
	{
		try {
			townshipVillageService.saveTownshipVillage(townshipVillage);
			displayInfoMssg("Update Successful...!!");
			loadTownshipVillageInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteTownshipVillage()
	{
		try {
			townshipVillageService.deleteTownshipVillage(townshipVillage);
			displayWarningMssg("Update Successful...!!");
			loadTownshipVillageInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<TownshipVillage> findAllTownshipVillage()
	{
		List<TownshipVillage> list=new ArrayList<>();
		try {
			list= townshipVillageService.findAllTownshipVillage();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<TownshipVillage> findAllTownshipVillagePageable()
	{
		Pageable p=null;
		try {
			return townshipVillageService.findAllTownshipVillage(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TownshipVillage> findAllTownshipVillageSort()
	{
		Sort s=null;
		List<TownshipVillage> list=new ArrayList<>();
		try {
			list =townshipVillageService.findAllTownshipVillage(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		townshipVillage = new TownshipVillage();
	}
	
	public void loadTownshipVillageInfo()
	{
		 dataModel = new LazyDataModel<TownshipVillage>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<TownshipVillage> list = new  ArrayList<TownshipVillage>();
			   
			   @Override 
			   public List<TownshipVillage> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<TownshipVillage>) entityDAOFacade.getResultList(TownshipVillage.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,TownshipVillage.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(TownshipVillage obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public TownshipVillage getRowData(String rowKey) {
			        for(TownshipVillage obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public TownshipVillage getTownshipVillage() {
		return townshipVillage;
	}

	public void setTownshipVillage(TownshipVillage townshipVillage) {
		this.townshipVillage = townshipVillage;
	}
	
	public ArrayList<TownshipVillage> getTownshipVillageList() {
		return townshipVillageList;
	}

	public void setTownshipVillageList(ArrayList<TownshipVillage> townshipVillageList) {
		this.townshipVillageList =townshipVillageList;
	}
	
	public LazyDataModel<TownshipVillage> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<TownshipVillage> dataModel) {
		this.dataModel = dataModel;
	}

}
