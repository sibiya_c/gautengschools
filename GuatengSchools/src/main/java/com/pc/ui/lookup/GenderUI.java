package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.Gender;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.GenderService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("genderUI")
@ViewScoped
public class GenderUI extends AbstractUI{

	@Autowired
	GenderService genderService;
	private ArrayList<Gender> genderList;
	private Gender gender;
	private LazyDataModel<Gender> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		gender = new Gender();
		loadGenderInfo();
	}

	public void saveGender()
	{
		try {
			genderService.saveGender(gender);
			displayInfoMssg("Update Successful...!!");
			loadGenderInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteGender()
	{
		try {
			genderService.deleteGender(gender);
			displayWarningMssg("Update Successful...!!");
			loadGenderInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Gender> findAllGender()
	{
		List<Gender> list=new ArrayList<>();
		try {
			list= genderService.findAllGender();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Gender> findAllGenderPageable()
	{
		Pageable p=null;
		try {
			return genderService.findAllGender(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Gender> findAllGenderSort()
	{
		Sort s=null;
		List<Gender> list=new ArrayList<>();
		try {
			list =genderService.findAllGender(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		gender = new Gender();
	}
	
	public void loadGenderInfo()
	{
		 dataModel = new LazyDataModel<Gender>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<Gender> list = new  ArrayList<Gender>();
			   
			   @Override 
			   public List<Gender> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<Gender>) entityDAOFacade.getResultList(Gender.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,Gender.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(Gender obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public Gender getRowData(String rowKey) {
			        for(Gender obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}
	
	public ArrayList<Gender> getGenderList() {
		return genderList;
	}

	public void setGenderList(ArrayList<Gender> genderList) {
		this.genderList =genderList;
	}
	
	public LazyDataModel<Gender> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Gender> dataModel) {
		this.dataModel = dataModel;
	}

}
