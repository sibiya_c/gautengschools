package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.TypeDoE;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.TypeDoEService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("typeDoEUI")
@ViewScoped
public class TypeDoEUI extends AbstractUI{

	@Autowired
	TypeDoEService typeDoEService;
	private ArrayList<TypeDoE> typeDoEList;
	private TypeDoE typeDoE;
	private LazyDataModel<TypeDoE> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		typeDoE = new TypeDoE();
		loadTypeDoEInfo();
	}

	public void saveTypeDoE()
	{
		try {
			typeDoEService.saveTypeDoE(typeDoE);
			displayInfoMssg("Update Successful...!!");
			loadTypeDoEInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteTypeDoE()
	{
		try {
			typeDoEService.deleteTypeDoE(typeDoE);
			displayWarningMssg("Update Successful...!!");
			loadTypeDoEInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<TypeDoE> findAllTypeDoE()
	{
		List<TypeDoE> list=new ArrayList<>();
		try {
			list= typeDoEService.findAllTypeDoE();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<TypeDoE> findAllTypeDoEPageable()
	{
		Pageable p=null;
		try {
			return typeDoEService.findAllTypeDoE(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TypeDoE> findAllTypeDoESort()
	{
		Sort s=null;
		List<TypeDoE> list=new ArrayList<>();
		try {
			list =typeDoEService.findAllTypeDoE(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		typeDoE = new TypeDoE();
	}
	
	public void loadTypeDoEInfo()
	{
		 dataModel = new LazyDataModel<TypeDoE>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<TypeDoE> list = new  ArrayList<TypeDoE>();
			   
			   @Override 
			   public List<TypeDoE> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<TypeDoE>) entityDAOFacade.getResultList(TypeDoE.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,TypeDoE.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(TypeDoE obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public TypeDoE getRowData(String rowKey) {
			        for(TypeDoE obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public TypeDoE getTypeDoE() {
		return typeDoE;
	}

	public void setTypeDoE(TypeDoE typeDoE) {
		this.typeDoE = typeDoE;
	}
	
	public ArrayList<TypeDoE> getTypeDoEList() {
		return typeDoEList;
	}

	public void setTypeDoEList(ArrayList<TypeDoE> typeDoEList) {
		this.typeDoEList =typeDoEList;
	}
	
	public LazyDataModel<TypeDoE> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<TypeDoE> dataModel) {
		this.dataModel = dataModel;
	}

}
