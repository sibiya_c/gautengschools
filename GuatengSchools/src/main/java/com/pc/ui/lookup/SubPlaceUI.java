package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.SubPlace;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.SubPlaceService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("subPlaceUI")
@ViewScoped
public class SubPlaceUI extends AbstractUI{

	@Autowired
	SubPlaceService subPlaceService;
	private ArrayList<SubPlace> subPlaceList;
	private SubPlace subPlace;
	private LazyDataModel<SubPlace> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		subPlace = new SubPlace();
		loadSubPlaceInfo();
	}

	public void saveSubPlace()
	{
		try {
			subPlaceService.saveSubPlace(subPlace);
			displayInfoMssg("Update Successful...!!");
			loadSubPlaceInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteSubPlace()
	{
		try {
			subPlaceService.deleteSubPlace(subPlace);
			displayWarningMssg("Update Successful...!!");
			loadSubPlaceInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<SubPlace> findAllSubPlace()
	{
		List<SubPlace> list=new ArrayList<>();
		try {
			list= subPlaceService.findAllSubPlace();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<SubPlace> findAllSubPlacePageable()
	{
		Pageable p=null;
		try {
			return subPlaceService.findAllSubPlace(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<SubPlace> findAllSubPlaceSort()
	{
		Sort s=null;
		List<SubPlace> list=new ArrayList<>();
		try {
			list =subPlaceService.findAllSubPlace(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		subPlace = new SubPlace();
	}
	
	public void loadSubPlaceInfo()
	{
		 dataModel = new LazyDataModel<SubPlace>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<SubPlace> list = new  ArrayList<SubPlace>();
			   
			   @Override 
			   public List<SubPlace> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<SubPlace>) entityDAOFacade.getResultList(SubPlace.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,SubPlace.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(SubPlace obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public SubPlace getRowData(String rowKey) {
			        for(SubPlace obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public SubPlace getSubPlace() {
		return subPlace;
	}

	public void setSubPlace(SubPlace subPlace) {
		this.subPlace = subPlace;
	}
	
	public ArrayList<SubPlace> getSubPlaceList() {
		return subPlaceList;
	}

	public void setSubPlaceList(ArrayList<SubPlace> subPlaceList) {
		this.subPlaceList =subPlaceList;
	}
	
	public LazyDataModel<SubPlace> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<SubPlace> dataModel) {
		this.dataModel = dataModel;
	}

}
