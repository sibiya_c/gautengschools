package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.ExamCentre;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.ExamCentreService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("examCentreUI")
@ViewScoped
public class ExamCentreUI extends AbstractUI{

	@Autowired
	ExamCentreService examCentreService;
	private ArrayList<ExamCentre> examCentreList;
	private ExamCentre examCentre;
	private LazyDataModel<ExamCentre> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		examCentre = new ExamCentre();
		loadExamCentreInfo();
	}

	public void saveExamCentre()
	{
		try {
			examCentreService.saveExamCentre(examCentre);
			displayInfoMssg("Update Successful...!!");
			loadExamCentreInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteExamCentre()
	{
		try {
			examCentreService.deleteExamCentre(examCentre);
			displayWarningMssg("Update Successful...!!");
			loadExamCentreInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<ExamCentre> findAllExamCentre()
	{
		List<ExamCentre> list=new ArrayList<>();
		try {
			list= examCentreService.findAllExamCentre();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<ExamCentre> findAllExamCentrePageable()
	{
		Pageable p=null;
		try {
			return examCentreService.findAllExamCentre(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<ExamCentre> findAllExamCentreSort()
	{
		Sort s=null;
		List<ExamCentre> list=new ArrayList<>();
		try {
			list =examCentreService.findAllExamCentre(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		examCentre = new ExamCentre();
	}
	
	public void loadExamCentreInfo()
	{
		 dataModel = new LazyDataModel<ExamCentre>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<ExamCentre> list = new  ArrayList<ExamCentre>();
			   
			   @Override 
			   public List<ExamCentre> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<ExamCentre>) entityDAOFacade.getResultList(ExamCentre.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,ExamCentre.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(ExamCentre obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public ExamCentre getRowData(String rowKey) {
			        for(ExamCentre obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public ExamCentre getExamCentre() {
		return examCentre;
	}

	public void setExamCentre(ExamCentre examCentre) {
		this.examCentre = examCentre;
	}
	
	public ArrayList<ExamCentre> getExamCentreList() {
		return examCentreList;
	}

	public void setExamCentreList(ArrayList<ExamCentre> examCentreList) {
		this.examCentreList =examCentreList;
	}
	
	public LazyDataModel<ExamCentre> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<ExamCentre> dataModel) {
		this.dataModel = dataModel;
	}

}
