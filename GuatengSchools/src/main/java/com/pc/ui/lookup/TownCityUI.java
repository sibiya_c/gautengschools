package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.TownCity;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.TownCityService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("townCityUI")
@ViewScoped
public class TownCityUI extends AbstractUI{

	@Autowired
	TownCityService townCityService;
	private ArrayList<TownCity> townCityList;
	private TownCity townCity;
	private LazyDataModel<TownCity> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		townCity = new TownCity();
		loadTownCityInfo();
	}

	public void saveTownCity()
	{
		try {
			townCityService.saveTownCity(townCity);
			displayInfoMssg("Update Successful...!!");
			loadTownCityInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteTownCity()
	{
		try {
			townCityService.deleteTownCity(townCity);
			displayWarningMssg("Update Successful...!!");
			loadTownCityInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<TownCity> findAllTownCity()
	{
		List<TownCity> list=new ArrayList<>();
		try {
			list= townCityService.findAllTownCity();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<TownCity> findAllTownCityPageable()
	{
		Pageable p=null;
		try {
			return townCityService.findAllTownCity(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TownCity> findAllTownCitySort()
	{
		Sort s=null;
		List<TownCity> list=new ArrayList<>();
		try {
			list =townCityService.findAllTownCity(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		townCity = new TownCity();
	}
	
	public void loadTownCityInfo()
	{
		 dataModel = new LazyDataModel<TownCity>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<TownCity> list = new  ArrayList<TownCity>();
			   
			   @Override 
			   public List<TownCity> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<TownCity>) entityDAOFacade.getResultList(TownCity.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,TownCity.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(TownCity obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public TownCity getRowData(String rowKey) {
			        for(TownCity obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public TownCity getTownCity() {
		return townCity;
	}

	public void setTownCity(TownCity townCity) {
		this.townCity = townCity;
	}
	
	public ArrayList<TownCity> getTownCityList() {
		return townCityList;
	}

	public void setTownCityList(ArrayList<TownCity> townCityList) {
		this.townCityList =townCityList;
	}
	
	public LazyDataModel<TownCity> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<TownCity> dataModel) {
		this.dataModel = dataModel;
	}

}
