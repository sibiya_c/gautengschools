package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.Suburb;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.SuburbService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("suburbUI")
@ViewScoped
public class SuburbUI extends AbstractUI{

	@Autowired
	SuburbService suburbService;
	private ArrayList<Suburb> suburbList;
	private Suburb suburb;
	private LazyDataModel<Suburb> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		suburb = new Suburb();
		loadSuburbInfo();
	}

	public void saveSuburb()
	{
		try {
			suburbService.saveSuburb(suburb);
			displayInfoMssg("Update Successful...!!");
			loadSuburbInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteSuburb()
	{
		try {
			suburbService.deleteSuburb(suburb);
			displayWarningMssg("Update Successful...!!");
			loadSuburbInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Suburb> findAllSuburb()
	{
		List<Suburb> list=new ArrayList<>();
		try {
			list= suburbService.findAllSuburb();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Suburb> findAllSuburbPageable()
	{
		Pageable p=null;
		try {
			return suburbService.findAllSuburb(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Suburb> findAllSuburbSort()
	{
		Sort s=null;
		List<Suburb> list=new ArrayList<>();
		try {
			list =suburbService.findAllSuburb(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		suburb = new Suburb();
	}
	
	public void loadSuburbInfo()
	{
		 dataModel = new LazyDataModel<Suburb>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<Suburb> list = new  ArrayList<Suburb>();
			   
			   @Override 
			   public List<Suburb> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<Suburb>) entityDAOFacade.getResultList(Suburb.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,Suburb.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(Suburb obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public Suburb getRowData(String rowKey) {
			        for(Suburb obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public Suburb getSuburb() {
		return suburb;
	}

	public void setSuburb(Suburb suburb) {
		this.suburb = suburb;
	}
	
	public ArrayList<Suburb> getSuburbList() {
		return suburbList;
	}

	public void setSuburbList(ArrayList<Suburb> suburbList) {
		this.suburbList =suburbList;
	}
	
	public LazyDataModel<Suburb> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Suburb> dataModel) {
		this.dataModel = dataModel;
	}

}
