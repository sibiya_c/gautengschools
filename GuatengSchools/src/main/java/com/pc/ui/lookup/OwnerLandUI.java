package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.OwnerLand;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.OwnerLandService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("ownerLandUI")
@ViewScoped
public class OwnerLandUI extends AbstractUI{

	@Autowired
	OwnerLandService ownerLandService;
	private ArrayList<OwnerLand> ownerLandList;
	private OwnerLand ownerLand;
	private LazyDataModel<OwnerLand> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		ownerLand = new OwnerLand();
		loadOwnerLandInfo();
	}

	public void saveOwnerLand()
	{
		try {
			ownerLandService.saveOwnerLand(ownerLand);
			displayInfoMssg("Update Successful...!!");
			loadOwnerLandInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteOwnerLand()
	{
		try {
			ownerLandService.deleteOwnerLand(ownerLand);
			displayWarningMssg("Update Successful...!!");
			loadOwnerLandInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<OwnerLand> findAllOwnerLand()
	{
		List<OwnerLand> list=new ArrayList<>();
		try {
			list= ownerLandService.findAllOwnerLand();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<OwnerLand> findAllOwnerLandPageable()
	{
		Pageable p=null;
		try {
			return ownerLandService.findAllOwnerLand(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<OwnerLand> findAllOwnerLandSort()
	{
		Sort s=null;
		List<OwnerLand> list=new ArrayList<>();
		try {
			list =ownerLandService.findAllOwnerLand(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		ownerLand = new OwnerLand();
	}
	
	public void loadOwnerLandInfo()
	{
		 dataModel = new LazyDataModel<OwnerLand>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<OwnerLand> list = new  ArrayList<OwnerLand>();
			   
			   @Override 
			   public List<OwnerLand> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<OwnerLand>) entityDAOFacade.getResultList(OwnerLand.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,OwnerLand.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(OwnerLand obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public OwnerLand getRowData(String rowKey) {
			        for(OwnerLand obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public OwnerLand getOwnerLand() {
		return ownerLand;
	}

	public void setOwnerLand(OwnerLand ownerLand) {
		this.ownerLand = ownerLand;
	}
	
	public ArrayList<OwnerLand> getOwnerLandList() {
		return ownerLandList;
	}

	public void setOwnerLandList(ArrayList<OwnerLand> ownerLandList) {
		this.ownerLandList =ownerLandList;
	}
	
	public LazyDataModel<OwnerLand> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<OwnerLand> dataModel) {
		this.dataModel = dataModel;
	}

}
