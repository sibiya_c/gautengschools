package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.OwnerBuilding;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.OwnerBuildingService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("ownerBuildingUI")
@ViewScoped
public class OwnerBuildingUI extends AbstractUI{

	@Autowired
	OwnerBuildingService ownerBuildingService;
	private ArrayList<OwnerBuilding> ownerBuildingList;
	private OwnerBuilding ownerBuilding;
	private LazyDataModel<OwnerBuilding> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		ownerBuilding = new OwnerBuilding();
		loadOwnerBuildingInfo();
	}

	public void saveOwnerBuilding()
	{
		try {
			ownerBuildingService.saveOwnerBuilding(ownerBuilding);
			displayInfoMssg("Update Successful...!!");
			loadOwnerBuildingInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteOwnerBuilding()
	{
		try {
			ownerBuildingService.deleteOwnerBuilding(ownerBuilding);
			displayWarningMssg("Update Successful...!!");
			loadOwnerBuildingInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<OwnerBuilding> findAllOwnerBuilding()
	{
		List<OwnerBuilding> list=new ArrayList<>();
		try {
			list= ownerBuildingService.findAllOwnerBuilding();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<OwnerBuilding> findAllOwnerBuildingPageable()
	{
		Pageable p=null;
		try {
			return ownerBuildingService.findAllOwnerBuilding(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<OwnerBuilding> findAllOwnerBuildingSort()
	{
		Sort s=null;
		List<OwnerBuilding> list=new ArrayList<>();
		try {
			list =ownerBuildingService.findAllOwnerBuilding(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		ownerBuilding = new OwnerBuilding();
	}
	
	public void loadOwnerBuildingInfo()
	{
		 dataModel = new LazyDataModel<OwnerBuilding>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<OwnerBuilding> list = new  ArrayList<OwnerBuilding>();
			   
			   @Override 
			   public List<OwnerBuilding> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<OwnerBuilding>) entityDAOFacade.getResultList(OwnerBuilding.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,OwnerBuilding.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(OwnerBuilding obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public OwnerBuilding getRowData(String rowKey) {
			        for(OwnerBuilding obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public OwnerBuilding getOwnerBuilding() {
		return ownerBuilding;
	}

	public void setOwnerBuilding(OwnerBuilding ownerBuilding) {
		this.ownerBuilding = ownerBuilding;
	}
	
	public ArrayList<OwnerBuilding> getOwnerBuildingList() {
		return ownerBuildingList;
	}

	public void setOwnerBuildingList(ArrayList<OwnerBuilding> ownerBuildingList) {
		this.ownerBuildingList =ownerBuildingList;
	}
	
	public LazyDataModel<OwnerBuilding> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<OwnerBuilding> dataModel) {
		this.dataModel = dataModel;
	}

}
