package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.District;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.DistrictService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("districtUI")
@ViewScoped
public class DistrictUI extends AbstractUI{

	@Autowired
	DistrictService districtService;
	private ArrayList<District> districtList;
	private District district;
	private LazyDataModel<District> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		district = new District();
		loadDistrictInfo();
	}

	public void saveDistrict()
	{
		try {
			districtService.saveDistrict(district);
			displayInfoMssg("Update Successful...!!");
			loadDistrictInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteDistrict()
	{
		try {
			districtService.deleteDistrict(district);
			displayWarningMssg("Update Successful...!!");
			loadDistrictInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<District> findAllDistrict()
	{
		List<District> list=new ArrayList<>();
		try {
			list= districtService.findAllDistrict();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<District> findAllDistrictPageable()
	{
		Pageable p=null;
		try {
			return districtService.findAllDistrict(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<District> findAllDistrictSort()
	{
		Sort s=null;
		List<District> list=new ArrayList<>();
		try {
			list =districtService.findAllDistrict(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		district = new District();
	}
	
	public void loadDistrictInfo()
	{
		 dataModel = new LazyDataModel<District>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<District> list = new  ArrayList<District>();
			   
			   @Override 
			   public List<District> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<District>) entityDAOFacade.getResultList(District.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,District.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(District obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public District getRowData(String rowKey) {
			        for(District obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}
	
	public ArrayList<District> getDistrictList() {
		return districtList;
	}

	public void setDistrictList(ArrayList<District> districtList) {
		this.districtList =districtList;
	}
	
	public LazyDataModel<District> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<District> dataModel) {
		this.dataModel = dataModel;
	}

}
