package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.PhasePED;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.PhasePEDService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("phasePEDUI")
@ViewScoped
public class PhasePEDUI extends AbstractUI{

	@Autowired
	PhasePEDService phasePEDService;
	private ArrayList<PhasePED> phasePEDList;
	private PhasePED phasePED;
	private LazyDataModel<PhasePED> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		phasePED = new PhasePED();
		loadPhasePEDInfo();
	}

	public void savePhasePED()
	{
		try {
			phasePEDService.savePhasePED(phasePED);
			displayInfoMssg("Update Successful...!!");
			loadPhasePEDInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deletePhasePED()
	{
		try {
			phasePEDService.deletePhasePED(phasePED);
			displayWarningMssg("Update Successful...!!");
			loadPhasePEDInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<PhasePED> findAllPhasePED()
	{
		List<PhasePED> list=new ArrayList<>();
		try {
			list= phasePEDService.findAllPhasePED();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<PhasePED> findAllPhasePEDPageable()
	{
		Pageable p=null;
		try {
			return phasePEDService.findAllPhasePED(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<PhasePED> findAllPhasePEDSort()
	{
		Sort s=null;
		List<PhasePED> list=new ArrayList<>();
		try {
			list =phasePEDService.findAllPhasePED(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		phasePED = new PhasePED();
	}
	
	public void loadPhasePEDInfo()
	{
		 dataModel = new LazyDataModel<PhasePED>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<PhasePED> list = new  ArrayList<PhasePED>();
			   
			   @Override 
			   public List<PhasePED> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<PhasePED>) entityDAOFacade.getResultList(PhasePED.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,PhasePED.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(PhasePED obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public PhasePED getRowData(String rowKey) {
			        for(PhasePED obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public PhasePED getPhasePED() {
		return phasePED;
	}

	public void setPhasePED(PhasePED phasePED) {
		this.phasePED = phasePED;
	}
	
	public ArrayList<PhasePED> getPhasePEDList() {
		return phasePEDList;
	}

	public void setPhasePEDList(ArrayList<PhasePED> phasePEDList) {
		this.phasePEDList =phasePEDList;
	}
	
	public LazyDataModel<PhasePED> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<PhasePED> dataModel) {
		this.dataModel = dataModel;
	}

}
