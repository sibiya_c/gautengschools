package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.AddrInit;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.AddrInitService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("addrInitUI")
@ViewScoped
public class AddrInitUI extends AbstractUI{

	@Autowired
	AddrInitService addrInitService;
	private ArrayList<AddrInit> addrInitList;
	private AddrInit addrInit;
	private LazyDataModel<AddrInit> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		addrInit = new AddrInit();
		loadAddrInitInfo();
	}

	public void saveAddrInit()
	{
		try {
			addrInitService.saveAddrInit(addrInit);
			displayInfoMssg("Update Successful...!!");
			loadAddrInitInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteAddrInit()
	{
		try {
			addrInitService.deleteAddrInit(addrInit);
			displayWarningMssg("Update Successful...!!");
			loadAddrInitInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<AddrInit> findAllAddrInit()
	{
		List<AddrInit> list=new ArrayList<>();
		try {
			list= addrInitService.findAllAddrInit();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<AddrInit> findAllAddrInitPageable()
	{
		Pageable p=null;
		try {
			return addrInitService.findAllAddrInit(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<AddrInit> findAllAddrInitSort()
	{
		Sort s=null;
		List<AddrInit> list=new ArrayList<>();
		try {
			list =addrInitService.findAllAddrInit(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		addrInit = new AddrInit();
	}
	
	public void loadAddrInitInfo()
	{
		 dataModel = new LazyDataModel<AddrInit>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<AddrInit> list = new  ArrayList<AddrInit>();
			   
			   @Override 
			   public List<AddrInit> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<AddrInit>) entityDAOFacade.getResultList(AddrInit.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,AddrInit.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(AddrInit obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public AddrInit getRowData(String rowKey) {
			        for(AddrInit obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public AddrInit getAddrInit() {
		return addrInit;
	}

	public void setAddrInit(AddrInit addrInit) {
		this.addrInit = addrInit;
	}
	
	public ArrayList<AddrInit> getAddrInitList() {
		return addrInitList;
	}

	public void setAddrInitList(ArrayList<AddrInit> addrInitList) {
		this.addrInitList =addrInitList;
	}
	
	public LazyDataModel<AddrInit> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<AddrInit> dataModel) {
		this.dataModel = dataModel;
	}

}
