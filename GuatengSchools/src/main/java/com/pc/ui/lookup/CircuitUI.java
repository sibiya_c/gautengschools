package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.Circuit;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.CircuitService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("circuitUI")
@ViewScoped
public class CircuitUI extends AbstractUI{

	@Autowired
	CircuitService circuitService;
	private ArrayList<Circuit> circuitList;
	private Circuit circuit;
	private LazyDataModel<Circuit> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		circuit = new Circuit();
		loadCircuitInfo();
	}

	public void saveCircuit()
	{
		try {
			circuitService.saveCircuit(circuit);
			displayInfoMssg("Update Successful...!!");
			loadCircuitInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteCircuit()
	{
		try {
			circuitService.deleteCircuit(circuit);
			displayWarningMssg("Update Successful...!!");
			loadCircuitInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Circuit> findAllCircuit()
	{
		List<Circuit> list=new ArrayList<>();
		try {
			list= circuitService.findAllCircuit();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Circuit> findAllCircuitPageable()
	{
		Pageable p=null;
		try {
			return circuitService.findAllCircuit(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Circuit> findAllCircuitSort()
	{
		Sort s=null;
		List<Circuit> list=new ArrayList<>();
		try {
			list =circuitService.findAllCircuit(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		circuit = new Circuit();
	}
	
	public void loadCircuitInfo()
	{
		 dataModel = new LazyDataModel<Circuit>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<Circuit> list = new  ArrayList<Circuit>();
			   
			   @Override 
			   public List<Circuit> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<Circuit>) entityDAOFacade.getResultList(Circuit.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,Circuit.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(Circuit obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public Circuit getRowData(String rowKey) {
			        for(Circuit obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public Circuit getCircuit() {
		return circuit;
	}

	public void setCircuit(Circuit circuit) {
		this.circuit = circuit;
	}
	
	public ArrayList<Circuit> getCircuitList() {
		return circuitList;
	}

	public void setCircuitList(ArrayList<Circuit> circuitList) {
		this.circuitList =circuitList;
	}
	
	public LazyDataModel<Circuit> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Circuit> dataModel) {
		this.dataModel = dataModel;
	}

}
