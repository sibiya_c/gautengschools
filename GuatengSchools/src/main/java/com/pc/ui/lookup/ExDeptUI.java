package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.ExDept;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.ExDeptService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("exDeptUI")
@ViewScoped
public class ExDeptUI extends AbstractUI{

	@Autowired
	ExDeptService exDeptService;
	private ArrayList<ExDept> exDeptList;
	private ExDept exDept;
	private LazyDataModel<ExDept> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		exDept = new ExDept();
		loadExDeptInfo();
	}

	public void saveExDept()
	{
		try {
			exDeptService.saveExDept(exDept);
			displayInfoMssg("Update Successful...!!");
			loadExDeptInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteExDept()
	{
		try {
			exDeptService.deleteExDept(exDept);
			displayWarningMssg("Update Successful...!!");
			loadExDeptInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<ExDept> findAllExDept()
	{
		List<ExDept> list=new ArrayList<>();
		try {
			list= exDeptService.findAllExDept();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<ExDept> findAllExDeptPageable()
	{
		Pageable p=null;
		try {
			return exDeptService.findAllExDept(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<ExDept> findAllExDeptSort()
	{
		Sort s=null;
		List<ExDept> list=new ArrayList<>();
		try {
			list =exDeptService.findAllExDept(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		exDept = new ExDept();
	}
	
	public void loadExDeptInfo()
	{
		 dataModel = new LazyDataModel<ExDept>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<ExDept> list = new  ArrayList<ExDept>();
			   
			   @Override 
			   public List<ExDept> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<ExDept>) entityDAOFacade.getResultList(ExDept.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,ExDept.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(ExDept obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public ExDept getRowData(String rowKey) {
			        for(ExDept obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public ExDept getExDept() {
		return exDept;
	}

	public void setExDept(ExDept exDept) {
		this.exDept = exDept;
	}
	
	public ArrayList<ExDept> getExDeptList() {
		return exDeptList;
	}

	public void setExDeptList(ArrayList<ExDept> exDeptList) {
		this.exDeptList =exDeptList;
	}
	
	public LazyDataModel<ExDept> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<ExDept> dataModel) {
		this.dataModel = dataModel;
	}

}
