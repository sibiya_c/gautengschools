package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.AppProperty;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.AppPropertyService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("appPropertyUI")
@ViewScoped
public class AppPropertyUI extends AbstractUI{

	@Autowired
	AppPropertyService appPropertyService;
	private ArrayList<AppProperty> appPropertyList;
	private AppProperty appProperty;
	private LazyDataModel<AppProperty> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		appProperty = new AppProperty();
		loadAppPropertyInfo();
	}

	public void saveAppProperty()
	{
		try {
			appPropertyService.saveAppProperty(appProperty);
			displayInfoMssg("Update Successful...!!");
			loadAppPropertyInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteAppProperty()
	{
		try {
			appPropertyService.deleteAppProperty(appProperty);
			displayWarningMssg("Update Successful...!!");
			loadAppPropertyInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<AppProperty> findAllAppProperty()
	{
		List<AppProperty> list=new ArrayList<>();
		try {
			list= appPropertyService.findAllAppProperty();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<AppProperty> findAllAppPropertyPageable()
	{
		Pageable p=null;
		try {
			return appPropertyService.findAllAppProperty(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<AppProperty> findAllAppPropertySort()
	{
		Sort s=null;
		List<AppProperty> list=new ArrayList<>();
		try {
			list =appPropertyService.findAllAppProperty(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		appProperty = new AppProperty();
	}
	
	public void loadAppPropertyInfo()
	{
		 dataModel = new LazyDataModel<AppProperty>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<AppProperty> list = new  ArrayList<AppProperty>();
			   
			   @Override 
			   public List<AppProperty> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<AppProperty>) entityDAOFacade.getResultList(AppProperty.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,AppProperty.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(AppProperty obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public AppProperty getRowData(String rowKey) {
			        for(AppProperty obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public AppProperty getAppProperty() {
		return appProperty;
	}

	public void setAppProperty(AppProperty appProperty) {
		this.appProperty = appProperty;
	}
	
	public ArrayList<AppProperty> getAppPropertyList() {
		return appPropertyList;
	}

	public void setAppPropertyList(ArrayList<AppProperty> appPropertyList) {
		this.appPropertyList =appPropertyList;
	}
	
	public LazyDataModel<AppProperty> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<AppProperty> dataModel) {
		this.dataModel = dataModel;
	}

}
