package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.Role;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.RoleService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("roleUI")
@ViewScoped
public class RoleUI extends AbstractUI{

	@Autowired
	RoleService roleService;
	private ArrayList<Role> roleList;
	private Role role;
	private LazyDataModel<Role> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		role = new Role();
		loadRoleInfo();
	}

	public void saveRole()
	{
		try {
			roleService.saveRole(role);
			displayInfoMssg("Update Successful...!!");
			loadRoleInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteRole()
	{
		try {
			roleService.deleteRole(role);
			displayWarningMssg("Update Successful...!!");
			loadRoleInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Role> findAllRole()
	{
		List<Role> list=new ArrayList<>();
		try {
			list= roleService.findAllRole();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Role> findAllRolePageable()
	{
		Pageable p=null;
		try {
			return roleService.findAllRole(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Role> findAllRoleSort()
	{
		Sort s=null;
		List<Role> list=new ArrayList<>();
		try {
			list =roleService.findAllRole(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		role = new Role();
	}
	
	public void loadRoleInfo()
	{
		 dataModel = new LazyDataModel<Role>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<Role> list = new  ArrayList<Role>();
			   
			   @Override 
			   public List<Role> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<Role>) entityDAOFacade.getResultList(Role.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,Role.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(Role obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public Role getRowData(String rowKey) {
			        for(Role obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public ArrayList<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(ArrayList<Role> roleList) {
		this.roleList =roleList;
	}
	
	public LazyDataModel<Role> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Role> dataModel) {
		this.dataModel = dataModel;
	}

}
