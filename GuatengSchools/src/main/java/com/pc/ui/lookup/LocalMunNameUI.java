package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.LocalMunName;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.LocalMunNameService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("localMunNameUI")
@ViewScoped
public class LocalMunNameUI extends AbstractUI{

	@Autowired
	LocalMunNameService localMunNameService;
	private ArrayList<LocalMunName> localMunNameList;
	private LocalMunName localMunName;
	private LazyDataModel<LocalMunName> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		localMunName = new LocalMunName();
		loadLocalMunNameInfo();
	}

	public void saveLocalMunName()
	{
		try {
			localMunNameService.saveLocalMunName(localMunName);
			displayInfoMssg("Update Successful...!!");
			loadLocalMunNameInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteLocalMunName()
	{
		try {
			localMunNameService.deleteLocalMunName(localMunName);
			displayWarningMssg("Update Successful...!!");
			loadLocalMunNameInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<LocalMunName> findAllLocalMunName()
	{
		List<LocalMunName> list=new ArrayList<>();
		try {
			list= localMunNameService.findAllLocalMunName();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<LocalMunName> findAllLocalMunNamePageable()
	{
		Pageable p=null;
		try {
			return localMunNameService.findAllLocalMunName(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<LocalMunName> findAllLocalMunNameSort()
	{
		Sort s=null;
		List<LocalMunName> list=new ArrayList<>();
		try {
			list =localMunNameService.findAllLocalMunName(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		localMunName = new LocalMunName();
	}
	
	public void loadLocalMunNameInfo()
	{
		 dataModel = new LazyDataModel<LocalMunName>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<LocalMunName> list = new  ArrayList<LocalMunName>();
			   
			   @Override 
			   public List<LocalMunName> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<LocalMunName>) entityDAOFacade.getResultList(LocalMunName.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,LocalMunName.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(LocalMunName obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public LocalMunName getRowData(String rowKey) {
			        for(LocalMunName obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public LocalMunName getLocalMunName() {
		return localMunName;
	}

	public void setLocalMunName(LocalMunName localMunName) {
		this.localMunName = localMunName;
	}
	
	public ArrayList<LocalMunName> getLocalMunNameList() {
		return localMunNameList;
	}

	public void setLocalMunNameList(ArrayList<LocalMunName> localMunNameList) {
		this.localMunNameList =localMunNameList;
	}
	
	public LazyDataModel<LocalMunName> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<LocalMunName> dataModel) {
		this.dataModel = dataModel;
	}

}
