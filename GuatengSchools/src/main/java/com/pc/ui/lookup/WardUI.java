package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.Ward;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.WardService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("wardUI")
@ViewScoped
public class WardUI extends AbstractUI{

	@Autowired
	WardService wardService;
	private ArrayList<Ward> wardList;
	private Ward ward;
	private LazyDataModel<Ward> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		ward = new Ward();
		loadWardInfo();
	}

	public void saveWard()
	{
		try {
			wardService.saveWard(ward);
			displayInfoMssg("Update Successful...!!");
			loadWardInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteWard()
	{
		try {
			wardService.deleteWard(ward);
			displayWarningMssg("Update Successful...!!");
			loadWardInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Ward> findAllWard()
	{
		List<Ward> list=new ArrayList<>();
		try {
			list= wardService.findAllWard();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Ward> findAllWardPageable()
	{
		Pageable p=null;
		try {
			return wardService.findAllWard(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Ward> findAllWardSort()
	{
		Sort s=null;
		List<Ward> list=new ArrayList<>();
		try {
			list =wardService.findAllWard(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		ward = new Ward();
	}
	
	public void loadWardInfo()
	{
		 dataModel = new LazyDataModel<Ward>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<Ward> list = new  ArrayList<Ward>();
			   
			   @Override 
			   public List<Ward> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<Ward>) entityDAOFacade.getResultList(Ward.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,Ward.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(Ward obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public Ward getRowData(String rowKey) {
			        for(Ward obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public Ward getWard() {
		return ward;
	}

	public void setWard(Ward ward) {
		this.ward = ward;
	}
	
	public ArrayList<Ward> getWardList() {
		return wardList;
	}

	public void setWardList(ArrayList<Ward> wardList) {
		this.wardList =wardList;
	}
	
	public LazyDataModel<Ward> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Ward> dataModel) {
		this.dataModel = dataModel;
	}

}
