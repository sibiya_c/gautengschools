package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.DeptMunName;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.DeptMunNameService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("deptMunNameUI")
@ViewScoped
public class DeptMunNameUI extends AbstractUI{

	@Autowired
	DeptMunNameService deptMunNameService;
	private ArrayList<DeptMunName> deptMunNameList;
	private DeptMunName deptMunName;
	private LazyDataModel<DeptMunName> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		deptMunName = new DeptMunName();
		loadDeptMunNameInfo();
	}

	public void saveDeptMunName()
	{
		try {
			deptMunNameService.saveDeptMunName(deptMunName);
			displayInfoMssg("Update Successful...!!");
			loadDeptMunNameInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteDeptMunName()
	{
		try {
			deptMunNameService.deleteDeptMunName(deptMunName);
			displayWarningMssg("Update Successful...!!");
			loadDeptMunNameInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<DeptMunName> findAllDeptMunName()
	{
		List<DeptMunName> list=new ArrayList<>();
		try {
			list= deptMunNameService.findAllDeptMunName();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<DeptMunName> findAllDeptMunNamePageable()
	{
		Pageable p=null;
		try {
			return deptMunNameService.findAllDeptMunName(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<DeptMunName> findAllDeptMunNameSort()
	{
		Sort s=null;
		List<DeptMunName> list=new ArrayList<>();
		try {
			list =deptMunNameService.findAllDeptMunName(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		deptMunName = new DeptMunName();
	}
	
	public void loadDeptMunNameInfo()
	{
		 dataModel = new LazyDataModel<DeptMunName>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<DeptMunName> list = new  ArrayList<DeptMunName>();
			   
			   @Override 
			   public List<DeptMunName> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<DeptMunName>) entityDAOFacade.getResultList(DeptMunName.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,DeptMunName.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(DeptMunName obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public DeptMunName getRowData(String rowKey) {
			        for(DeptMunName obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public DeptMunName getDeptMunName() {
		return deptMunName;
	}

	public void setDeptMunName(DeptMunName deptMunName) {
		this.deptMunName = deptMunName;
	}
	
	public ArrayList<DeptMunName> getDeptMunNameList() {
		return deptMunNameList;
	}

	public void setDeptMunNameList(ArrayList<DeptMunName> deptMunNameList) {
		this.deptMunNameList =deptMunNameList;
	}
	
	public LazyDataModel<DeptMunName> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<DeptMunName> dataModel) {
		this.dataModel = dataModel;
	}

}
