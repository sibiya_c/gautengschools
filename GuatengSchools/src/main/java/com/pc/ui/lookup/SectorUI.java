package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.Sector;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.SectorService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("sectorUI")
@ViewScoped
public class SectorUI extends AbstractUI{

	@Autowired
	SectorService sectorService;
	private ArrayList<Sector> sectorList;
	private Sector sector;
	private LazyDataModel<Sector> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		sector = new Sector();
		loadSectorInfo();
	}

	public void saveSector()
	{
		try {
			sectorService.saveSector(sector);
			displayInfoMssg("Update Successful...!!");
			loadSectorInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteSector()
	{
		try {
			sectorService.deleteSector(sector);
			displayWarningMssg("Update Successful...!!");
			loadSectorInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Sector> findAllSector()
	{
		List<Sector> list=new ArrayList<>();
		try {
			list= sectorService.findAllSector();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Sector> findAllSectorPageable()
	{
		Pageable p=null;
		try {
			return sectorService.findAllSector(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Sector> findAllSectorSort()
	{
		Sort s=null;
		List<Sector> list=new ArrayList<>();
		try {
			list =sectorService.findAllSector(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		sector = new Sector();
	}
	
	public void loadSectorInfo()
	{
		 dataModel = new LazyDataModel<Sector>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<Sector> list = new  ArrayList<Sector>();
			   
			   @Override 
			   public List<Sector> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<Sector>) entityDAOFacade.getResultList(Sector.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,Sector.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(Sector obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public Sector getRowData(String rowKey) {
			        for(Sector obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public Sector getSector() {
		return sector;
	}

	public void setSector(Sector sector) {
		this.sector = sector;
	}
	
	public ArrayList<Sector> getSectorList() {
		return sectorList;
	}

	public void setSectorList(ArrayList<Sector> sectorList) {
		this.sectorList =sectorList;
	}
	
	public LazyDataModel<Sector> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Sector> dataModel) {
		this.dataModel = dataModel;
	}

}
