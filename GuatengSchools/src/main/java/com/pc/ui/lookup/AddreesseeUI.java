package com.pc.ui.lookup;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import com.pc.entities.lookup.Addreessee;
import com.pc.framework.AbstractUI;
import com.pc.service.lookup.AddreesseeService;
import com.pc.dao.EntityDAOFacade;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;

@Component("addreesseeUI")
@ViewScoped
public class AddreesseeUI extends AbstractUI{

	@Autowired
	AddreesseeService addreesseeService;
	private ArrayList<Addreessee> addreesseeList;
	private Addreessee addreessee;
	private LazyDataModel<Addreessee> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		addreessee = new Addreessee();
		loadAddreesseeInfo();
	}

	public void saveAddreessee()
	{
		try {
			addreesseeService.saveAddreessee(addreessee);
			displayInfoMssg("Update Successful...!!");
			loadAddreesseeInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteAddreessee()
	{
		try {
			addreesseeService.deleteAddreessee(addreessee);
			displayWarningMssg("Update Successful...!!");
			loadAddreesseeInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Addreessee> findAllAddreessee()
	{
		List<Addreessee> list=new ArrayList<>();
		try {
			list= addreesseeService.findAllAddreessee();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Addreessee> findAllAddreesseePageable()
	{
		Pageable p=null;
		try {
			return addreesseeService.findAllAddreessee(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Addreessee> findAllAddreesseeSort()
	{
		Sort s=null;
		List<Addreessee> list=new ArrayList<>();
		try {
			list =addreesseeService.findAllAddreessee(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		addreessee = new Addreessee();
	}
	
	public void loadAddreesseeInfo()
	{
		 dataModel = new LazyDataModel<Addreessee>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<Addreessee> list = new  ArrayList<Addreessee>();
			   
			   @Override 
			   public List<Addreessee> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<Addreessee>) entityDAOFacade.getResultList(Addreessee.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,Addreessee.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(Addreessee obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public Addreessee getRowData(String rowKey) {
			        for(Addreessee obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	

	public Addreessee getAddreessee() {
		return addreessee;
	}

	public void setAddreessee(Addreessee addreessee) {
		this.addreessee = addreessee;
	}
	
	public ArrayList<Addreessee> getAddreesseeList() {
		return addreesseeList;
	}

	public void setAddreesseeList(ArrayList<Addreessee> addreesseeList) {
		this.addreesseeList =addreesseeList;
	}
	
	public LazyDataModel<Addreessee> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Addreessee> dataModel) {
		this.dataModel = dataModel;
	}

}
