package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;
import com.pc.dao.EntityDAOFacade;

import com.pc.entities.GautengSchoolImport;
import com.pc.framework.AbstractUI;
import com.pc.service.GautengSchoolImportService;
import com.pc.service.InstitutionService;
import com.pc.utils.CSVUtil;

@Component
@ManagedBean(name="gautengSchoolImportUI")
@ViewScoped
public class GautengSchoolImportUI extends AbstractUI{

	@Autowired
	GautengSchoolImportService gautengSchoolImportService;
	private ArrayList<GautengSchoolImport> gautengSchoolImportList;
	private GautengSchoolImport gautengSchoolImport;
	private LazyDataModel<GautengSchoolImport> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;
	@Autowired
	InstitutionService institutionService;
	private CSVUtil csvUtil = new CSVUtil();

	@PostConstruct
	public void init() {
		gautengSchoolImport = new GautengSchoolImport();
		loadGautengSchoolImportInfo();
	}

	public void saveGautengSchoolImport()
	{
		try {
			gautengSchoolImportService.saveGautengSchoolImport(gautengSchoolImport);
			displayInfoMssg("Update Successful...!!");
			loadGautengSchoolImportInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteGautengSchoolImport()
	{
		try {
			gautengSchoolImportService.deleteGautengSchoolImport(gautengSchoolImport);
			displayWarningMssg("Update Successful...!!");
			loadGautengSchoolImportInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<GautengSchoolImport> findAllGautengSchoolImport()
	{
		List<GautengSchoolImport> list=new ArrayList<>();
		try {
			list= gautengSchoolImportService.findAllGautengSchoolImport();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<GautengSchoolImport> findAllGautengSchoolImportPageable()
	{
		Pageable p=null;
		try {
			return gautengSchoolImportService.findAllGautengSchoolImport(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<GautengSchoolImport> findAllGautengSchoolImportSort()
	{
		Sort s=null;
		List<GautengSchoolImport> list=new ArrayList<>();
		try {
			list =gautengSchoolImportService.findAllGautengSchoolImport(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		gautengSchoolImport = new GautengSchoolImport();
	}
	
	public void loadGautengSchoolImportInfo()
	{
		 dataModel = new LazyDataModel<GautengSchoolImport>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<GautengSchoolImport> list = new  ArrayList<GautengSchoolImport>();
			   
			   @Override 
			   public List<GautengSchoolImport> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<GautengSchoolImport>) entityDAOFacade.getResultList(GautengSchoolImport.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,GautengSchoolImport.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(GautengSchoolImport obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public GautengSchoolImport getRowData(String rowKey) {
			        for(GautengSchoolImport obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	public GautengSchoolImport getGautengSchoolImport() {
		return gautengSchoolImport;
	}

	public void setGautengSchoolImport(GautengSchoolImport gautengSchoolImport) {
		this.gautengSchoolImport = gautengSchoolImport;
	}
	
	public ArrayList<GautengSchoolImport> getGautengSchoolImportList() {
		return gautengSchoolImportList;
	}

	public void setGautengSchoolImportList(ArrayList<GautengSchoolImport> gautengSchoolImportList) {
		this.gautengSchoolImportList =gautengSchoolImportList;
	}
	public LazyDataModel<GautengSchoolImport> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<GautengSchoolImport> dataModel) {
		this.dataModel = dataModel;
	}
	
	public void uploadSchools(FileUploadEvent event) {
		try {
			
			logger.info("Starting file upload");
			List<GautengSchoolImport> csvDataList = csvUtil.getObjects(GautengSchoolImport.class,
					event.getFile().getInputstream(), ",");
			gautengSchoolImportService.saveAllGautengSchoolImport(csvDataList);
			institutionService.saveImpotedInstitution(csvDataList);
			
			logger.info("Finished uploading data. number of entries: " + csvDataList.size());
			displayInfoMssg("Finished uploading data. number of entries: " + csvDataList.size());
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		} finally {
			csvUtil = new CSVUtil();
		}
	}
	
	
}
