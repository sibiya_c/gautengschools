package com.pc.ui;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.pc.beans.LookupMenuBeans;
import com.pc.framework.AbstractUI;
import com.pc.service.LookupService;

@Component("lookupsUI")
@ViewScoped
public class LookupsUI extends AbstractUI{

	ArrayList<LookupMenuBeans> lookupMenuBeansList;
	
	@Autowired
	LookupService lookupService;

	@PostConstruct
	public void init() {
		try {
			lookupMenuBeansList =lookupService.getlookupsMenuBean();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			displayErrorMssg(e.getMessage());
		}
	}

	public ArrayList<LookupMenuBeans> getLookupMenuBeansList() {
		return lookupMenuBeansList;
	}

	public void setLookupMenuBeansList(ArrayList<LookupMenuBeans> lookupMenuBeansList) {
		this.lookupMenuBeansList = lookupMenuBeansList;
	}

	
	

	

}
