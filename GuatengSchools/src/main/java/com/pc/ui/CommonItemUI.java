package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pc.entities.*;
import com.pc.entities.enums.*;
import com.pc.entities.lookup.*;
import com.pc.framework.AbstractUI;
import com.pc.service.*;
import com.pc.service.lookup.*;


@Component("commonItemUI")
@ViewScoped
public class CommonItemUI  extends AbstractUI
{
	@Autowired
	GenderService genderService;
	
	@Autowired
	TitleService titleService;
	
	
	@PostConstruct
	public void init() {
		
	}
	
	/**
	 * Gets the select items gender.
	 *
	 * @return the select items gender
	 */
	public List<Gender> getSelectItemsGender() {
		
		List<Gender> l = null;
		try {
			
			l = genderService.findAllGender();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	public List<Gender> autoCompleteGender(String searchText) 
	{
		List<Gender> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=genderService.findListByGenderName(searchText);
			}
			else
			{
				list=genderService.findAllGender();
			}
			
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	/**
	 * Gets the select items title.
	 *
	 * @return the select items title
	 */
	public List<Title> getSelectItemsTitle() {
		
		List<Title> l = null;
		try {
			
			l = titleService.findAllTitle();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	public List<SelectItem> getYesNoEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (YesNoEnum val : YesNoEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	
	
	
	
	@Autowired
	AppPropertyService appPropertyService; 
	
	public List<AppProperty> autoCompleteAppProperty(String searchText) 
	{
		List<AppProperty> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=appPropertyService.findAllAppProperty();
			}
			else
			{
				list=appPropertyService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<AppProperty> getSelectItemsAppProperty() {
		
		List<AppProperty> l = null;
		try {
			
			l = appPropertyService.findAllAppProperty();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	@Autowired
	AddressService addressService; 
	
	public List<Address> autoCompleteAddress(String searchText) 
	{
		List<Address> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=addressService.findAllAddress();
			}
			else
			{
				list=addressService.findByDescriptionStartingWith(searchText);
			}*/
			list=addressService.findAllAddress();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Address> getSelectItemsAddress() {
		
		List<Address> l = null;
		try {
			
			l = addressService.findAllAddress();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	TownCityService townCityService; 
	
	public List<TownCity> autoCompleteTownCity(String searchText) 
	{
		List<TownCity> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=townCityService.findAllTownCity();
			}
			else
			{
				list=townCityService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<TownCity> getSelectItemsTownCity() {
		
		List<TownCity> l = null;
		try {
			
			l = townCityService.findAllTownCity();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	SuburbService suburbService; 
	
	public List<Suburb> autoCompleteSuburb(String searchText) 
	{
		List<Suburb> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=suburbService.findAllSuburb();
			}
			else
			{
				list=suburbService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Suburb> getSelectItemsSuburb() {
		
		List<Suburb> l = null;
		try {
			
			l = suburbService.findAllSuburb();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	TownshipVillageService townshipVillageService; 
	
	public List<TownshipVillage> autoCompleteTownshipVillage(String searchText) 
	{
		List<TownshipVillage> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=townshipVillageService.findAllTownshipVillage();
			}
			else
			{
				list=townshipVillageService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<TownshipVillage> getSelectItemsTownshipVillage() {
		
		List<TownshipVillage> l = null;
		try {
			
			l = townshipVillageService.findAllTownshipVillage();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	AddreesseeService addreesseeService; 
	
	public List<Addreessee> autoCompleteAddreessee(String searchText) 
	{
		List<Addreessee> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=addreesseeService.findAllAddreessee();
			}
			else
			{
				list=addreesseeService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Addreessee> getSelectItemsAddreessee() {
		
		List<Addreessee> l = null;
		try {
			
			l = addreesseeService.findAllAddreessee();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	AddrInitService addrInitService; 
	
	public List<AddrInit> autoCompleteAddrInit(String searchText) 
	{
		List<AddrInit> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=addrInitService.findAllAddrInit();
			}
			else
			{
				list=addrInitService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<AddrInit> getSelectItemsAddrInit() {
		
		List<AddrInit> l = null;
		try {
			
			l = addrInitService.findAllAddrInit();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	CircuitService circuitService; 
	
	public List<Circuit> autoCompleteCircuit(String searchText) 
	{
		List<Circuit> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=circuitService.findAllCircuit();
			}
			else
			{
				list=circuitService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Circuit> getSelectItemsCircuit() {
		
		List<Circuit> l = null;
		try {
			
			l = circuitService.findAllCircuit();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	DistrictService districtService; 
	
	public List<District> autoCompleteDistrict(String searchText) 
	{
		List<District> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=districtService.findAllDistrict();
			}
			else
			{
				list=districtService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<District> getSelectItemsDistrict() {
		
		List<District> l = null;
		try {
			
			l = districtService.findAllDistrict();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	RegionService regionService; 
	
	public List<Region> autoCompleteRegion(String searchText) 
	{
		List<Region> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=regionService.findAllRegion();
			}
			else
			{
				list=regionService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Region> getSelectItemsRegion() {
		
		List<Region> l = null;
		try {
			
			l = regionService.findAllRegion();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}

	
	@Autowired
	WardService wardService; 
	
	public List<Ward> autoCompleteWard(String searchText) 
	{
		List<Ward> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=wardService.findAllWard();
			}
			else
			{
				list=wardService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Ward> getSelectItemsWard() {
		
		List<Ward> l = null;
		try {
			
			l = wardService.findAllWard();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	ProvinceService provinceService; 
	
	public List<Province> autoCompleteProvince(String searchText) 
	{
		List<Province> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=provinceService.findAllProvince();
			}
			else
			{
				list=provinceService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Province> getSelectItemsProvince() {
		
		List<Province> l = null;
		try {
			
			l = provinceService.findAllProvince();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	SectorService sectorService; 
	
	public List<Sector> autoCompleteSector(String searchText) 
	{
		List<Sector> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=sectorService.findAllSector();
			}
			else
			{
				list=sectorService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Sector> getSelectItemsSector() {
		
		List<Sector> l = null;
		try {
			
			l = sectorService.findAllSector();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	TypeDoEService typeDoEService; 
	
	public List<TypeDoE> autoCompleteTypeDoE(String searchText) 
	{
		List<TypeDoE> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=typeDoEService.findAllTypeDoE();
			}
			else
			{
				list=typeDoEService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<TypeDoE> getSelectItemsTypeDoE() {
		
		List<TypeDoE> l = null;
		try {
			
			l = typeDoEService.findAllTypeDoE();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	PhasePEDService phasePEDService; 
	
	public List<PhasePED> autoCompletePhasePED(String searchText) 
	{
		List<PhasePED> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=phasePEDService.findAllPhasePED();
			}
			else
			{
				list=phasePEDService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<PhasePED> getSelectItemsPhasePED() {
		
		List<PhasePED> l = null;
		try {
			
			l = phasePEDService.findAllPhasePED();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	SpecialisationService specialisationService; 
	
	public List<Specialisation> autoCompleteSpecialisation(String searchText) 
	{
		List<Specialisation> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=specialisationService.findAllSpecialisation();
			}
			else
			{
				list=specialisationService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Specialisation> getSelectItemsSpecialisation() {
		
		List<Specialisation> l = null;
		try {
			
			l = specialisationService.findAllSpecialisation();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	OwnerLandService ownerLandService; 
	
	public List<OwnerLand> autoCompleteOwnerLand(String searchText) 
	{
		List<OwnerLand> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=ownerLandService.findAllOwnerLand();
			}
			else
			{
				list=ownerLandService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<OwnerLand> getSelectItemsOwnerLand() {
		
		List<OwnerLand> l = null;
		try {
			
			l = ownerLandService.findAllOwnerLand();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	OwnerBuildingService ownerBuildingService; 
	
	public List<OwnerBuilding> autoCompleteOwnerBuilding(String searchText) 
	{
		List<OwnerBuilding> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=ownerBuildingService.findAllOwnerBuilding();
			}
			else
			{
				list=ownerBuildingService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<OwnerBuilding> getSelectItemsOwnerBuilding() {
		
		List<OwnerBuilding> l = null;
		try {
			
			l = ownerBuildingService.findAllOwnerBuilding();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	ExDeptService exDeptService; 
	
	public List<ExDept> autoCompleteExDept(String searchText) 
	{
		List<ExDept> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=exDeptService.findAllExDept();
			}
			else
			{
				list=exDeptService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<ExDept> getSelectItemsExDept() {
		
		List<ExDept> l = null;
		try {
			
			l = exDeptService.findAllExDept();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	ExamCentreService examCentreService; 
	
	public List<ExamCentre> autoCompleteExamCentre(String searchText) 
	{
		List<ExamCentre> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=examCentreService.findAllExamCentre();
			}
			else
			{
				list=examCentreService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<ExamCentre> getSelectItemsExamCentre() {
		
		List<ExamCentre> l = null;
		try {
			
			l = examCentreService.findAllExamCentre();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}

	
	@Autowired
	TestAUService testAUService; 
	
	public List<TestAU> autoCompleteTestAU(String searchText) 
	{
		List<TestAU> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=testAUService.findAllTestAU();
			}
			else
			{
				list=testAUService.findByDescriptionStartingWith(searchText);
			}*/
			list=testAUService.findAllTestAU();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<TestAU> getSelectItemsTestAU() {
		
		List<TestAU> l = null;
		try {
			
			l = testAUService.findAllTestAU();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
		
	
	
	 
	 public List<SelectItem> getInstStatusEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (InstStatusEnum val : InstStatusEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	
	
	@Autowired
	TestJonhService testJonhService; 
	
	public List<TestJonh> autoCompleteTestJonh(String searchText) 
	{
		List<TestJonh> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=testJonhService.findAllTestJonh();
			}
			else
			{
				list=testJonhService.findByDescriptionStartingWith(searchText);
			}*/
			list=testJonhService.findAllTestJonh();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<TestJonh> getSelectItemsTestJonh() {
		
		List<TestJonh> l = null;
		try {
			
			l = testJonhService.findAllTestJonh();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	 
	/*
	 * public List<SelectItem> getRegistrationDateDD() { List<SelectItem> l = new
	 * ArrayList<SelectItem>(); for (RegistrationDate val :
	 * RegistrationDate.values()) { l.add(new SelectItem(val,
	 * val.getFriendlyName())); } return l; }
	 */
	
	
	@Autowired
	GautengSchoolImportService gautengSchoolImportService; 
	
	public List<GautengSchoolImport> autoCompleteGautengSchoolImport(String searchText) 
	{
		List<GautengSchoolImport> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=gautengSchoolImportService.findAllGautengSchoolImport();
			}
			else
			{
				list=gautengSchoolImportService.findByDescriptionStartingWith(searchText);
			}*/
			list=gautengSchoolImportService.findAllGautengSchoolImport();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<GautengSchoolImport> getSelectItemsGautengSchoolImport() {
		
		List<GautengSchoolImport> l = null;
		try {
			
			l = gautengSchoolImportService.findAllGautengSchoolImport();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	InstitutionService institutionService; 
	
	public List<Institution> autoCompleteInstitution(String searchText) 
	{
		List<Institution> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=institutionService.findAllInstitution();
			}
			else
			{
				list=institutionService.findByOfficialInstitutionNameStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<Institution> getSelectItemsInstitution() {
		
		List<Institution> l = null;
		try {
			
			l = institutionService.findAllInstitution();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	 
	 public List<SelectItem> getNoFeeSchoolEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (NoFeeSchoolEnum val : NoFeeSchoolEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	
	 
	 public List<SelectItem> getUrbanRuralEnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (UrbanRuralEnum val : UrbanRuralEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	
	 
	 public List<SelectItem> getSection21EnumDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (SectionEnum val : SectionEnum.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	
	@Autowired
	DeptMunNameService deptMunNameService; 
	
	public List<DeptMunName> autoCompleteDeptMunName(String searchText) 
	{
		List<DeptMunName> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=deptMunNameService.findAllDeptMunName();
			}
			else
			{
				list=deptMunNameService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<DeptMunName> getSelectItemsDeptMunName() {
		
		List<DeptMunName> l = null;
		try {
			
			l = deptMunNameService.findAllDeptMunName();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	LocalMunNameService localMunNameService; 
	
	public List<LocalMunName> autoCompleteLocalMunName(String searchText) 
	{
		List<LocalMunName> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=localMunNameService.findAllLocalMunName();
			}
			else
			{
				list=localMunNameService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<LocalMunName> getSelectItemsLocalMunName() {
		
		List<LocalMunName> l = null;
		try {
			
			l = localMunNameService.findAllLocalMunName();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}

	
	
	@Autowired
	SubPlaceService subPlaceService; 
	
	public List<SubPlace> autoCompleteSubPlace(String searchText) 
	{
		List<SubPlace> list=null;
		try {
			if(searchText==null || searchText.isEmpty())
			{
				list=subPlaceService.findAllSubPlace();
			}
			else
			{
				list=subPlaceService.findByDescriptionStartingWith(searchText);
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<SubPlace> getSelectItemsSubPlace() {
		
		List<SubPlace> l = null;
		try {
			
			l = subPlaceService.findAllSubPlace();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}

	
	
	@Autowired
	LearnersAndEducatorsService learnersAndEducatorsService; 
	
	public List<LearnersAndEducators> autoCompleteLearnersAndEducators(String searchText) 
	{
		List<LearnersAndEducators> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=learnersAndEducatorsService.findAllLearnersAndEducators();
			}
			else
			{
				list=learnersAndEducatorsService.findByDescriptionStartingWith(searchText);
			}*/
			list=learnersAndEducatorsService.findAllLearnersAndEducators();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<LearnersAndEducators> getSelectItemsLearnersAndEducators() {
		
		List<LearnersAndEducators> l = null;
		try {
			
			l = learnersAndEducatorsService.findAllLearnersAndEducators();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	
	
	@Autowired
	ReportedInstitutionsService reportedInstitutionsService; 
	
	public List<ReportedInstitutions> autoCompleteReportedInstitutions(String searchText) 
	{
		List<ReportedInstitutions> list=null;
		try {
			/*if(searchText==null || searchText.isEmpty())
			{
				list=reportedInstitutionsService.findAllReportedInstitutions();
			}
			else
			{
				list=reportedInstitutionsService.findByDescriptionStartingWith(searchText);
			}*/
			list=reportedInstitutionsService.findAllReportedInstitutions();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	 public List<ReportedInstitutions> getSelectItemsReportedInstitutions() {
		
		List<ReportedInstitutions> l = null;
		try {
			
			l = reportedInstitutionsService.findAllReportedInstitutions();
		
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
		}
		return l;
	}
	 
	 public List<SelectItem> getReportedInstitutionsStatusDD() {
		List<SelectItem> l = new ArrayList<SelectItem>();
		for (ReportedInstitutionsStatus val : ReportedInstitutionsStatus.values()) {
			l.add(new SelectItem(val, val.getFriendlyName()));
		}
		return l;
	}
	
	//===============DO NOT REMOVE THIS=================//

















































	

}
