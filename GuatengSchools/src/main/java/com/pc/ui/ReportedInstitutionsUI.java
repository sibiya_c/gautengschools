package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;
import com.pc.dao.EntityDAOFacade;

import com.pc.entities.ReportedInstitutions;
import com.pc.entities.User;
import com.pc.framework.AbstractUI;
import com.pc.service.ReportedInstitutionsService;

@Component("reportedInstitutionsUI")
@ViewScoped
public class ReportedInstitutionsUI extends AbstractUI{

	@Autowired
	ReportedInstitutionsService reportedInstitutionsService;
	private ArrayList<ReportedInstitutions> reportedInstitutionsList;
	private ReportedInstitutions reportedInstitutions;
	private LazyDataModel<ReportedInstitutions> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;
	private User activeUser;
	private String refNum;

	@PostConstruct
	public void init() {

		try {
			prepareCurrentUser();
			activeUser=currentUser;
			reportedInstitutions = new ReportedInstitutions();
			if (hasRole("ADMIN") && getParameter("user-type", false) != null
					&& getParameter("user-type", false).equals("adm")) {
				loadReportedInstitutionsInfo(false);
			} else if (hasRole("INV") && getParameter("user-type", false) != null
					&& getParameter("user-type", false).equals("inv")) {
				loadReportedInstitutionsInfo(true);
			} else if(getParameter("user-type", false) !=null) {
				ajaxRedirect("/dashboard");
			} 
		} catch (Exception e) {
			e.printStackTrace();
			displayErrorMssg(e.getMessage());
		}
	}

	public void saveReportedInstitutions()
	{
		try {
			reportedInstitutionsService.submitReportedInstitutions(reportedInstitutions);
			displayInfoMssg("Report Submitted...!!");
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void updateReportedInstitutions()
	{
		try {
			reportedInstitutionsService.saveReportedInstitutions(reportedInstitutions);
			displayInfoMssg("Update Successful...!!");
			loadReportedInstitutionsInfo(true);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteReportedInstitutions()
	{
		try {
			reportedInstitutionsService.deleteReportedInstitutions(reportedInstitutions);
			displayWarningMssg("Update Successful...!!");
			loadReportedInstitutionsInfo(true);
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void findByRefNumber()
	{
		try {
			reportedInstitutions=reportedInstitutionsService.findByRefNumber(refNum);
			if(reportedInstitutions !=null) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('reportedSchoolDialog').show();");
				context.update("reportedSchoolDialog");
			}else {
				displayWarningMssg("Invalid refence number...!!");
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<ReportedInstitutions> findAllReportedInstitutions()
	{
		List<ReportedInstitutions> list=new ArrayList<>();
		try {
			list= reportedInstitutionsService.findAllReportedInstitutions();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<ReportedInstitutions> findAllReportedInstitutionsPageable()
	{
		Pageable p=null;
		try {
			return reportedInstitutionsService.findAllReportedInstitutions(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<ReportedInstitutions> findAllReportedInstitutionsSort()
	{
		Sort s=null;
		List<ReportedInstitutions> list=new ArrayList<>();
		try {
			list =reportedInstitutionsService.findAllReportedInstitutions(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		reportedInstitutions = new ReportedInstitutions();
	}
	
	public void loadReportedInstitutionsInfo(boolean isInvestigater)
	{
		 dataModel = new LazyDataModel<ReportedInstitutions>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<ReportedInstitutions> list = new  ArrayList<ReportedInstitutions>();
			   
			   @Override 
			   public List<ReportedInstitutions> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					if(isInvestigater) {
						filters.put("investigator", currentUser);
					}
					
					list = (List<ReportedInstitutions>) entityDAOFacade.getResultList(ReportedInstitutions.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,ReportedInstitutions.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(ReportedInstitutions obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public ReportedInstitutions getRowData(String rowKey) {
			        for(ReportedInstitutions obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	
	
	public ReportedInstitutions getReportedInstitutions() {
		return reportedInstitutions;
	}

	public void setReportedInstitutions(ReportedInstitutions reportedInstitutions) {
		this.reportedInstitutions = reportedInstitutions;
	}
	
	public ArrayList<ReportedInstitutions> getReportedInstitutionsList() {
		return reportedInstitutionsList;
	}

	public void setReportedInstitutionsList(ArrayList<ReportedInstitutions> reportedInstitutionsList) {
		this.reportedInstitutionsList =reportedInstitutionsList;
	}
	public LazyDataModel<ReportedInstitutions> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<ReportedInstitutions> dataModel) {
		this.dataModel = dataModel;
	}

	public User getActiveUser() {
		return activeUser;
	}

	public void setActiveUser(User activeUser) {
		this.activeUser = activeUser;
	}

	public String getRefNum() {
		return refNum;
	}

	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}

	

}
