package com.pc.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pc.framework.AbstractUI;
import com.pc.service.UserService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
@Component
@ManagedBean(name="landingPageUI")
@ViewScoped
public class LandingPageUI extends AbstractUI{
	@PostConstruct
	public void init() {
		
	}

	public void goToSearchSchools(){
		ajaxRedirect("/searchschool");
	}

	
}
