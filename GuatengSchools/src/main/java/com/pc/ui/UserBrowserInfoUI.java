package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;
import com.pc.dao.EntityDAOFacade;

import com.pc.entities.UserBrowserInfo;
import com.pc.framework.AbstractUI;
import com.pc.service.UserBrowserInfoService;

@Component("userBrowserInfoUI")
@ViewScoped
public class UserBrowserInfoUI extends AbstractUI{

	@Autowired
	UserBrowserInfoService userBrowserInfoService;
	private ArrayList<UserBrowserInfo> userBrowserInfoList;
	private UserBrowserInfo userBrowserInfo;
	private LazyDataModel<UserBrowserInfo> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		userBrowserInfo = new UserBrowserInfo();
		loadUserBrowserInfoInfo();
	}

	public void saveUserBrowserInfo()
	{
		try {
			userBrowserInfoService.saveUserBrowserInfo(userBrowserInfo);
			displayInfoMssg("Update Successful...!!");
			loadUserBrowserInfoInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteUserBrowserInfo()
	{
		try {
			userBrowserInfoService.deleteUserBrowserInfo(userBrowserInfo);
			displayWarningMssg("Update Successful...!!");
			loadUserBrowserInfoInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<UserBrowserInfo> findAllUserBrowserInfo()
	{
		List<UserBrowserInfo> list=new ArrayList<>();
		try {
			list= userBrowserInfoService.findAllUserBrowserInfo();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<UserBrowserInfo> findAllUserBrowserInfoPageable()
	{
		Pageable p=null;
		try {
			return userBrowserInfoService.findAllUserBrowserInfo(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<UserBrowserInfo> findAllUserBrowserInfoSort()
	{
		Sort s=null;
		List<UserBrowserInfo> list=new ArrayList<>();
		try {
			list =userBrowserInfoService.findAllUserBrowserInfo(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		userBrowserInfo = new UserBrowserInfo();
	}
	
	public void loadUserBrowserInfoInfo()
	{
		 dataModel = new LazyDataModel<UserBrowserInfo>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<UserBrowserInfo> list = new  ArrayList<UserBrowserInfo>();
			   
			   @Override 
			   public List<UserBrowserInfo> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<UserBrowserInfo>) entityDAOFacade.getResultList(UserBrowserInfo.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,UserBrowserInfo.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(UserBrowserInfo obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public UserBrowserInfo getRowData(String rowKey) {
			        for(UserBrowserInfo obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	public UserBrowserInfo getUserBrowserInfo() {
		return userBrowserInfo;
	}

	public void setUserBrowserInfo(UserBrowserInfo userBrowserInfo) {
		this.userBrowserInfo = userBrowserInfo;
	}
	
	public ArrayList<UserBrowserInfo> getUserBrowserInfoList() {
		return userBrowserInfoList;
	}

	public void setUserBrowserInfoList(ArrayList<UserBrowserInfo> userBrowserInfoList) {
		this.userBrowserInfoList =userBrowserInfoList;
	}
	public LazyDataModel<UserBrowserInfo> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<UserBrowserInfo> dataModel) {
		this.dataModel = dataModel;
	}
	

}
