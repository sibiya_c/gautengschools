package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.map.OverlaySelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.map.Circle;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;

import java.util.Map;
import com.pc.dao.EntityDAOFacade;
import com.pc.entities.Address;
import com.pc.entities.Institution;
import com.pc.framework.AbstractUI;
import com.pc.service.InstitutionService;

@Component("institutionUI")
@ViewScoped
public class InstitutionUI extends AbstractUI{

	@Autowired
	InstitutionService institutionService;
	private ArrayList<Institution> institutionList;
	private Institution institution;
	private LazyDataModel<Institution> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;
	private MapModel circleModel;
	private String searchValue;
	

	@PostConstruct
	public void init() {
		institution = new Institution();
		institution.setAddress(new Address());
		loadInstitutionInfo();
		prepaMapModel();
	}

	public void saveInstitution()
	{
		try {
			institutionService.saveInstitution(institution);
			displayInfoMssg("Update Successful...!!");
			loadInstitutionInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteInstitution()
	{
		try {
			institutionService.deleteInstitution(institution);
			displayWarningMssg("Update Successful...!!");
			loadInstitutionInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Institution> findAllInstitution()
	{
		List<Institution> list=new ArrayList<>();
		try {
			list= institutionService.findAllInstitution();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Institution> findAllInstitutionPageable()
	{
		Pageable p=null;
		try {
			return institutionService.findAllInstitution(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Institution> findAllInstitutionSort()
	{
		Sort s=null;
		List<Institution> list=new ArrayList<>();
		try {
			list =institutionService.findAllInstitution(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		institution = new Institution();
	}
	
	public void searchSchool() {
		try {
			institutionList =(ArrayList<Institution>) institutionService.findByOfficialInstitutionNameStartingWith(searchValue);
			if(institutionList==null || institutionList.size()==0) {
				displayWarningMssg(searchValue+" is NOT accredited with the Gauteng Department Of Education");
			}
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void clearSearchSchool() {
		try {
			institutionList =null;
			institution = new Institution();
			searchValue=null;
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void loadInstitutionInfo()
	{
		 dataModel = new LazyDataModel<Institution>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<Institution> list = new  ArrayList<Institution>();
			   
			   @Override 
			   public List<Institution> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<Institution>) entityDAOFacade.getResultList(Institution.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,Institution.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(Institution obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public Institution getRowData(String rowKey) {
			        for(Institution obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	
	public void prepaMapModel()
	{
		try {
			circleModel = new DefaultMapModel();
			LatLng coord1 = new LatLng(36.879466, 30.667648);
			 Circle circle1 = new Circle(coord1, 500);
			    circle1.setStrokeColor("#d93c3c");
			    circle1.setFillColor("#d93c3c");
			    circle1.setFillOpacity(0.5);
			    circleModel.addOverlay(circle1);
			  
			/*if(institution.getAddress().getGisLatitude() !=null && institution.getAddress().getGisLongitude() !=null){
			    //Shared coordinates
			    //LatLng coord1 = new LatLng(36.879466, 30.667648);
			    LatLng coord1=new LatLng(Double.parseDouble(institution.getAddress().getGisLatitude()), Double.parseDouble(institution.getAddress().getGisLongitude()));
			    //Circle
			    Circle circle1 = new Circle(coord1, 500);
			    circle1.setStrokeColor("#d93c3c");
			    circle1.setFillColor("#d93c3c");
			    circle1.setFillOpacity(0.5);
			    circleModel.addOverlay(circle1);
			}*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
    public void onCircleSelect(OverlaySelectEvent event) {
    	displayInfoMssg("Institution Location");
    }
	  
	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}
	
	public ArrayList<Institution> getInstitutionList() {
		return institutionList;
	}

	public void setInstitutionList(ArrayList<Institution> institutionList) {
		this.institutionList =institutionList;
	}
	public LazyDataModel<Institution> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Institution> dataModel) {
		this.dataModel = dataModel;
	}

	public MapModel getCircleModel() {
		return circleModel;
	}

	public void setCircleModel(MapModel circleModel) {
		this.circleModel = circleModel;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}
	

}
