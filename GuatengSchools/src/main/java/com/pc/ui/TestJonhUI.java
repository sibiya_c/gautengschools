package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;
import com.pc.dao.EntityDAOFacade;

import com.pc.entities.TestJonh;
import com.pc.framework.AbstractUI;
import com.pc.service.TestJonhService;

@Component("testJonhUI")
@ViewScoped
public class TestJonhUI extends AbstractUI{

	@Autowired
	TestJonhService testJonhService;
	private ArrayList<TestJonh> testJonhList;
	private TestJonh testJonh;
	private LazyDataModel<TestJonh> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		testJonh = new TestJonh();
		loadTestJonhInfo();
	}

	public void saveTestJonh()
	{
		try {
			testJonhService.saveTestJonh(testJonh);
			displayInfoMssg("Update Successful...!!");
			loadTestJonhInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteTestJonh()
	{
		try {
			testJonhService.deleteTestJonh(testJonh);
			displayWarningMssg("Update Successful...!!");
			loadTestJonhInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<TestJonh> findAllTestJonh()
	{
		List<TestJonh> list=new ArrayList<>();
		try {
			list= testJonhService.findAllTestJonh();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<TestJonh> findAllTestJonhPageable()
	{
		Pageable p=null;
		try {
			return testJonhService.findAllTestJonh(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<TestJonh> findAllTestJonhSort()
	{
		Sort s=null;
		List<TestJonh> list=new ArrayList<>();
		try {
			list =testJonhService.findAllTestJonh(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		testJonh = new TestJonh();
	}
	
	public void loadTestJonhInfo()
	{
		 dataModel = new LazyDataModel<TestJonh>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<TestJonh> list = new  ArrayList<TestJonh>();
			   
			   @Override 
			   public List<TestJonh> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<TestJonh>) entityDAOFacade.getResultList(TestJonh.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,TestJonh.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(TestJonh obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public TestJonh getRowData(String rowKey) {
			        for(TestJonh obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	public TestJonh getTestJonh() {
		return testJonh;
	}

	public void setTestJonh(TestJonh testJonh) {
		this.testJonh = testJonh;
	}
	
	public ArrayList<TestJonh> getTestJonhList() {
		return testJonhList;
	}

	public void setTestJonhList(ArrayList<TestJonh> testJonhList) {
		this.testJonhList =testJonhList;
	}
	public LazyDataModel<TestJonh> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<TestJonh> dataModel) {
		this.dataModel = dataModel;
	}
	

}
