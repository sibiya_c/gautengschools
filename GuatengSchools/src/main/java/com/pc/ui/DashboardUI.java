package com.pc.ui;

import java.util.ArrayList;
import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.pc.entities.User;
import com.pc.entities.enums.InstStatusEnum;
import com.pc.entities.enums.ReportedInstitutionsStatus;
import com.pc.framework.AbstractUI;
import com.pc.service.InstitutionService;
import com.pc.service.LearnersAndEducatorsService;
import com.pc.service.ReportedInstitutionsService;
import com.pc.service.UserService;

@Component("dashboardUI")
@ViewScoped
public class DashboardUI extends AbstractUI{

  private String authUser="" ;
  private User currentUser;
  private Long numberOfSchools;
  private Long numberOfLearners;
  private Long numberOfEducators;
  private Long numberOfOpenSchools;
  
  private Long totalReported;
  private Long statusSubmited;
  private Long statusPendingInv;
  private Long statusClosed;
  private Long statusUnderInv;
  
  @Autowired
  UserService userService;
  @Autowired
  InstitutionService institutionService;
  @Autowired
  ReportedInstitutionsService reportedInstitutionsService;
  @Autowired
  LearnersAndEducatorsService learnersAndEducatorsService;
  private Integer leanersYear;
  private Integer educatorsYear;
  ArrayList<Integer> yearList=new ArrayList<>();
  
  @PostConstruct
  public void init() {
	  
	  try
	  {
		prepareCurrentUser();
    	authUser=showGreeting();
    	preparTotals();
	  }
	  catch (Exception e) {
		 e.printStackTrace();
		displayErrorMssg(e.getMessage());
	}
  }
  
  private void preparTotals() throws Exception {
	  int currentYear=Calendar.getInstance().get(Calendar.YEAR);
	  int startYear=currentYear-5;
	  for(int x=startYear;x<currentYear+10;x++) {
		  yearList.add(x);
	  }
	  leanersYear = currentYear;
	  educatorsYear =currentYear;
	  numberOfSchools=institutionService.count();
	  numberOfOpenSchools=institutionService.countByInstStatusEnum(InstStatusEnum.OPNE);
	  calcTotalLearners();
	  calcTotalEducators();
	  
	 totalReported=reportedInstitutionsService.count();
	 statusSubmited=reportedInstitutionsService.countByStatus(ReportedInstitutionsStatus.Submitted);
	 statusPendingInv=reportedInstitutionsService.countByStatus(ReportedInstitutionsStatus.InvestigationPending);
	 statusClosed=reportedInstitutionsService.countByStatus(ReportedInstitutionsStatus.Close);
	 statusUnderInv=reportedInstitutionsService.countByStatus(ReportedInstitutionsStatus.UnderInvestigation);
  }
  
  public void calcTotalLearners() {
	  try {
		numberOfLearners=learnersAndEducatorsService.totalLearners(leanersYear);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		displayErrorMssg(e.getMessage());
	}
  }
  
  public void calcTotalEducators() {
	  try {
		  numberOfEducators=learnersAndEducatorsService.totalEducators(educatorsYear);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		displayErrorMssg(e.getMessage());
	}
  }
    
  public String showGreeting() {
   
    return "Hello " + currentUser.getName() + " "+currentUser.getSurname()+ "!";
  }
  
  public void prepareCurrentUser() throws Exception
  {
	  Authentication authentication =
		        SecurityContextHolder.getContext().getAuthentication();
	  currentUser= userService.findByEmail(authentication.getName() );
  }



public String getAuthUser() {
	return authUser;
}



public void setAuthUser(String authUser) {
	this.authUser = authUser;
}

public User getCurrentUser() {
	return currentUser;
}

public void setCurrentUser(User currentUser) {
	this.currentUser = currentUser;
}

public Long getNumberOfSchools() {
	return numberOfSchools;
}

public void setNumberOfSchools(Long numberOfSchools) {
	this.numberOfSchools = numberOfSchools;
}

public Long getNumberOfLearners() {
	return numberOfLearners;
}

public void setNumberOfLearners(Long numberOfLearners) {
	this.numberOfLearners = numberOfLearners;
}

public Long getNumberOfEducators() {
	return numberOfEducators;
}

public void setNumberOfEducators(Long numberOfEducators) {
	this.numberOfEducators = numberOfEducators;
}

public Long getNumberOfOpenSchools() {
	return numberOfOpenSchools;
}

public void setNumberOfOpenSchools(Long numberOfOpenSchools) {
	this.numberOfOpenSchools = numberOfOpenSchools;
}

public Integer getLeanersYear() {
	return leanersYear;
}

public void setLeanersYear(Integer leanersYear) {
	this.leanersYear = leanersYear;
}

public Integer getEducatorsYear() {
	return educatorsYear;
}

public void setEducatorsYear(Integer educatorsYear) {
	this.educatorsYear = educatorsYear;
}

public ArrayList<Integer> getYearList() {
	return yearList;
}

public void setYearList(ArrayList<Integer> yearList) {
	this.yearList = yearList;
}

public Long getTotalReported() {
	return totalReported;
}

public void setTotalReported(Long totalReported) {
	this.totalReported = totalReported;
}

public Long getStatusSubmited() {
	return statusSubmited;
}

public void setStatusSubmited(Long statusSubmited) {
	this.statusSubmited = statusSubmited;
}

public Long getStatusPendingInv() {
	return statusPendingInv;
}

public void setStatusPendingInv(Long statusPendingInv) {
	this.statusPendingInv = statusPendingInv;
}

public Long getStatusClosed() {
	return statusClosed;
}

public void setStatusClosed(Long statusClosed) {
	this.statusClosed = statusClosed;
}

public Long getStatusUnderInv() {
	return statusUnderInv;
}

public void setStatusUnderInv(Long statusUnderInv) {
	this.statusUnderInv = statusUnderInv;
}


}
