package com.pc.ui;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import java.util.Map;
import com.pc.dao.EntityDAOFacade;

import com.pc.entities.Address;
import com.pc.framework.AbstractUI;
import com.pc.service.AddressService;

@Component("addressUI")
@ViewScoped
public class AddressUI extends AbstractUI{

	@Autowired
	AddressService addressService;
	private ArrayList<Address> addressList;
	private Address address;
	private LazyDataModel<Address> dataModel;
	@Autowired
	EntityDAOFacade entityDAOFacade;

	@PostConstruct
	public void init() {
		address = new Address();
		loadAddressInfo();
	}

	public void saveAddress()
	{
		try {
			addressService.saveAddress(address);
			displayInfoMssg("Update Successful...!!");
			loadAddressInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void deleteAddress()
	{
		try {
			addressService.deleteAddress(address);
			displayWarningMssg("Update Successful...!!");
			loadAddressInfo();
			reset();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public List<Address> findAllAddress()
	{
		List<Address> list=new ArrayList<>();
		try {
			list= addressService.findAllAddress();
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		
		return list;
	}
	
	public Page<Address> findAllAddressPageable()
	{
		Pageable p=null;
		try {
			return addressService.findAllAddress(p);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	public List<Address> findAllAddressSort()
	{
		Sort s=null;
		List<Address> list=new ArrayList<>();
		try {
			list =addressService.findAllAddress(s);
		} catch (Exception e) {
			displayErrorMssg(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	public void reset() {
		address = new Address();
	}
	
	public void loadAddressInfo()
	{
		 dataModel = new LazyDataModel<Address>() { 
			 
			   private static final long serialVersionUID = 1L; 
			   private List<Address> list = new  ArrayList<Address>();
			   
			   @Override 
			   public List<Address> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters)  { 
			   
				try {
					list = (List<Address>) entityDAOFacade.getResultList(Address.class,first, pageSize, sortField, sortOrder, filters);
					dataModel.setRowCount(entityDAOFacade.count(filters,Address.class));
				} catch (Exception e) {
					logger.fatal(e);
				} 
			    return list; 
			   }
			   
			    @Override
			    public Object getRowKey(Address obj) {
			        return obj.getId();
			    }
			    
			    @Override
			    public Address getRowData(String rowKey) {
			        for(Address obj : list) {
			            if(obj.getId().equals(Long.valueOf(rowKey)))
			                return obj;
			        }
			        return null;
			    }			    
			    
			  }; 
			
	}
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public ArrayList<Address> getAddressList() {
		return addressList;
	}

	public void setAddressList(ArrayList<Address> addressList) {
		this.addressList =addressList;
	}
	public LazyDataModel<Address> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Address> dataModel) {
		this.dataModel = dataModel;
	}
	

}
