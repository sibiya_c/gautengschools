package com.pc.model;

public class AppReportedInstitution {
    private String schoolName;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;
    private String suburbArea;
    private String postalCode;
    private String reporterName;
    private String reporterSurname;
    private String reporterPhone;
    private String reporterEmail;

    public AppReportedInstitution() {
    }

    public AppReportedInstitution(String schoolName, String addressLine1, String addressLine2, String addressLine3, String suburbArea, String postalCode, String reporterName, String reporterSurname, String reporterPhone, String reporterEmail) {
        this.schoolName = schoolName;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressLine3 = addressLine3;
        this.suburbArea = suburbArea;
        this.postalCode = postalCode;
        this.reporterName = reporterName;
        this.reporterSurname = reporterSurname;
        this.reporterPhone = reporterPhone;
        this.reporterEmail = reporterEmail;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getSuburbArea() {
        return suburbArea;
    }

    public void setSuburbArea(String suburbArea) {
        this.suburbArea = suburbArea;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getReporterName() {
        return reporterName;
    }

    public void setReporterName(String reporterName) {
        this.reporterName = reporterName;
    }

    public String getReporterSurname() {
        return reporterSurname;
    }

    public void setReporterSurname(String reporterSurname) {
        this.reporterSurname = reporterSurname;
    }

    public String getReporterPhone() {
        return reporterPhone;
    }

    public void setReporterPhone(String reporterPhone) {
        this.reporterPhone = reporterPhone;
    }

    public String getReporterEmail() {
        return reporterEmail;
    }

    public void setReporterEmail(String reporterEmail) {
        this.reporterEmail = reporterEmail;
    }
}
