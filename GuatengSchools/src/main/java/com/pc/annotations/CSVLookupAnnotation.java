package com.pc.annotations;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface CSVLookupAnnotation {

	public Class<?> className() default Object.class;

	public String method() default "";
	
	public Class<?> paramClass() default Object.class;
	
	public String lookupField() default "";

}
