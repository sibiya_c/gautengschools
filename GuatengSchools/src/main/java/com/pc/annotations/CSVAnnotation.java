package com.pc.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface CSVAnnotation {
	public String name() default "";
	public Class<?> className() default Object.class;
	public String datePattern() default "dd/MM/yyyy hh:mm";
	public boolean required() default false;
	public boolean process() default false;
	public String lookupField() default "";
	public int length() default 0;
	public String numericPattern() default "%.2f";
}