package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.LocalMunName;
import com.pc.entities.lookup.Province;
import com.pc.repositories.lookup.LocalMunNameRepository;
import com.pc.framework.AbstractService;

@Service
public class LocalMunNameService extends AbstractService{
	@Autowired
	LocalMunNameRepository repository;
	
	public void saveLocalMunName(LocalMunName localMunName)  throws Exception
	{
	    if(localMunName.getId()==null)
		{
			localMunName.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){localMunName.setLastUpdateUser(getCurrentUser());}
			localMunName.setLastUpdateDate(new Date());
		}
		repository.save(localMunName);
	}
	
	public void deleteLocalMunName(LocalMunName localMunName)  throws Exception
	{
		repository.delete(localMunName);
	}
	
	public void deleteLocalMunNameByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<LocalMunName> findAllLocalMunName()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<LocalMunName> findAllLocalMunName(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<LocalMunName> findAllLocalMunName(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public LocalMunName findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<LocalMunName> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	
	public LocalMunName findByDescription(String desc) throws Exception
	{
		LocalMunName localMunName=null;
		List<LocalMunName> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			localMunName=list.get(0);
		}
		return localMunName;
	}
	
	
	
	

}
