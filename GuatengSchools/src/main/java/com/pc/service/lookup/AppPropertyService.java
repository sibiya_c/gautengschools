package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.AppProperty;
import com.pc.repositories.lookup.AppPropertyRepository;
import com.pc.framework.AbstractService;

@Service
public class AppPropertyService extends AbstractService{
	@Autowired
	AppPropertyRepository repository;
	
	public void saveAppProperty(AppProperty appProperty)  throws Exception
	{
	    if(appProperty.getId()==null)
		{
			appProperty.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){appProperty.setLastUpdateUser(getCurrentUser());}
			appProperty.setLastUpdateDate(new Date());
		}
		repository.save(appProperty);
	}
	
	public void deleteAppProperty(AppProperty appProperty)  throws Exception
	{
		repository.delete(appProperty);
	}
	
	public void deleteAppPropertyByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<AppProperty> findAllAppProperty()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<AppProperty> findAllAppProperty(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<AppProperty> findAllAppProperty(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public AppProperty findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<AppProperty> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	
	

}
