package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.TownCity;
import com.pc.entities.lookup.TownshipVillage;
import com.pc.repositories.lookup.TownshipVillageRepository;
import com.pc.framework.AbstractService;

@Service
public class TownshipVillageService extends AbstractService{
	@Autowired
	TownshipVillageRepository repository;
	
	public void saveTownshipVillage(TownshipVillage townshipVillage)  throws Exception
	{
	    if(townshipVillage.getId()==null)
		{
			townshipVillage.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){townshipVillage.setLastUpdateUser(getCurrentUser());}
			townshipVillage.setLastUpdateDate(new Date());
		}
		repository.save(townshipVillage);
	}
	
	public void deleteTownshipVillage(TownshipVillage townshipVillage)  throws Exception
	{
		repository.delete(townshipVillage);
	}
	
	public void deleteTownshipVillageByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<TownshipVillage> findAllTownshipVillage()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<TownshipVillage> findAllTownshipVillage(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<TownshipVillage> findAllTownshipVillage(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public TownshipVillage findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<TownshipVillage> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public TownshipVillage findByDescription(String desc) throws Exception
	{
		TownshipVillage townshipVillage=null;
		List<TownshipVillage> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			townshipVillage=list.get(0);
		}
		return townshipVillage;
	}
	
	
	

}
