package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.Title;
import com.pc.repositories.lookup.TitleRepository;
import com.pc.framework.AbstractService;

@Service
public class TitleService extends AbstractService{
	@Autowired
	TitleRepository repository;
	
	public void saveTitle(Title title)  throws Exception
	{
	    if(title.getId()==null)
		{
			title.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){title.setLastUpdateUser(getCurrentUser());}
			title.setLastUpdateDate(new Date());
		}
		repository.save(title);
	}
	
	public void deleteTitle(Title title)  throws Exception
	{
		repository.delete(title);
	}
	
	public void deleteTitleByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Title> findAllTitle()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Title> findAllTitle(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Title> findAllTitle(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Title findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<Title> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	
	

}
