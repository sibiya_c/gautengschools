package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.AddrInit;
import com.pc.entities.lookup.Circuit;
import com.pc.repositories.lookup.CircuitRepository;
import com.pc.framework.AbstractService;

@Service
public class CircuitService extends AbstractService{
	@Autowired
	CircuitRepository repository;
	
	public void saveCircuit(Circuit circuit)  throws Exception
	{
	    if(circuit.getId()==null)
		{
			circuit.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){circuit.setLastUpdateUser(getCurrentUser());}
			circuit.setLastUpdateDate(new Date());
		}
		repository.save(circuit);
	}
	
	public void deleteCircuit(Circuit circuit)  throws Exception
	{
		repository.delete(circuit);
	}
	
	public void deleteCircuitByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Circuit> findAllCircuit()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Circuit> findAllCircuit(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Circuit> findAllCircuit(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Circuit findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<Circuit> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public Circuit findByDescription(String desc) throws Exception
	{
		Circuit circuit=null;
		List<Circuit> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			circuit=list.get(0);
		}
		return circuit;
	}
	
	
	

}
