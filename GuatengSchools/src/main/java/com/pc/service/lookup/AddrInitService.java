package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.AddrInit;
import com.pc.entities.lookup.Addreessee;
import com.pc.repositories.lookup.AddrInitRepository;
import com.pc.framework.AbstractService;

@Service
public class AddrInitService extends AbstractService{
	@Autowired
	AddrInitRepository repository;
	
	public void saveAddrInit(AddrInit addrInit)  throws Exception
	{
	    if(addrInit.getId()==null)
		{
			addrInit.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){addrInit.setLastUpdateUser(getCurrentUser());}
			addrInit.setLastUpdateDate(new Date());
		}
		repository.save(addrInit);
	}
	
	public void deleteAddrInit(AddrInit addrInit)  throws Exception
	{
		repository.delete(addrInit);
	}
	
	public void deleteAddrInitByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<AddrInit> findAllAddrInit()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<AddrInit> findAllAddrInit(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<AddrInit> findAllAddrInit(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public AddrInit findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<AddrInit> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public AddrInit findByDescription(String desc) throws Exception
	{
		AddrInit addrInit=null;
		List<AddrInit> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			addrInit=list.get(0);
		}
		return addrInit;
	}
	
	
	
	

}
