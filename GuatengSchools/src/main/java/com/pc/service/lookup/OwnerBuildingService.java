package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.LocalMunName;
import com.pc.entities.lookup.OwnerBuilding;
import com.pc.repositories.lookup.OwnerBuildingRepository;
import com.pc.framework.AbstractService;

@Service
public class OwnerBuildingService extends AbstractService{
	@Autowired
	OwnerBuildingRepository repository;
	
	public void saveOwnerBuilding(OwnerBuilding ownerBuilding)  throws Exception
	{
	    if(ownerBuilding.getId()==null)
		{
			ownerBuilding.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){ownerBuilding.setLastUpdateUser(getCurrentUser());}
			ownerBuilding.setLastUpdateDate(new Date());
		}
		repository.save(ownerBuilding);
	}
	
	public void deleteOwnerBuilding(OwnerBuilding ownerBuilding)  throws Exception
	{
		repository.delete(ownerBuilding);
	}
	
	public void deleteOwnerBuildingByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<OwnerBuilding> findAllOwnerBuilding()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<OwnerBuilding> findAllOwnerBuilding(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<OwnerBuilding> findAllOwnerBuilding(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public OwnerBuilding findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<OwnerBuilding> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public OwnerBuilding findByDescription(String desc) throws Exception
	{
		OwnerBuilding ownerBuilding=null;
		List<OwnerBuilding> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			ownerBuilding=list.get(0);
		}
		return ownerBuilding;
	}
	
	
	

}
