package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.PhasePED;
import com.pc.entities.lookup.Province;
import com.pc.repositories.lookup.ProvinceRepository;
import com.pc.framework.AbstractService;

@Service
public class ProvinceService extends AbstractService{
	@Autowired
	ProvinceRepository repository;
	
	public void saveProvince(Province province)  throws Exception
	{
	    if(province.getId()==null)
		{
			province.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){province.setLastUpdateUser(getCurrentUser());}
			province.setLastUpdateDate(new Date());
		}
		repository.save(province);
	}
	
	public void deleteProvince(Province province)  throws Exception
	{
		repository.delete(province);
	}
	
	public void deleteProvinceByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Province> findAllProvince()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Province> findAllProvince(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Province> findAllProvince(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Province findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<Province> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public Province findByDescription(String desc) throws Exception
	{
		Province province=null;
		List<Province> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			province=list.get(0);
		}
		return province;
	}
	
	public Province findByCode(String code) throws Exception
	{
		return repository.findByCode(code);
	}
	
	
	

}
