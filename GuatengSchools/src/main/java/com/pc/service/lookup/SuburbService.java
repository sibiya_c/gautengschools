package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.SubPlace;
import com.pc.entities.lookup.Suburb;
import com.pc.repositories.lookup.SuburbRepository;
import com.pc.framework.AbstractService;

@Service
public class SuburbService extends AbstractService{
	@Autowired
	SuburbRepository repository;
	
	public void saveSuburb(Suburb suburb)  throws Exception
	{
	    if(suburb.getId()==null)
		{
			suburb.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){suburb.setLastUpdateUser(getCurrentUser());}
			suburb.setLastUpdateDate(new Date());
		}
		repository.save(suburb);
	}
	
	public void deleteSuburb(Suburb suburb)  throws Exception
	{
		repository.delete(suburb);
	}
	
	public void deleteSuburbByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Suburb> findAllSuburb()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Suburb> findAllSuburb(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Suburb> findAllSuburb(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Suburb findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<Suburb> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public Suburb findByDescription(String desc) throws Exception
	{
		Suburb suburb=null;
		List<Suburb> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			suburb=list.get(0);
		}
		return suburb;
	}
	
	
	

}
