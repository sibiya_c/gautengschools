package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.Sector;
import com.pc.entities.lookup.Specialisation;
import com.pc.repositories.lookup.SpecialisationRepository;
import com.pc.framework.AbstractService;

@Service
public class SpecialisationService extends AbstractService{
	@Autowired
	SpecialisationRepository repository;
	
	public void saveSpecialisation(Specialisation specialisation)  throws Exception
	{
	    if(specialisation.getId()==null)
		{
			specialisation.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){specialisation.setLastUpdateUser(getCurrentUser());}
			specialisation.setLastUpdateDate(new Date());
		}
		repository.save(specialisation);
	}
	
	public void deleteSpecialisation(Specialisation specialisation)  throws Exception
	{
		repository.delete(specialisation);
	}
	
	public void deleteSpecialisationByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Specialisation> findAllSpecialisation()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Specialisation> findAllSpecialisation(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Specialisation> findAllSpecialisation(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Specialisation findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<Specialisation> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public Specialisation findByDescription(String desc) throws Exception
	{
		Specialisation specialisation=null;
		List<Specialisation> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			specialisation=list.get(0);
		}
		return specialisation;
	}
	
	
	

}
