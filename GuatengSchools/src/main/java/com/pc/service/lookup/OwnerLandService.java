package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.OwnerBuilding;
import com.pc.entities.lookup.OwnerLand;
import com.pc.repositories.lookup.OwnerLandRepository;
import com.pc.framework.AbstractService;

@Service
public class OwnerLandService extends AbstractService{
	@Autowired
	OwnerLandRepository repository;
	
	public void saveOwnerLand(OwnerLand ownerLand)  throws Exception
	{
	    if(ownerLand.getId()==null)
		{
			ownerLand.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){ownerLand.setLastUpdateUser(getCurrentUser());}
			ownerLand.setLastUpdateDate(new Date());
		}
		repository.save(ownerLand);
	}
	
	public void deleteOwnerLand(OwnerLand ownerLand)  throws Exception
	{
		repository.delete(ownerLand);
	}
	
	public void deleteOwnerLandByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<OwnerLand> findAllOwnerLand()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<OwnerLand> findAllOwnerLand(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<OwnerLand> findAllOwnerLand(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public OwnerLand findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<OwnerLand> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public OwnerLand findByDescription(String desc) throws Exception
	{
		OwnerLand ownerLand=null;
		List<OwnerLand> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			ownerLand=list.get(0);
		}
		return ownerLand;
	}
	
	
	

}
