package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.Province;
import com.pc.entities.lookup.Region;
import com.pc.repositories.lookup.RegionRepository;
import com.pc.framework.AbstractService;

@Service
public class RegionService extends AbstractService{
	@Autowired
	RegionRepository repository;
	
	public void saveRegion(Region region)  throws Exception
	{
	    if(region.getId()==null)
		{
			region.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){region.setLastUpdateUser(getCurrentUser());}
			region.setLastUpdateDate(new Date());
		}
		repository.save(region);
	}
	
	public void deleteRegion(Region region)  throws Exception
	{
		repository.delete(region);
	}
	
	public void deleteRegionByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Region> findAllRegion()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Region> findAllRegion(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Region> findAllRegion(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Region findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<Region> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public Region findByDescription(String desc) throws Exception
	{
		Region region=null;
		List<Region> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			region=list.get(0);
		}
		return region;
	}
	
	
	

}
