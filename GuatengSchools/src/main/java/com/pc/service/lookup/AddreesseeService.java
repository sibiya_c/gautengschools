package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.Addreessee;
import com.pc.repositories.lookup.AddreesseeRepository;
import com.pc.framework.AbstractService;

@Service
public class AddreesseeService extends AbstractService{
	@Autowired
	AddreesseeRepository repository;
	
	public void saveAddreessee(Addreessee addreessee)  throws Exception
	{
	    if(addreessee.getId()==null)
		{
			addreessee.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){addreessee.setLastUpdateUser(getCurrentUser());}
			addreessee.setLastUpdateDate(new Date());
		}
		repository.save(addreessee);
	}
	
	public void deleteAddreessee(Addreessee addreessee)  throws Exception
	{
		repository.delete(addreessee);
	}
	
	public void deleteAddreesseeByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Addreessee> findAllAddreessee()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Addreessee> findAllAddreessee(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Addreessee> findAllAddreessee(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Addreessee findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<Addreessee> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public Addreessee findByDescription(String desc) throws Exception
	{
		Addreessee addreessee=null;
		List<Addreessee> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			addreessee=list.get(0);
		}
		return addreessee;
	}
	
	
	
	

}
