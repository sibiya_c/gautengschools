package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.Role;
import com.pc.repositories.lookup.RoleRepository;
import com.pc.framework.AbstractService;

@Service
public class RoleService extends AbstractService{
	@Autowired
	RoleRepository repository;
	
	public void saveRole(Role role)  throws Exception
	{
	    if(role.getId()==null)
		{
			role.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){role.setLastUpdateUser(getCurrentUser());}
			role.setLastUpdateDate(new Date());
		}
		repository.save(role);
	}
	
	public void deleteRole(Role role)  throws Exception
	{
		repository.delete(role);
	}
	
	public void deleteRoleByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Role> findAllRole()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Role> findAllRole(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Role> findAllRole(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Role findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<Role> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}

	public Role findByCode(String code) throws Exception {
		return repository.findByCode(code);
	}
	
	
	

}
