package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.Circuit;
import com.pc.entities.lookup.District;
import com.pc.repositories.lookup.DistrictRepository;
import com.pc.framework.AbstractService;

@Service
public class DistrictService extends AbstractService{
	@Autowired
	DistrictRepository repository;
	
	public void saveDistrict(District district)  throws Exception
	{
	    if(district.getId()==null)
		{
			district.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){district.setLastUpdateUser(getCurrentUser());}
			district.setLastUpdateDate(new Date());
		}
		repository.save(district);
	}
	
	public void deleteDistrict(District district)  throws Exception
	{
		repository.delete(district);
	}
	
	public void deleteDistrictByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<District> findAllDistrict()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<District> findAllDistrict(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<District> findAllDistrict(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public District findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<District> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public District findByDescription(String desc) throws Exception
	{
		District district=null;
		List<District> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			district=list.get(0);
		}
		return district;
	}
	
	
	

}
