package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.Suburb;
import com.pc.entities.lookup.TownCity;
import com.pc.repositories.lookup.TownCityRepository;
import com.pc.framework.AbstractService;

@Service
public class TownCityService extends AbstractService{
	@Autowired
	TownCityRepository repository;
	
	public void saveTownCity(TownCity townCity)  throws Exception
	{
	    if(townCity.getId()==null)
		{
			townCity.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){townCity.setLastUpdateUser(getCurrentUser());}
			townCity.setLastUpdateDate(new Date());
		}
		repository.save(townCity);
	}
	
	public void deleteTownCity(TownCity townCity)  throws Exception
	{
		repository.delete(townCity);
	}
	
	public void deleteTownCityByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<TownCity> findAllTownCity()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<TownCity> findAllTownCity(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<TownCity> findAllTownCity(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public TownCity findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<TownCity> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public TownCity findByDescription(String desc) throws Exception
	{
		TownCity townCity=null;
		List<TownCity> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			townCity=list.get(0);
		}
		return townCity;
	}
	
	
	

}
