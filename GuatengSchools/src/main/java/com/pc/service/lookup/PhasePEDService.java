package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.OwnerLand;
import com.pc.entities.lookup.PhasePED;
import com.pc.repositories.lookup.PhasePEDRepository;
import com.pc.framework.AbstractService;

@Service
public class PhasePEDService extends AbstractService{
	@Autowired
	PhasePEDRepository repository;
	
	public void savePhasePED(PhasePED phasePED)  throws Exception
	{
	    if(phasePED.getId()==null)
		{
			phasePED.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){phasePED.setLastUpdateUser(getCurrentUser());}
			phasePED.setLastUpdateDate(new Date());
		}
		repository.save(phasePED);
	}
	
	public void deletePhasePED(PhasePED phasePED)  throws Exception
	{
		repository.delete(phasePED);
	}
	
	public void deletePhasePEDByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<PhasePED> findAllPhasePED()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<PhasePED> findAllPhasePED(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<PhasePED> findAllPhasePED(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public PhasePED findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<PhasePED> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public PhasePED findByDescription(String desc) throws Exception
	{
		PhasePED phasePED=null;
		List<PhasePED> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			phasePED=list.get(0);
		}
		return phasePED;
	}
	
	
	

}
