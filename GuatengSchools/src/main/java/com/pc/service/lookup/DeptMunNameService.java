package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.Circuit;
import com.pc.entities.lookup.DeptMunName;
import com.pc.repositories.lookup.DeptMunNameRepository;
import com.pc.framework.AbstractService;

@Service
public class DeptMunNameService extends AbstractService{
	@Autowired
	DeptMunNameRepository repository;
	
	public void saveDeptMunName(DeptMunName deptMunName)  throws Exception
	{
	    if(deptMunName.getId()==null)
		{
			deptMunName.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){deptMunName.setLastUpdateUser(getCurrentUser());}
			deptMunName.setLastUpdateDate(new Date());
		}
		repository.save(deptMunName);
	}
	
	public void deleteDeptMunName(DeptMunName deptMunName)  throws Exception
	{
		repository.delete(deptMunName);
	}
	
	public void deleteDeptMunNameByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<DeptMunName> findAllDeptMunName()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<DeptMunName> findAllDeptMunName(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<DeptMunName> findAllDeptMunName(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public DeptMunName findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<DeptMunName> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public DeptMunName findByDescription(String desc) throws Exception
	{
		DeptMunName deptMunName=null;
		List<DeptMunName> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			deptMunName=list.get(0);
		}
		return deptMunName;
	}
	
	
	

}
