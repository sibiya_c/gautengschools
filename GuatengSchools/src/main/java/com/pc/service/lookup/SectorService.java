package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.Region;
import com.pc.entities.lookup.Sector;
import com.pc.repositories.lookup.SectorRepository;
import com.pc.framework.AbstractService;

@Service
public class SectorService extends AbstractService{
	@Autowired
	SectorRepository repository;
	
	public void saveSector(Sector sector)  throws Exception
	{
	    if(sector.getId()==null)
		{
			sector.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){sector.setLastUpdateUser(getCurrentUser());}
			sector.setLastUpdateDate(new Date());
		}
		repository.save(sector);
	}
	
	public void deleteSector(Sector sector)  throws Exception
	{
		repository.delete(sector);
	}
	
	public void deleteSectorByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Sector> findAllSector()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Sector> findAllSector(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Sector> findAllSector(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Sector findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<Sector> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public Sector findByDescription(String desc) throws Exception
	{
		Sector sector=null;
		List<Sector> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			sector=list.get(0);
		}
		return sector;
	}
	
	
	
	

}
