package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.ExDept;
import com.pc.entities.lookup.ExamCentre;
import com.pc.repositories.lookup.ExDeptRepository;
import com.pc.framework.AbstractService;

@Service
public class ExDeptService extends AbstractService{
	@Autowired
	ExDeptRepository repository;
	
	public void saveExDept(ExDept exDept)  throws Exception
	{
	    if(exDept.getId()==null)
		{
			exDept.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){exDept.setLastUpdateUser(getCurrentUser());}
			exDept.setLastUpdateDate(new Date());
		}
		repository.save(exDept);
	}
	
	public void deleteExDept(ExDept exDept)  throws Exception
	{
		repository.delete(exDept);
	}
	
	public void deleteExDeptByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<ExDept> findAllExDept()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<ExDept> findAllExDept(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<ExDept> findAllExDept(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public ExDept findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<ExDept> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public ExDept findByDescription(String desc) throws Exception
	{
		ExDept exDept=null;
		List<ExDept> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			exDept=list.get(0);
		}
		return exDept;
	}
	
	
	
	

}
