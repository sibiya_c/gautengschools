package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.TownshipVillage;
import com.pc.entities.lookup.TypeDoE;
import com.pc.repositories.lookup.TypeDoERepository;
import com.pc.framework.AbstractService;

@Service
public class TypeDoEService extends AbstractService{
	@Autowired
	TypeDoERepository repository;
	
	public void saveTypeDoE(TypeDoE typeDoE)  throws Exception
	{
	    if(typeDoE.getId()==null)
		{
			typeDoE.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){typeDoE.setLastUpdateUser(getCurrentUser());}
			typeDoE.setLastUpdateDate(new Date());
		}
		repository.save(typeDoE);
	}
	
	public void deleteTypeDoE(TypeDoE typeDoE)  throws Exception
	{
		repository.delete(typeDoE);
	}
	
	public void deleteTypeDoEByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<TypeDoE> findAllTypeDoE()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<TypeDoE> findAllTypeDoE(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<TypeDoE> findAllTypeDoE(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public TypeDoE findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<TypeDoE> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public TypeDoE findByDescription(String desc) throws Exception
	{
		TypeDoE typeDoE=null;
		List<TypeDoE> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			typeDoE=list.get(0);
		}
		return typeDoE;
	}
	
	
	

}
