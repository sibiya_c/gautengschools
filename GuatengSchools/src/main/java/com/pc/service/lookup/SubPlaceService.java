package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.Sector;
import com.pc.entities.lookup.SubPlace;
import com.pc.repositories.lookup.SubPlaceRepository;
import com.pc.framework.AbstractService;

@Service
public class SubPlaceService extends AbstractService{
	@Autowired
	SubPlaceRepository repository;
	
	public void saveSubPlace(SubPlace subPlace)  throws Exception
	{
	    if(subPlace.getId()==null)
		{
			subPlace.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){subPlace.setLastUpdateUser(getCurrentUser());}
			subPlace.setLastUpdateDate(new Date());
		}
		repository.save(subPlace);
	}
	
	public void deleteSubPlace(SubPlace subPlace)  throws Exception
	{
		repository.delete(subPlace);
	}
	
	public void deleteSubPlaceByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<SubPlace> findAllSubPlace()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<SubPlace> findAllSubPlace(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<SubPlace> findAllSubPlace(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public SubPlace findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<SubPlace> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public SubPlace findByDescription(String desc) throws Exception
	{
		SubPlace subPlace=null;
		List<SubPlace> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			subPlace=list.get(0);
		}
		return subPlace;
	}
	
	

}
