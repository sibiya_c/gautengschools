package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.TypeDoE;
import com.pc.entities.lookup.Ward;
import com.pc.repositories.lookup.WardRepository;
import com.pc.framework.AbstractService;

@Service
public class WardService extends AbstractService{
	@Autowired
	WardRepository repository;
	
	public void saveWard(Ward ward)  throws Exception
	{
	    if(ward.getId()==null)
		{
			ward.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){ward.setLastUpdateUser(getCurrentUser());}
			ward.setLastUpdateDate(new Date());
		}
		repository.save(ward);
	}
	
	public void deleteWard(Ward ward)  throws Exception
	{
		repository.delete(ward);
	}
	
	public void deleteWardByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Ward> findAllWard()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Ward> findAllWard(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Ward> findAllWard(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Ward findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<Ward> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public Ward findByDescription(String desc) throws Exception
	{
		Ward ward=null;
		List<Ward> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			ward=list.get(0);
		}
		return ward;
	}
	
	
	

}
