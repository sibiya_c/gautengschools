package com.pc.service.lookup;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.lookup.DeptMunName;
import com.pc.entities.lookup.ExamCentre;
import com.pc.repositories.lookup.ExamCentreRepository;
import com.pc.framework.AbstractService;

@Service
public class ExamCentreService extends AbstractService{
	@Autowired
	ExamCentreRepository repository;
	
	public void saveExamCentre(ExamCentre examCentre)  throws Exception
	{
	    if(examCentre.getId()==null)
		{
			examCentre.setCreateDate(new Date());
		}
		else{
			
			if(getCurrentUser() !=null){examCentre.setLastUpdateUser(getCurrentUser());}
			examCentre.setLastUpdateDate(new Date());
		}
		repository.save(examCentre);
	}
	
	public void deleteExamCentre(ExamCentre examCentre)  throws Exception
	{
		repository.delete(examCentre);
	}
	
	public void deleteExamCentreByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<ExamCentre> findAllExamCentre()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<ExamCentre> findAllExamCentre(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<ExamCentre> findAllExamCentre(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public ExamCentre findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public List<ExamCentre> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}
	
	public ExamCentre findByDescription(String desc) throws Exception
	{
		ExamCentre examCentre=null;
		List<ExamCentre> list=repository.findByDescription(desc);
		if(list !=null && list.size()>0) {
			examCentre=list.get(0);
		}
		return examCentre;
	}
	
	
	
	

}
