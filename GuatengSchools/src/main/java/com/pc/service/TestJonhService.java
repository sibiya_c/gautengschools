package com.pc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.TestJonh;
import com.pc.repositories.TestJonhRepository;
import com.pc.framework.AbstractService;

@Service
public class TestJonhService extends AbstractService{
	@Autowired
	TestJonhRepository repository;
	
	public void saveTestJonh(TestJonh testJonh)  throws Exception
	{
	    if(testJonh.getId()==null)
		{
			testJonh.setCreateDate(new Date());
		}else{
			
			if(getCurrentUser() !=null){testJonh.setLastUpdateUser(getCurrentUser());}
			testJonh.setLastUpdateDate(new Date());
		}
		repository.save(testJonh);
	}
	
	public void deleteTestJonh(TestJonh testJonh)  throws Exception
	{
		repository.delete(testJonh);
	}
	
	public void deleteTestJonhByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<TestJonh> findAllTestJonh()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<TestJonh> findAllTestJonh(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<TestJonh> findAllTestJonh(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public TestJonh findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	/*public List<TestJonh> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}*/
	
	
	

}
