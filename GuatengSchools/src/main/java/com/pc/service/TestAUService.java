package com.pc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.TestAU;
import com.pc.repositories.TestAURepository;
import com.pc.framework.AbstractService;

@Service
public class TestAUService extends AbstractService{
	@Autowired
	TestAURepository repository;
	
	public void saveTestAU(TestAU testAU)  throws Exception
	{
	    if(testAU.getId()==null)
		{
			testAU.setCreateDate(new Date());
		}else{
			
			if(getCurrentUser() !=null){testAU.setLastUpdateUser(getCurrentUser());}
			testAU.setLastUpdateDate(new Date());
		}
		repository.save(testAU);
	}
	
	public void deleteTestAU(TestAU testAU)  throws Exception
	{
		repository.delete(testAU);
	}
	
	public void deleteTestAUByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<TestAU> findAllTestAU()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<TestAU> findAllTestAU(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<TestAU> findAllTestAU(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public TestAU findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	/*public List<TestAU> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}*/
	
	
	

}
