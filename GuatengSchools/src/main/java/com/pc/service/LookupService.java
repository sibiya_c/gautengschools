package com.pc.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.pc.beans.LookupMenuBeans;
import com.pc.constants.AppConstants;
import com.pc.framework.AbstractService;

@Service
public class LookupService  extends AbstractService {
	
	
	public ArrayList<LookupMenuBeans> getlookupsMenuBean() throws Exception
	{
		ArrayList<LookupMenuBeans>  list=new ArrayList<>();
		AppConstants appConstants=new AppConstants();
		//System.out.println("************************************ "+appConstants.lookupsPath);
		
		try (Stream<Path> filePathStream=Files.walk(Paths.get(appConstants.lookupsPath))) {
		    filePathStream.forEach(filePath -> {
		        if (Files.isRegularFile(filePath)) {
			        	if(!toString().valueOf(filePath).contains("empty.xhtml"))//Exclude empty.xhtml
			        	{
				            String strPath=String.valueOf(filePath);
				           
				            if(strPath.contains("\\")) //For Windows
				            {
				               String[] element=filePath.toString().split("lookups");
				               if(element.length>1)
				               {
				            	   LookupMenuBeans bean=new LookupMenuBeans(element[1].replace("\\", ""), getLookupHeading(strPath));
					               list.add(bean);
				               }
				               
				            }
							else
							{
								String[] element=filePath.toString().split("lookups");
					             if(element.length>1)
					             {
					            	 LookupMenuBeans bean=new LookupMenuBeans(element[1].replace("//", ""), getLookupHeading(strPath));
						             list.add(bean);
					             }
							}
						
			        }
		        }
		    });
		}
		catch(Exception e)
		{
			 System.out.println("****************: "+appConstants.lookupsPath+" :*********************");
			 System.out.println("ERROR : "+e.getMessage());
		}
		
				
		return list;
		
	}
	
	public String getLookupHeading(String path) 
	{
		String heading="No Title, Please add the following code on the xhtml file(<!--  <displayName>DisplayName</displayName> -->)";
		String content;
		try {
			content = new String(Files.readAllBytes(Paths.get(path)));
			/*Ensure that this code is provided on the lookup file*/
			/*<!--  <displayName>DisplayName</displayName> -->*/
			if (content.contains("<displayName>")) {
				String name = content.substring(content.indexOf("<displayName>")+13);
				heading = name.substring(0,name.indexOf("</displayName>"));
			}
			return heading;
		} catch (IOException e) {
			
			e.printStackTrace();
			return heading;
			
		}
		
		
		
	}
}
