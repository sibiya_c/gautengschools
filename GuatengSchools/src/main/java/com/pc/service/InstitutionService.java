package com.pc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;

import com.pc.entities.GautengSchoolImport;
import com.pc.entities.Institution;
import com.pc.entities.enums.InstStatusEnum;
import com.pc.repositories.GautengSchoolImportRepository;
import com.pc.repositories.InstitutionRepository;
import com.pc.framework.AbstractService;

@Service
public class InstitutionService extends AbstractService{
	@Autowired
	InstitutionRepository repository;
	@Autowired
	PrepareInstitutionDataService prepareInstitutionDataService;
	
	public void saveInstitution(Institution institution)  throws Exception
	{
	    if(institution.getId()==null)
		{
			institution.setCreateDate(new Date());
		}else{
			
			if(getCurrentUser() !=null){institution.setLastUpdateUser(getCurrentUser());}
			institution.setLastUpdateDate(new Date());
		}
		repository.save(institution);
	}
	
	public void deleteInstitution(Institution institution)  throws Exception
	{
		repository.delete(institution);
	}
	
	public void deleteInstitutionByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Institution> findAllInstitution()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Institution> findAllInstitution(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Institution> findAllInstitution(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Institution findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public void saveImpotedInstitution(List<GautengSchoolImport> schoolList)  throws Exception
	{
		
		List<Institution> institutionList= new ArrayList<Institution>();
		for(GautengSchoolImport shcool:schoolList) {
			Institution inst=new Institution();
			inst.setCreateDate(new Date());
			prepareInstitutionDataService.prepareInstitutionData(inst, shcool);
			institutionList.add(inst);
		}
		repository.saveAll(institutionList);
	}
	
	public List<Institution> findByOfficialInstitutionNameStartingWith(String searchValue) throws Exception{
		return repository.findByOfficialInstitutionNameStartingWith(searchValue);
	}
	
	public long count() throws Exception{
		return repository.count();
	}
	
	public long countByInstStatusEnum(InstStatusEnum instStatus) throws Exception{
		return repository.countByInstStatusEnum(instStatus);
	}
	
	

}
