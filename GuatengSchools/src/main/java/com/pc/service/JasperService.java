package com.pc.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.mail.Address;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;

import org.apache.commons.io.FileUtils;
import org.hibernate.jdbc.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import com.pc.beans.AttachmentBean;
import com.pc.beans.ItemBean;
import com.pc.beans.Mail;
import com.pc.mail.MailSender;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class JasperService{

	@Autowired
	MailSender mailSender;
	
	/** The Constant _pdfContent. */
	private static final String _pdfContent = "application/pdf";

	/** The Constant _excelContent. */
	private static final String _excelContent = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	
	private static JasperService jasperService;
	


	public static synchronized JasperService instance() {
		if (jasperService == null) {
			jasperService = new JasperService();
		}
		return jasperService;
	}

	public  void addLogo(Map<String, Object> params) {
		try {
			byte[] buff = FileUtils.readFileToByteArray(new File(geWebappPath() + "/resources/img/logo2.png"));
			params.put("logo", new ImageIcon(buff).getImage());
			byte[] buff2 = FileUtils.readFileToByteArray(new File(geWebappPath() + "/resources/img/mer.jpeg"));
			params.put("backround_image", new ImageIcon(buff2).getImage());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addImage(Map<String, Object> params, String image, String paramName) {
		try {
			byte[] buff = FileUtils.readFileToByteArray(new File(geWebappPath() + "/resources/img/" + image));
			params.put(paramName, new ImageIcon(buff).getImage());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	
	private static void convertByteArrayToServletOutputStream(byte[] bytes, String filename) throws Exception {
		FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
		Object resp = context.getExternalContext().getResponse();
		if (resp instanceof HttpServletResponse) {
			HttpServletResponse response = createRespObj((HttpServletResponse) resp, _pdfContent, filename);
			ServletOutputStream out = response.getOutputStream();
			out.write(bytes);
			out.flush();
			out.close();
			context.responseComplete();
		}
	}

	
	public static void convertByteArrayToServletOutputStreamExcel(byte[] bytes, String filename) throws Exception {
		FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
		Object resp = context.getExternalContext().getResponse();
		if (resp instanceof HttpServletResponse) {
			HttpServletResponse response = createRespObj((HttpServletResponse) resp, _excelContent, filename);
			ServletOutputStream out = response.getOutputStream();
			out.write(bytes);
			out.flush();
			out.close();
			context.responseComplete();
		}
	}

	public static void convertByteArrayToServletOutputStream(byte[] bytes, String filename, String contentType) throws Exception {
		FacesContext context = javax.faces.context.FacesContext.getCurrentInstance();
		Object resp = context.getExternalContext().getResponse();
		if (resp instanceof HttpServletResponse) {
			HttpServletResponse response = createRespObj((HttpServletResponse) resp, contentType, filename);
			ServletOutputStream out = response.getOutputStream();
			out.write(bytes);
			out.flush();
			out.close();
			context.responseComplete();
		}
	}

	

	/**
	 * Creates the resp obj.
	 *
	 * @param response
	 *            the response
	 * @param contentType
	 *            the content type
	 * @param fileName
	 *            the file name
	 * @return the http servlet response
	 */
	private static HttpServletResponse createRespObj(HttpServletResponse response, String contentType, String fileName) {
		response.setContentType(contentType);
		response.setHeader("Content-disposition", "attachment; filename=" + fileName);
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
		return response;
	}



	/**
	 * Print report using Java Bean
	 *
	 * @param report
	 *            the report
	 * @param fileName
	 *            the fileName
	 * @param collection
	 *            collection
	 */
	public void printReportUsingBean(String report, String fileName, Collection collection, Map<String, Object> params) throws Exception {
		if(params ==null)
		{
		 params = new HashMap<String, Object>();
		}
		addImage(params, "logo.PNG", "logo");
		params.put("itemsDataSource", new JRBeanCollectionDataSource(getItems()));
		JasperReport jasperReport = JasperCompileManager.compileReport(getReportPath()+"/"+ report);
		JRDataSource dataSource = new JRBeanCollectionDataSource(collection);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
		// PrintingThe Report
		byte[] output = JasperExportManager.exportReportToPdf(jasperPrint);
		convertByteArrayToServletOutputStream(output, fileName);

	}
	
	/*
	 * public void viewInvoiceQuotation(InvoiceQuotation invoiceQuotation,
	 * List<InvoiceQuotationItems> invoiceQuotationItemsList) throws Exception {
	 * Map<String, Object> params=new HashMap<String, Object>(); Collection
	 * collection=new ArrayList<>();
	 * 
	 * if(invoiceQuotation.getCompany().getLogo() !=null) { params.put("logo", new
	 * ImageIcon(invoiceQuotation.getCompany().getLogo().getPic()).getImage()); }
	 * else { addImage(params, "companyplaceholder.jpg", "logo"); } addImage(params,
	 * "invoice4_bg.jpg", "backround");
	 * params.put("invoiceQuotation",invoiceQuotation);
	 * params.put("itemsDataSource", new
	 * JRBeanCollectionDataSource(invoiceQuotationItemsList)); JasperReport
	 * jasperReport = JasperCompileManager.compileReport(getReportPath()+"/"+
	 * "billing_statement.jrxml"); JRDataSource dataSource = new
	 * JRBeanCollectionDataSource(collection); JasperPrint jasperPrint =
	 * JasperFillManager.fillReport(jasperReport, params, dataSource); //
	 * PrintingThe Report byte[] output =
	 * JasperExportManager.exportReportToPdf(jasperPrint);
	 * convertByteArrayToServletOutputStream(output, "InvoiceQuatation.pdf");
	 * 
	 * }
	 */
	
	
	/*
	 * public void sendInvoiceQuotation(InvoiceQuotation invoiceQuotation,
	 * List<InvoiceQuotationItems> invoiceQuotationItemsList) throws Exception {
	 * Map<String, Object> params=new HashMap<String, Object>(); Collection
	 * collection=new ArrayList<>();
	 * 
	 * 
	 * if(invoiceQuotation.getCompany().getLogo() !=null) { params.put("logo", new
	 * ImageIcon(invoiceQuotation.getCompany().getLogo().getPic()).getImage()); }
	 * else { addImage(params, "companyplaceholder.jpg", "logo"); }
	 * params.put("invoiceQuotation",invoiceQuotation);
	 * params.put("itemsDataSource", new
	 * JRBeanCollectionDataSource(invoiceQuotationItemsList)); JasperReport
	 * jasperReport = JasperCompileManager.compileReport(getReportPath()+"/"+
	 * "Invoice2.jrxml"); JRDataSource dataSource = new
	 * JRBeanCollectionDataSource(collection); JasperPrint jasperPrint =
	 * JasperFillManager.fillReport(jasperReport, params, dataSource);
	 * 
	 * // PrintingThe Report byte[] output =
	 * JasperExportManager.exportReportToPdf(jasperPrint); //Sending Email
	 * AttachmentBean attachmentBean=new com.pc.beans.AttachmentBean("Invoice.pdf",
	 * "application/pdf", output); List<AttachmentBean> attachmentBeanList=new
	 * ArrayList<>(); attachmentBeanList.add(attachmentBean); String []
	 * to={invoiceQuotation.getCustomerContactPerson().getEmail()}; String []
	 * cc={invoiceQuotation.getCreatorUser().getEmail()}; Mail mail=new Mail();
	 * mail.setFrom("cc@info.ac.za"); mail.setTo(to); mail.setCc(cc);
	 * mail.setSubject("Invoice");
	 * mail.setContent("Attached please find your invoice");
	 * mail.setAttachmentBeanList(attachmentBeanList);
	 * mailSender.sendWithBeanAttachement(mail);
	 * 
	 * }
	 */
	
	
	public ArrayList<ItemBean> getItems()
	{
		 ArrayList<ItemBean> list=new ArrayList<>();
		 
		 ItemBean itemBean=new ItemBean("Item 1", 5, 2, 10);
		 list.add(itemBean);
		 
		 itemBean=new ItemBean("Item 2", 50, 3, 150);
		 list.add(itemBean);
		 
		 itemBean=new ItemBean("Item 3", 60, 1, 60);
		 list.add(itemBean);
		 
		 itemBean=new ItemBean("Item 4", 70, 3, 210);
		 list.add(itemBean);
		 
		 itemBean=new ItemBean("Item 5", 100, 3, 300);
		 list.add(itemBean);
		 
		 return list;
	}

	/**
	 * Format array.
	 *
	 * @param arr
	 *            the arr
	 * @return the string
	 */
	private String formatArray(Address[] arr) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < arr.length; i++) {
			sb.append(arr[i].toString());
			if (i < arr.length - 1) sb.append(", ");
		}
		return sb.toString();
	}

	/**
	 * The Class RunReportToPdfStreamHelper.
	 */
	class RunReportToPdfStreamHelper implements Work {

		/** The input stream. */
		private InputStream inputStream;

		/** The output stream. */
		private OutputStream outputStream;

		/** The parameters. */
		private Map<?, ?> parameters;

		/**
		 * Instantiates a new run report to pdf stream helper.
		 *
		 * @param inputStream
		 *            the input stream
		 * @param outputStream
		 *            the output stream
		 * @param parameters
		 *            the parameters
		 */
		public RunReportToPdfStreamHelper(InputStream inputStream, OutputStream outputStream, Map<?, ?> parameters) {
			this.inputStream = inputStream;
			this.outputStream = outputStream;
			this.parameters = parameters;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.hibernate.jdbc.Work#execute(java.sql.Connection)
		 */
		@Override
		public void execute(Connection conn) throws SQLException {
			try {
				JasperRunManager.runReportToPdfStream(inputStream, outputStream, (Map<String, Object>) parameters, conn);
			} catch (JRException e) {
				throw new SQLException(e);
			}
		}

	}

	/**
	 * Convert a file to bite array
	 *
	 * @param file
	 * @return byte[]
	 * @throws Exception
	 *             the exception
	 */
	public static byte[] readBytesFromFile(File file) throws Exception {
		FileInputStream fileInputStream = null;
		byte[] bytesArray = null;
		bytesArray = new byte[(int) file.length()];

		// read file into bytes[]
		fileInputStream = new FileInputStream(file);
		fileInputStream.read(bytesArray);
		if (fileInputStream != null) {
			fileInputStream.close();
		}
		return bytesArray;
	}
	
	public String getReportPath()
	{
		/* /C:/Users/Christoph%20Sibiya/Documents/mopanefsProject/MopaneFS/src/main/webapp */
		String path =getPath().replace("target/classes/", "src/main/webapp");
		if(path.contains("%20")){
			path=path.replaceAll("%20", " ");
		}
		if(String.valueOf(path.charAt(0)).equalsIgnoreCase("/") || String.valueOf(path.charAt(0)).equalsIgnoreCase("\\"))
		{
			path=path.replaceFirst(String.valueOf(path.charAt(0)), "");
		}
		
		path=path+"/reports";
		
		return path;
		
	}
	
	public String geWebappPath()
	{
		/* /C:/Users/Christoph%20Sibiya/Documents/mopanefsProject/MopaneFS/src/main/webapp */
		String path =getPath().replace("target/classes/", "src/main/webapp");
		if(path.contains("%20")){
			path=path.replaceAll("%20", " ");
		}
		if(String.valueOf(path.charAt(0)).equalsIgnoreCase("/") || String.valueOf(path.charAt(0)).equalsIgnoreCase("\\"))
		{
			path=path.replaceFirst(String.valueOf(path.charAt(0)), "");
		}
		
		
		return path;
		
	}
	
	public String getPath()
	{
		URL resource = getClass().getResource("/");
		String path = resource.getPath();
		
		//path=path.replace("target/classes/", "src/main/webapp");
		return path;
	}

	

}
