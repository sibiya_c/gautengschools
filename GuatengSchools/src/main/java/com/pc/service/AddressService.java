package com.pc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.Address;
import com.pc.repositories.AddressRepository;
import com.pc.framework.AbstractService;

@Service
public class AddressService extends AbstractService{
	@Autowired
	AddressRepository repository;
	
	public void saveAddress(Address address)  throws Exception
	{
	    if(address.getId()==null)
		{
			address.setCreateDate(new Date());
		}else{
			
			if(getCurrentUser() !=null){address.setLastUpdateUser(getCurrentUser());}
			address.setLastUpdateDate(new Date());
		}
		repository.save(address);
	}
	
	public void deleteAddress(Address address)  throws Exception
	{
		repository.delete(address);
	}
	
	public void deleteAddressByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<Address> findAllAddress()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<Address> findAllAddress(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<Address> findAllAddress(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public Address findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	/*public List<Address> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}*/
	
	
	

}
