package com.pc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.GautengSchoolImport;
import com.pc.repositories.GautengSchoolImportRepository;
import com.pc.framework.AbstractService;

@Service
public class GautengSchoolImportService extends AbstractService{
	@Autowired
	GautengSchoolImportRepository repository;
	
	public void saveGautengSchoolImport(GautengSchoolImport gautengSchoolImport)  throws Exception
	{
	    if(gautengSchoolImport.getId()==null)
		{
			gautengSchoolImport.setCreateDate(new Date());
		}else{
			
			if(getCurrentUser() !=null){gautengSchoolImport.setLastUpdateUser(getCurrentUser());}
			gautengSchoolImport.setLastUpdateDate(new Date());
		}
		repository.save(gautengSchoolImport);
	}
	
	public void deleteGautengSchoolImport(GautengSchoolImport gautengSchoolImport)  throws Exception
	{
		repository.delete(gautengSchoolImport);
	}
	
	public void deleteGautengSchoolImportByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<GautengSchoolImport> findAllGautengSchoolImport()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<GautengSchoolImport> findAllGautengSchoolImport(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<GautengSchoolImport> findAllGautengSchoolImport(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public GautengSchoolImport findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	

	public void saveAllGautengSchoolImport(List<GautengSchoolImport> gautengSchoolImportList)  throws Exception
	{
		if(gautengSchoolImportList !=null && gautengSchoolImportList.size()>0) {
			for(GautengSchoolImport school :gautengSchoolImportList) {
				school.setCreateDate(new Date());
			}
			repository.saveAll(gautengSchoolImportList);
		}
		
	}
	
	

}
