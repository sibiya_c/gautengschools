package com.pc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.Date;

import com.pc.entities.LearnersAndEducators;
import com.pc.repositories.LearnersAndEducatorsRepository;
import com.pc.framework.AbstractService;

@Service
public class LearnersAndEducatorsService extends AbstractService{
	@Autowired
	LearnersAndEducatorsRepository repository;
	
	public void saveLearnersAndEducators(LearnersAndEducators learnersAndEducators)  throws Exception
	{
	    if(learnersAndEducators.getId()==null)
		{
			learnersAndEducators.setCreateDate(new Date());
		}else{
			
			if(getCurrentUser() !=null){learnersAndEducators.setLastUpdateUser(getCurrentUser());}
			learnersAndEducators.setLastUpdateDate(new Date());
		}
		repository.save(learnersAndEducators);
	}
	
	public void deleteLearnersAndEducators(LearnersAndEducators learnersAndEducators)  throws Exception
	{
		repository.delete(learnersAndEducators);
	}
	
	public void deleteLearnersAndEducatorsByID(Integer  arg0)  throws Exception
	{
		 repository.deleteById(arg0);
	}
	
	public List<LearnersAndEducators> findAllLearnersAndEducators()  throws Exception
	{
		return repository.findAll();
	}
	
	public Page<LearnersAndEducators> findAllLearnersAndEducators(Pageable p)  throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<LearnersAndEducators> findAllLearnersAndEducators(Sort s)  throws Exception
	{
		return repository.findAll(s);
	}
	
	public LearnersAndEducators findById(Long parseLong) throws Exception
	{
		return repository.findById(parseLong);
	}
	
	public Long totalLearners(Integer year) throws Exception {
		Long total=repository.totalLearners(year);
		if(total==null) {
			total=0L;
		}
		return total;
	}
	
	public Long totalEducators(Integer year) throws Exception {
		Long total=repository.totalEducators(year);
		if(total==null) {
			total=0L;
		}
		return total;
	}
	
	/*public List<LearnersAndEducators> findByDescriptionStartingWith(String description)  throws Exception
	{
		return repository.findByDescriptionStartingWith(description);
	}*/
	
	
	

}
