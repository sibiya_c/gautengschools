package com.pc.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pc.entities.Address;
import com.pc.entities.GautengSchoolImport;
import com.pc.entities.Institution;
import com.pc.entities.LearnersAndEducators;
import com.pc.entities.enums.InstStatusEnum;
import com.pc.entities.enums.NoFeeSchoolEnum;
import com.pc.entities.enums.SectionEnum;
import com.pc.entities.enums.UrbanRuralEnum;
import com.pc.entities.lookup.Province;
import com.pc.framework.AbstractService;
import com.pc.service.lookup.AddrInitService;
import com.pc.service.lookup.AddreesseeService;
import com.pc.service.lookup.CircuitService;
import com.pc.service.lookup.DeptMunNameService;
import com.pc.service.lookup.DistrictService;
import com.pc.service.lookup.ExDeptService;
import com.pc.service.lookup.ExamCentreService;
import com.pc.service.lookup.LocalMunNameService;
import com.pc.service.lookup.OwnerBuildingService;
import com.pc.service.lookup.OwnerLandService;
import com.pc.service.lookup.PhasePEDService;
import com.pc.service.lookup.ProvinceService;
import com.pc.service.lookup.RegionService;
import com.pc.service.lookup.SubPlaceService;
import com.pc.service.lookup.SectorService;
import com.pc.service.lookup.SpecialisationService;
import com.pc.service.lookup.TownCityService;
import com.pc.service.lookup.TownshipVillageService;
import com.pc.service.lookup.TypeDoEService;
import com.pc.service.lookup.WardService;

@Service
public class PrepareInstitutionDataService extends AbstractService {
	
	@Autowired
	private AddreesseeService addreesseeService;
	@Autowired
	private AddrInitService addrInitService;
	@Autowired
	private CircuitService circuitService;
	@Autowired
	private DistrictService districtService;
	@Autowired
	private DeptMunNameService deptMunNameService;
	@Autowired
	private LocalMunNameService localMunNameService;
	@Autowired
	private ExamCentreService examCentreService;
	@Autowired
	private ExDeptService exDeptService;
	@Autowired
	private ProvinceService provinceService;
	@Autowired
	private OwnerBuildingService ownerBuildingService;
	@Autowired
	private OwnerLandService ownerLandService;
	@Autowired
	private PhasePEDService phasePEDService;
	@Autowired
	private RegionService regionService;
	@Autowired
	private SectorService sectorService;
	@Autowired
	private SpecialisationService specialisationService;
	@Autowired
	private SubPlaceService subPlaceService;
	@Autowired
	private TownCityService townCityService;
	@Autowired
	private TownshipVillageService townshipVillageService;
	@Autowired
	private TypeDoEService typeDoEService;
	@Autowired
	private WardService wardService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private LearnersAndEducatorsService learnersAndEducatorsService;
	
	
	public void prepareInstitutionData(Institution institution,GautengSchoolImport gautengSchoolImport) {
		try {
			institution.setGautengSchoolImport(gautengSchoolImport);
			institution.setAllocation(gautengSchoolImport.getAllocation());
			institution.setComponetNo(gautengSchoolImport.getComponentNo());
			institution.setDataYear(gautengSchoolImport.getDataYear());
			institution.setExamNo(gautengSchoolImport.getExamNo());
			institution.setInstStatusEnum(InstStatusEnum.fromString(gautengSchoolImport.getStatus()));
			/**Learners And Educators*/
			LearnersAndEducators learnersAndEducators=new LearnersAndEducators();
			learnersAndEducators.setYear(2018);
			if(gautengSchoolImport.getEducator2018() !=null && !gautengSchoolImport.getEducator2018().isEmpty()) {
				learnersAndEducators.setEducators(Integer.parseInt(gautengSchoolImport.getEducator2018()));
			}
			if(gautengSchoolImport.getLearners2018() !=null && !gautengSchoolImport.getLearners2018().isEmpty()) {
				learnersAndEducators.setLearners(Integer.valueOf(gautengSchoolImport.getLearners2018()));
			}
			learnersAndEducatorsService.saveLearnersAndEducators(learnersAndEducators);
			institution.setLearnersAndEducators(learnersAndEducators);
			
			institution.setNatEmis(gautengSchoolImport.getNatEmis());
			institution.setNewNATEMIS(gautengSchoolImport.getNewNATEMIS());
			institution.setNodalArea(gautengSchoolImport.getNodalArea());
			institution.setNoFeeSchoolEnum(NoFeeSchoolEnum.fromString(gautengSchoolImport.getNoFeeSchool()));
			institution.setOfficialInstitutionName(gautengSchoolImport.getOfficialInstitutionName());
			institution.setOldNATEMIS(gautengSchoolImport.getOldNATEMIS());
			institution.setPaypointNo(gautengSchoolImport.getPayPointNo());
			institution.setQuantile(gautengSchoolImport.getQuintile());
			institution.setSection(SectionEnum.fromString(gautengSchoolImport.getSection21()));
			institution.setSection21Funct(gautengSchoolImport.getSection21Funct());
			institution.setTelNum(gautengSchoolImport.getTelephone());
			institution.setUrbanRuralEnum(UrbanRuralEnum.fromString(gautengSchoolImport.getUrbanRural()));
			
			/**Lookup Data*/
			institution.setAddreesseeLookup(addreesseeService.findByDescription(gautengSchoolImport.getAddressee()));
			institution.setAddrInitLookup(addrInitService.findByDescription(gautengSchoolImport.getAddrInit()));
			institution.setCircuitLookup(circuitService.findByDescription(gautengSchoolImport.getEiCircuit()));
			institution.setDistrictLookup(districtService.findByDescription(gautengSchoolImport.getEiDistrict()));
			institution.setDeptMunNameLookup(deptMunNameService.findByDescription(gautengSchoolImport.getdMunName()));
			institution.setExamCentreLookup(examCentreService.findByDescription(gautengSchoolImport.getExamCentre()));
			institution.setExDeptLookup(exDeptService.findByDescription(gautengSchoolImport.getExDept()));
			institution.setFemorcationFrom(provinceService.findByCode(gautengSchoolImport.getDemarcationFrom()));
			institution.setFemorcationTo(provinceService.findByCode(gautengSchoolImport.getDemarcationTo()));
			institution.setLocalMunNameLookup(localMunNameService.findByDescription(gautengSchoolImport.getlMunName()));
			institution.setOwnerBuildingLookup(ownerBuildingService.findByDescription(gautengSchoolImport.getOwnerBuildings()));
			institution.setOwnerLandLookup(ownerLandService.findByDescription(gautengSchoolImport.getOwnerLand()));
			institution.setPhasePEDLookup(phasePEDService.findByDescription(gautengSchoolImport.getPhasePED()));
			institution.setProvinceLookup(provinceService.findByCode(gautengSchoolImport.getProvince()));
			institution.setRegionLookup(regionService.findByDescription(gautengSchoolImport.getEiRegion()));
			institution.setSectorLookup(sectorService.findByDescription(gautengSchoolImport.getSector()));
			institution.setSpecialisationLookup(specialisationService.findByDescription(gautengSchoolImport.getSpecialization()));
			institution.setSubPlaceLookup(subPlaceService.findByDescription(gautengSchoolImport.getSpName()));
			institution.setTownCityLookup(townCityService.findByDescription(gautengSchoolImport.getTownCity()));
			institution.setTownshipVillageLookup(townshipVillageService.findByDescription(gautengSchoolImport.getTownshipVillage()));
			institution.setTypeDoELookup(typeDoEService.findByDescription(gautengSchoolImport.getTypeDoE()));
			institution.setWardLookup(wardService.findByDescription(gautengSchoolImport.getWardId()));
			if(gautengSchoolImport.getRegistrationDate() !=null && !gautengSchoolImport.getRegistrationDate().isEmpty()){
				try {
					SimpleDateFormat originalFormat = new SimpleDateFormat("yyyyMMdd");
					Date date = originalFormat.parse(gautengSchoolImport.getRegistrationDate());
					institution.setRegistrationDate(date);
				}catch(Exception e) {
					//e.printStackTrace();
				}
			}
			
			
			/**Address Data*/
			Address address= new Address();
			try {
				if(gautengSchoolImport.getStreetAddress() !=null) {
					String[] elements=gautengSchoolImport.getStreetAddress().split(",");
					if(elements !=null && elements.length>0) {
						address.setAddressLine1(elements[0]);
						if(elements.length >=1) {
							address.setAddressLine2(elements[1]);
						}
						if(elements.length >=2) {
							address.setAddressLine3(elements[2]);
						}
						if(elements.length >=3) {
							address.setAddressLine3(elements[3]);
						}
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
			address.setGisLatitude(gautengSchoolImport.getGisLatitude());
			address.setGisLongitude(gautengSchoolImport.getGisLongitude());
			address.setGisSource(gautengSchoolImport.getGisSource());
			address.setPostAddress(gautengSchoolImport.getPostalAddress());
			addressService.saveAddress(address);
			institution.setAddress(address);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
