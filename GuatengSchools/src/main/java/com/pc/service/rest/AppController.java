package com.pc.service.rest;

import com.ibm.icu.text.SimpleDateFormat;
import com.pc.entities.ContactUs;
import com.pc.entities.Institution;
import com.pc.entities.ReportedInstitutions;
import com.pc.entities.enums.ReportedInstitutionsStatus;
import com.pc.model.AppReportedInstitution;
import com.pc.model.InstitutionDTO;
import com.pc.model.ReportedInstitutionDTO;
import com.pc.model.Response;
import com.pc.service.ContactUsService;
import com.pc.service.InstitutionService;
import com.pc.service.ReportedInstitutionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@CrossOrigin(origins = "http://localhost", maxAge = 3600)
@RestController
@RequestMapping("rest")
public class AppController {

    @Autowired
    private InstitutionService institutionService;

    @Autowired
    private ReportedInstitutionsService reportedInstitutionsService;

    @Autowired
    private ContactUsService contactUsService;

    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    @GetMapping("/find-institution-by-name")
    public List<InstitutionDTO> findInstitutionByName(@RequestParam("institutionName") String institutionName) {
        try {
            return mapSchools(institutionService.findByOfficialInstitutionNameStartingWith(institutionName));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/find-institution-by-id")
    public InstitutionDTO findInstitutionById(@RequestParam("id") Long id) {
        try {
            return mapSingleSchool(institutionService.findById(id));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private List<InstitutionDTO> mapSchools(List<Institution> byOfficialInstitutionNameStartingWith) {
        List<InstitutionDTO> list = new ArrayList<>();
        byOfficialInstitutionNameStartingWith.forEach(institution -> {
            InstitutionDTO institutionDTO = new InstitutionDTO();
            institutionDTO.setId(institution.getId());
            institutionDTO.setSchoolName(institution.getOfficialInstitutionName());
            institutionDTO.setSectorCode(institution.getSectorLookup().getCode());
            institutionDTO.setSectorDescription(institution.getSectorLookup().getDescription());
            institutionDTO.setDistrictCode(institution.getDistrictLookup().getCode());
            institutionDTO.setAddressLine1(institution.getAddress().getAddressLine1());
            institutionDTO.setAddressLine2(institution.getAddress().getAddressLine2());
            institutionDTO.setAddressLine3(institution.getAddress().getAddressLine3());
            institutionDTO.setAddressLine4(institution.getAddress().getAddressLine4());

            list.add(institutionDTO);
        });

        return list;
    }

    private InstitutionDTO mapSingleSchool(Institution institution) {
        InstitutionDTO institutionDTO = new InstitutionDTO();
        institutionDTO.setId(institution.getId());
        institutionDTO.setSchoolName(institution.getOfficialInstitutionName());
        institutionDTO.setSectorCode(institution.getSectorLookup().getCode());
        institutionDTO.setSectorDescription(institution.getSectorLookup().getDescription());
        institutionDTO.setDistrictCode(institution.getDistrictLookup().getCode());
        institutionDTO.setAddressLine1(institution.getAddress().getAddressLine1());
        institutionDTO.setAddressLine2(institution.getAddress().getAddressLine2());
        institutionDTO.setAddressLine3(institution.getAddress().getAddressLine3());
        institutionDTO.setAddressLine4(institution.getAddress().getAddressLine4());
        return institutionDTO;
    }

    @GetMapping("/check-status")
    public ReportedInstitutionDTO chechStatus(@RequestParam("reference-number") String referenceNumber) {
        ReportedInstitutionDTO reportedInstitutionDTO = new ReportedInstitutionDTO();
        try {
            if (referenceNumber != null && !referenceNumber.isEmpty()) {
                referenceNumber = referenceNumber.trim();
                ReportedInstitutions reportedInstitutions = reportedInstitutionsService.findByRefNumber(referenceNumber);
                if (reportedInstitutions != null) {
                    String institutionName = reportedInstitutions.getInstitutionName();
                    reportedInstitutionDTO.setInstitutionName(institutionName);
                    reportedInstitutionDTO.setInvestigatorFeedback(reportedInstitutions.getInvestigatorFeedback());
                    reportedInstitutionDTO.setRefNumber(referenceNumber);
                    reportedInstitutionDTO.setRepordedDate(sdf.format(reportedInstitutions.getCreateDate()));
                    reportedInstitutionDTO.setReporterFullName(reportedInstitutions.getReporterName() + " " + reportedInstitutions.getReporterSurname());
                    reportedInstitutionDTO.setStatus(reportedInstitutions.getStatus().getFriendlyName());
                    if (reportedInstitutions.getLastUpdateDate() != null) {
                        reportedInstitutionDTO.setLastUpdatedDate(sdf.format(reportedInstitutions.getLastUpdateDate()));
                    } else {
                        reportedInstitutionDTO.setLastUpdatedDate("N/A");
                    }
                    reportedInstitutionDTO.setErrorMessage("");
                } else {
                    reportedInstitutionDTO.setErrorMessage("Invalid reference number");
                }
            } else {
                reportedInstitutionDTO.setErrorMessage("Please enter reference number");
            }
        } catch (Exception err) {
            err.printStackTrace();
            reportedInstitutionDTO.setErrorMessage("Error: " + err.getMessage());
        }
        return reportedInstitutionDTO;
    }

    @PostMapping("/report-institution")
    @ResponseBody
    public Response reportInstitution(@RequestBody AppReportedInstitution appReportedInstitution) {
        Response response = new Response();
        try {
            reportedInstitutionsService.submitReportedInstitutions(mapInstitution(appReportedInstitution));
            response.setMessage(appReportedInstitution.getSchoolName() + " successfully reported");
            return response;
        } catch (Exception err) {
            err.printStackTrace();
            response.setMessage("Error couldn't save report " + err.getMessage());
            return response;
        }
    }

    @PostMapping("/send-message")
    @ResponseBody
    public Response sendContactUsMessage(@RequestBody ContactUs contactUs) {
        Response response = new Response();
        try {
            ContactUs cu = contactUsService.sendContactUsMessage(contactUs);
            response.setMessage("Your message has been sent, thank you.");
            response.setId(cu.getId());
        } catch (Exception err) {
            err.printStackTrace();
            response.setMessage("Error couldn't send message " + err.getMessage());
        }
        return response;
    }

    private ReportedInstitutions mapInstitution(AppReportedInstitution appReportedInstitution) {
        ReportedInstitutions reportedInstitutions = new ReportedInstitutions();
        reportedInstitutions.setCreateDate(new Date());
        reportedInstitutions.setRefNumber(getRefNumber());
        reportedInstitutions.setStatus(ReportedInstitutionsStatus.Submitted);
        reportedInstitutions.setReporterCell(appReportedInstitution.getReporterPhone());
        reportedInstitutions.setReporterEmail(appReportedInstitution.getReporterEmail());
        reportedInstitutions.setReporterName(appReportedInstitution.getReporterName());
        reportedInstitutions.setReporterSurname(appReportedInstitution.getReporterSurname());
        reportedInstitutions.setAddressLine1(appReportedInstitution.getAddressLine1());
        reportedInstitutions.setAddressLine2(appReportedInstitution.getAddressLine2());
        reportedInstitutions.setAddressLine3(appReportedInstitution.getAddressLine3());
        reportedInstitutions.setAddressLine4(appReportedInstitution.getSuburbArea());
        reportedInstitutions.setPostCode(appReportedInstitution.getPostalCode());
        return reportedInstitutions;
    }

    private String getRefNumber() {
        int leftLimit = 97; // letter 'a'd
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 7;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        long millis = System.currentTimeMillis();
        String generatedString = "REF" + buffer.toString().toUpperCase() + "" + millis;
        return generatedString;
    }
}
