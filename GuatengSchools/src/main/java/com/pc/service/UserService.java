package com.pc.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.primefaces.event.FileUploadEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.pc.beans.Mail;
import com.pc.entities.ImageModel;
import com.pc.entities.User;
import com.pc.entities.UserRole;
import com.pc.framework.AbstractService;
import com.pc.mail.MailSender;
import com.pc.repositories.UserRepository;
import com.pc.service.lookup.GenderService;
import com.pc.service.lookup.RoleService;


@Service
public class UserService  extends AbstractService{
	@Autowired
	UserRepository repository;
	
	@Autowired
	MailSender mailSender;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	ImagesService imagesService;
	
	@Autowired
	GenderService genderService;
	
	@Autowired
	UserRoleService userRoleService;
	
	@Autowired
	RoleService roleService;
	
	private static final SimpleDateFormat sdfIdDate = new SimpleDateFormat("yyMMdd");
	
	public void saveUser(User user) throws Exception
	{
		boolean isNew=false;
		String plainPass=user.getPassword();
		if(user.getId()==null)
		{
			isNew=true;
			user.setPassword(generatePassword());
			user.setChangePassword(true);
			plainPass=user.getPassword();
			//Setting gender and date of bith
			user.setDob(sdfIdDate.parse(user.getRsaId().substring(0, 6)));
			if (Integer.parseInt(user.getRsaId().substring(6, 7)) > 4) {
				user.setGender(genderService.findByGenderName("Male"));
			} else {
				user.setGender(genderService.findByGenderName("Female"));
			}
			/*
			 * Setting Default
			 * The image with an ID of 0 must be added 
			 * on the ImageModel table
			 */
			ImageModel defaultImg=imagesService.getById(0L);
			ImageModel img=new ImageModel();
			img.setName(defaultImg.getName());
			img.setPic(defaultImg.getPic());
			img.setType(defaultImg.getType());
			
			imagesService.save(img);
			user.setUsername(user.getEmail());
			user.setImage(img);
			if(user.getPassword() !=null)
			{
				String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
				user.setPassword(encodedPassword);
			}
			else
			{
				String encodedPassword = bCryptPasswordEncoder.encode(user.getRsaId());
				user.setPassword(encodedPassword);
			}
			
			user.setCreateDate(new Date());
			user.setDeleted(false);
		}else{
			
			if(getCurrentUser() !=null){user.setLastUpdateUser(getCurrentUser());}
			user.setLastUpdateDate(new Date());
		}
		
		user.setUsername(user.getEmail());
		repository.save(user);
		if(isNew){
			addGeneralUserRole(user);
			//Send login email notification
			try{
				registrationNotification(user,plainPass);
			}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				System.err.println("Sending Email Error: "+e.getMessage());
			}
		}
		
		
		
	}
	

	
	public void addGeneralUserRole(User user)throws Exception
	{
		UserRole userRole=new UserRole();
		userRole.setRole(roleService.findByCode("USER"));
		userRole.setUser(user);
		userRoleService.saveUserRole(userRole);
	}
	
	public void deleteUser(User User) throws Exception
	{
		repository.delete(User);
	}
	
	public User changePassword(String email,  String password, String newPassword)throws Exception 
	{
		User user =  repository.getUserByEmail(email);

		if (user == null)
			throw new Exception("User with email address: " + email + " is not registered on the system! If you typed in the correct email please contact support.");
		else {
			if (!bCryptPasswordEncoder.matches(password.trim(), user.getPassword().trim())) {
				throw new Exception("Invalid password for user id: " + email);
			}
			
			user.setPassword(bCryptPasswordEncoder.encode(newPassword));
			repository.save(user);
		}
		return user;
		
	}
	
	/*public void deleteUserByID(Integer  arg0) throws Exception
	{
		 repository.delete(arg0);
	}*/
	
	public List<User> findAllUser() throws Exception
	{
		return repository.findAll();
	}
	
	public Page<User> findAllUser(Pageable p) throws Exception
	{
		return repository.findAll(p);
	}
	
	public List<User> findAllUser(Sort s) throws Exception
	{
		return repository.findAll(s);
	}
	
	
	
	/*public User findOnelUser(Integer  arg0) throws Exception
	{
		return repository.findOne(arg0);
	}*/
	
	public User findByEmail(String  email) throws Exception
	{
		return repository.findByEmail(email);
	}
	
	public User findByEmailAndRsaIdNot(String  email,String rsaId) throws Exception
	{
		return repository.findByEmailAndRsaIdNot(email, rsaId);
	}
	
	public User getUserByRsaId(String  idNumber) throws Exception
	{
		return repository.getUserByRsaId(idNumber);
	}
	
	public User getUserByEmail(String  email) throws Exception
	{
		return repository.getUserByEmail(email);
	}
	
	

	public void notifyUserNewPasswordEmail(String email) throws Exception {
		User ul = getUserByEmail(email);
		if (ul == null)
			throw new Exception("User not registered");
		notifyUserNewPassword(ul);
	}
	
	

	/**
	 * Notify a user of new password.
	 *
	 * @param u
	 *            the u
	 * @throws Exception
	 *             the exception
	 */
	public void notifyUserNewPassword(User u) throws Exception {
		//Generate password
		String pwd = generatePassword();
		u.setPassword(bCryptPasswordEncoder.encode(pwd));	
		repository.save(u);
		
		String welcome = "<p>Dear #NAME#,</p>" + "<br/>" + "<p>This is your new password: <b>" + pwd
				+ "</b> for email: <b>" + u.getEmail() + "</b></p>"
				+ "<p>You have to change it when you login.</p>" 
				+ "<p>Regards</p>" 
				+ "<p>Schools Verify Team</p>"
				+ "<br/>";
		welcome = welcome.replaceAll("#NAME#", u.getName()+" "+u.getSurname());
		
		Mail mail=new Mail();
		
		mail.setContent(welcome);
		mail.setFrom("info@cc.co.za");
		String[] to={u.getEmail()};;
		mail.setTo(to);
		mail.setSubject("Forgot Password");
		mail.setCc(to);
		mailSender.sendHtmlEmil(mail);
	}
	
	
	/**
	 * Notify a user.
	 *
	 * 
	 */
	public void registrationNotification(User u,String plainPass) throws Exception {
		
		String welcome = "<p>Dear #NAME#,</p>" 
				+ "<p>Your Schools Verify account has been created"
	            + "<br/>" 
				+"<P>Find your login details below: </p>"
				+ "<p><b>Username/Email:</b> " + u.getEmail() + "</p>"
				+ "<p><b>Password:</b>" + plainPass+"</p>"
				+ "<p>Regards</p>" 
				+ "<p>Schools Verify Team</p>"
				+ "<br/>";
		welcome = welcome.replaceAll("#NAME#", u.getName()+" "+u.getSurname());
		
		Mail mail=new Mail();
		
		mail.setContent(welcome);
		mail.setFrom("info@cc.co.za");
		String[] to={u.getEmail()};;
		mail.setTo(to);
		mail.setSubject("Schools Verify Registration");
		mail.setCc(to);
		mailSender.sendHtmlEmil(mail);
	}
	

	public String generatePassword() {
		  
	    int leftLimit = 97; // letter 'a'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 10;
	    Random random = new Random();
	    StringBuilder buffer = new StringBuilder(targetStringLength);
	    for (int i = 0; i < targetStringLength; i++) {
	        int randomLimitedInt = leftLimit + (int) 
	          (random.nextFloat() * (rightLimit - leftLimit + 1));
	        Random rand = new Random();
	        int n = rand.nextInt(15) + 1;
	        if(n>10)
	        {
	        	 buffer.append(Character.toUpperCase((char) randomLimitedInt));
	        }
	        else
	        {
	        	 buffer.append((char) randomLimitedInt);
	        }
	    }
	    String generatedString = buffer.toString();
	    generatedString=generatedString.trim();
	    System.out.println(generatedString);
		return generatedString;
	}

	public void saveProfileImage(User currentUser, FileUploadEvent event) throws Exception {
		
		if(currentUser.getImage() ==null)
		{
			ImageModel imageModel =  new ImageModel();
			imageModel.setName(event.getFile().getFileName().trim());
			imageModel.setType(event.getFile().getContentType());
			imageModel.setPic(event.getFile().getContents());
			imageModel  = imagesService.save(imageModel);
			currentUser.setImage(imageModel);
			saveUser(currentUser);
			
		}
		else
		{
			ImageModel newImg=currentUser.getImage();
			newImg.setName(event.getFile().getFileName().trim());
			newImg.setType(event.getFile().getContentType());
			newImg.setPic(event.getFile().getContents());
			//currentUser.setImage(newImg);
			imagesService.save(newImg);
		}
		
	}
	
	public void resetPassword(User user,String newPassword)throws Exception 
	{
			user.setPassword(bCryptPasswordEncoder.encode(newPassword));
			user.setChangePassword(false);
			repository.save(user);
	
	}
}
