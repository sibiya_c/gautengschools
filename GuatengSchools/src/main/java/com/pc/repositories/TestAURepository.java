package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.TestAU;

@Repository
public interface TestAURepository extends JpaRepository<TestAU, Integer> 
{
	
	public TestAU findById(Long parseLong);
	//public List<TestAU> findByDescriptionStartingWith(String description);
}
