package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pc.entities.LearnersAndEducators;

@Repository
public interface LearnersAndEducatorsRepository extends JpaRepository<LearnersAndEducators, Integer> 
{
	
	public LearnersAndEducators findById(Long parseLong);
	@Query("SELECT SUM(learners) FROM LearnersAndEducators o WHERE o.year =:year")
	public Long totalLearners(@Param("year") Integer year);
	@Query("SELECT SUM(educators) FROM LearnersAndEducators o WHERE o.year =:year")
	public Long totalEducators(@Param("year") Integer year);
	//public List<LearnersAndEducators> findByDescriptionStartingWith(String description);
}
