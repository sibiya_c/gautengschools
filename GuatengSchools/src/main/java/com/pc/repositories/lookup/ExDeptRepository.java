package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.ExDept;

@Repository
public interface ExDeptRepository extends JpaRepository<ExDept, Integer> 
{
	
	public ExDept findById(Long parseLong);
	public List<ExDept> findByDescriptionStartingWith(String description);
	public List<ExDept> findByDescription(String description);
}
