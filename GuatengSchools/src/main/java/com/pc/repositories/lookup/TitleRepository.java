package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Title;

@Repository
public interface TitleRepository extends JpaRepository<Title, Integer> 
{
	
	public Title findById(Long parseLong);
	public List<Title> findByDescriptionStartingWith(String description);
}
