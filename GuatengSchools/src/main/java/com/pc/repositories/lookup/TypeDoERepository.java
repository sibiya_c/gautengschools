package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.TypeDoE;

@Repository
public interface TypeDoERepository extends JpaRepository<TypeDoE, Integer> 
{
	
	public TypeDoE findById(Long parseLong);
	public List<TypeDoE> findByDescriptionStartingWith(String description);
	public List<TypeDoE> findByDescription(String description);
}
