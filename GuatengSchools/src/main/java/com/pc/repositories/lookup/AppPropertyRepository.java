package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.AppProperty;

@Repository
public interface AppPropertyRepository extends JpaRepository<AppProperty, Integer> 
{
	
	public AppProperty findById(Long parseLong);
	public List<AppProperty> findByDescriptionStartingWith(String description);
}
