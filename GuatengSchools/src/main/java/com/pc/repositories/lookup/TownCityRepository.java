package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.TownCity;

@Repository
public interface TownCityRepository extends JpaRepository<TownCity, Integer> 
{
	
	public TownCity findById(Long parseLong);
	public List<TownCity> findByDescriptionStartingWith(String description);
	public List<TownCity> findByDescription(String description);
}
