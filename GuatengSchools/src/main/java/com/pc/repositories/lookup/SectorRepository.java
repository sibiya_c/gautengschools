package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Sector;

@Repository
public interface SectorRepository extends JpaRepository<Sector, Integer> 
{
	
	public Sector findById(Long parseLong);
	public List<Sector> findByDescriptionStartingWith(String description);
	public List<Sector> findByDescription(String description);
}
