package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Specialisation;

@Repository
public interface SpecialisationRepository extends JpaRepository<Specialisation, Integer> 
{
	
	public Specialisation findById(Long parseLong);
	public List<Specialisation> findByDescriptionStartingWith(String description);
	public List<Specialisation> findByDescription(String description);
}
