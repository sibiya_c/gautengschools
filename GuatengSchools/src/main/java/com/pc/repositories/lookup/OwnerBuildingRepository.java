package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.OwnerBuilding;

@Repository
public interface OwnerBuildingRepository extends JpaRepository<OwnerBuilding, Integer> 
{
	
	public OwnerBuilding findById(Long parseLong);
	public List<OwnerBuilding> findByDescriptionStartingWith(String description);
	public List<OwnerBuilding> findByDescription(String description);
}
