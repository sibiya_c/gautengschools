package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Suburb;

@Repository
public interface SuburbRepository extends JpaRepository<Suburb, Integer> 
{
	
	public Suburb findById(Long parseLong);
	public List<Suburb> findByDescriptionStartingWith(String description);
	public List<Suburb> findByDescription(String description);
}
