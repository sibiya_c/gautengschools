package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.PhasePED;

@Repository
public interface PhasePEDRepository extends JpaRepository<PhasePED, Integer> 
{
	
	public PhasePED findById(Long parseLong);
	public List<PhasePED> findByDescriptionStartingWith(String description);
	public List<PhasePED> findByDescription(String description);
}
