package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Province;

@Repository
public interface ProvinceRepository extends JpaRepository<Province, Integer> 
{
	
	public Province findById(Long parseLong);
	public List<Province> findByDescriptionStartingWith(String description);
	public List<Province> findByDescription(String description);
	public Province findByCode(String code);
}
