package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Addreessee;

@Repository
public interface AddreesseeRepository extends JpaRepository<Addreessee, Integer> 
{
	
	public Addreessee findById(Long parseLong);
	public List<Addreessee> findByDescriptionStartingWith(String description);
	public List<Addreessee> findByDescription(String description);
}
