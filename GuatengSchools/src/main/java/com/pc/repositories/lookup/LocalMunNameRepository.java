package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.LocalMunName;

@Repository
public interface LocalMunNameRepository extends JpaRepository<LocalMunName, Integer> 
{
	
	public LocalMunName findById(Long parseLong);
	public List<LocalMunName> findByDescriptionStartingWith(String description);
	public List<LocalMunName> findByDescription(String description);
}
