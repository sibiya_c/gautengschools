package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Circuit;

@Repository
public interface CircuitRepository extends JpaRepository<Circuit, Integer> 
{
	
	public Circuit findById(Long parseLong);
	public List<Circuit> findByDescriptionStartingWith(String description);
	public List<Circuit> findByDescription(String description);
}
