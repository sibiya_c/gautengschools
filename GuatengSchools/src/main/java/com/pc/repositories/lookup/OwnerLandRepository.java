package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.OwnerLand;

@Repository
public interface OwnerLandRepository extends JpaRepository<OwnerLand, Integer> 
{
	
	public OwnerLand findById(Long parseLong);
	public List<OwnerLand> findByDescriptionStartingWith(String description);
	public List<OwnerLand> findByDescription(String description);
}
