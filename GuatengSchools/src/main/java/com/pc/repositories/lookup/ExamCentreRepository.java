package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.ExamCentre;

@Repository
public interface ExamCentreRepository extends JpaRepository<ExamCentre, Integer> 
{
	
	public ExamCentre findById(Long parseLong);
	public List<ExamCentre> findByDescriptionStartingWith(String description);
	public List<ExamCentre> findByDescription(String description);
}
