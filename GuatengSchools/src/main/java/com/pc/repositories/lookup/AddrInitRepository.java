package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.AddrInit;

@Repository
public interface AddrInitRepository extends JpaRepository<AddrInit, Integer> 
{
	
	public AddrInit findById(Long parseLong);
	public List<AddrInit> findByDescriptionStartingWith(String description);
	public List<AddrInit> findByDescription(String description);
}
