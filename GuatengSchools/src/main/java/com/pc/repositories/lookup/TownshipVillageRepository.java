package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.TownshipVillage;

@Repository
public interface TownshipVillageRepository extends JpaRepository<TownshipVillage, Integer> 
{
	
	public TownshipVillage findById(Long parseLong);
	public List<TownshipVillage> findByDescriptionStartingWith(String description);
	public List<TownshipVillage> findByDescription(String description);
}
