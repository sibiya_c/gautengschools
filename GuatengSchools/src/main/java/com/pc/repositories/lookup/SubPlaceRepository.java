package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.SubPlace;

@Repository
public interface SubPlaceRepository extends JpaRepository<SubPlace, Integer> 
{
	
	public SubPlace findById(Long parseLong);
	public List<SubPlace> findByDescriptionStartingWith(String description);
	public List<SubPlace> findByDescription(String description);
}
