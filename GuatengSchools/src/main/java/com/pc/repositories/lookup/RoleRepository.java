package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> 
{
	
	public Role findById(Long parseLong);
	public List<Role> findByDescriptionStartingWith(String description);
	public Role findByCode(String code);
}
