package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.District;

@Repository
public interface DistrictRepository extends JpaRepository<District, Integer> 
{
	
	public District findById(Long parseLong);
	public List<District> findByDescriptionStartingWith(String description);
	public List<District> findByDescription(String description);
}
