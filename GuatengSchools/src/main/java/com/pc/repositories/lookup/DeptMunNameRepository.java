package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.DeptMunName;

@Repository
public interface DeptMunNameRepository extends JpaRepository<DeptMunName, Integer> 
{
	
	public DeptMunName findById(Long parseLong);
	public List<DeptMunName> findByDescriptionStartingWith(String description);
	public List<DeptMunName> findByDescription(String description);
}
