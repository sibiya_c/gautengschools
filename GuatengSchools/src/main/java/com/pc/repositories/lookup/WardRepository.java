package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Ward;

@Repository
public interface WardRepository extends JpaRepository<Ward, Integer> 
{
	
	public Ward findById(Long parseLong);
	public List<Ward> findByDescriptionStartingWith(String description);
	public List<Ward> findByDescription(String description);
}
