package com.pc.repositories.lookup;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.lookup.Region;

@Repository
public interface RegionRepository extends JpaRepository<Region, Integer> 
{
	
	public Region findById(Long parseLong);
	public List<Region> findByDescriptionStartingWith(String description);
	public List<Region> findByDescription(String description);
}
