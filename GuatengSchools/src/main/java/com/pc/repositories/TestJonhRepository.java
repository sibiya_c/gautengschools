package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.TestJonh;

@Repository
public interface TestJonhRepository extends JpaRepository<TestJonh, Integer> 
{
	
	public TestJonh findById(Long parseLong);
	//public List<TestJonh> findByDescriptionStartingWith(String description);
}
