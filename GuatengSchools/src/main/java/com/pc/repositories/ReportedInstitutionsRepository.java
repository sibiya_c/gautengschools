package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.ReportedInstitutions;
import com.pc.entities.User;
import com.pc.entities.enums.ReportedInstitutionsStatus;

@Repository
public interface ReportedInstitutionsRepository extends JpaRepository<ReportedInstitutions, Integer> 
{
	
	public ReportedInstitutions findById(Long parseLong);
	//public List<ReportedInstitutions> findByDescriptionStartingWith(String description);
	public long countByInvestigator(User investigator);
	
	public ReportedInstitutions findByRefNumber(String refNumber);
	
	public long countByStatus(ReportedInstitutionsStatus status);
}
