package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.GautengSchoolImport;

@Repository
public interface GautengSchoolImportRepository extends JpaRepository<GautengSchoolImport, Integer> 
{
	
	public GautengSchoolImport findById(Long parseLong);
	//public List<GautengSchoolImport> findByDescriptionStartingWith(String description);
}
