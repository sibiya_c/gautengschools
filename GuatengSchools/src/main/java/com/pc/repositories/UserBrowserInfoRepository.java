package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.UserBrowserInfo;

@Repository
public interface UserBrowserInfoRepository extends JpaRepository<UserBrowserInfo, Integer> 
{
	
	public UserBrowserInfo findById(Long parseLong);
	//public List<UserBrowserInfo> findByDescriptionStartingWith(String description);
}
