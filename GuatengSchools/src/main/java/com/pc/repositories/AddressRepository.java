package com.pc.repositories;


import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pc.entities.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer> 
{
	
	public Address findById(Long parseLong);
	//public List<Address> findByDescriptionStartingWith(String description);
}
