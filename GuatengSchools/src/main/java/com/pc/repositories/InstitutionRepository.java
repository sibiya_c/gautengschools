package com.pc.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.pc.entities.Institution;
import com.pc.entities.enums.InstStatusEnum;

@Repository
public interface InstitutionRepository extends JpaRepository<Institution, Integer> 
{
	
	public Institution findById(Long parseLong);
	public List<Institution> findByOfficialInstitutionNameStartingWith(String description);
	public long countByInstStatusEnum(InstStatusEnum instStatus);
	
}
