package com.pc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.pc.entities.enums.ReportedInstitutionsStatus;
import com.pc.entities.lookup.TownCity;
import com.pc.framework.IDataEntity;

@Entity
@Table(name = "reported_institutions")
@Audited
@AuditTable(value = "reported_institutions_hist")
public class ReportedInstitutions implements IDataEntity {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;
	
	@Column(name = "last_update_date", length = 19)
	private Date lastUpdateDate;
	
	@Column(name = "institution_name")
	private String institutionName;
	
	@Column(name = "known_as")
	private String knownAs;
	
	@Column(name = "ref_number")
	private String refNumber;
	
	@Column(name = "investigator_feedback",length=5000)
	private String investigatorFeedback;
	
	@Enumerated
	@Column(name = "status")
	private ReportedInstitutionsStatus status;
	
	@Column(name = "additional_info",length=5000)
	private String additionalInfo;
	
	/**
	 * Reporter's Details
	 * */
	
	@Column(name = "reporter_name")
	private String reporterName;
	
	@Column(name = "reporter_surname")
	private String reporterSurname;
	
	@Column(name = "reporter_email")
	private String reporterEmail;
	
	@Column(name = "reporter_cell")
	private String reporterCell;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "investigator_id", nullable=true)
	private User investigator;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="last_update_user")
	private User lastUpdateUser;
	
	/**
	 * Address Details
	 * */
	@Column(name = "address_line_1", nullable = false)
	private String addressLine1;
	
	@Column(name = "address_line_2", nullable = false)
	private String addressLine2;
	
	@Column(name = "address_line_3", nullable = true)
	private String addressLine3;
	
	@Column(name = "address_line_4", nullable = true)
	private String addressLine4;
	
	@Column(name = "post_code")
	private String postCode;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="town_city_id")
	private TownCity townCityLookup;
	
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		ReportedInstitutions other = (ReportedInstitutions) obj;
		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;
		return true;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the institutionName
	 */
	public String getInstitutionName() {
		return institutionName;
	}

	/**
	 * @param institutionName the institutionName to set
	 */
	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	

	/**
	 * @return the refNumber
	 */
	public String getRefNumber() {
		return refNumber;
	}

	/**
	 * @param refNumber the refNumber to set
	 */
	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}

	/**
	 * @return the status
	 */
	public ReportedInstitutionsStatus getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(ReportedInstitutionsStatus status) {
		this.status = status;
	}

	/**
	 * @return the investigatorFeedback
	 */
	public String getInvestigatorFeedback() {
		return investigatorFeedback;
	}

	/**
	 * @param investigatorFeedback the investigatorFeedback to set
	 */
	public void setInvestigatorFeedback(String investigatorFeedback) {
		this.investigatorFeedback = investigatorFeedback;
	}

	public String getReporterName() {
		return reporterName;
	}

	public void setReporterName(String reporterName) {
		this.reporterName = reporterName;
	}

	public String getReporterSurname() {
		return reporterSurname;
	}

	public void setReporterSurname(String reporterSurname) {
		this.reporterSurname = reporterSurname;
	}

	public String getReporterEmail() {
		return reporterEmail;
	}

	public void setReporterEmail(String reporterEmail) {
		this.reporterEmail = reporterEmail;
	}

	public String getReporterCell() {
		return reporterCell;
	}

	public void setReporterCell(String reporterCell) {
		this.reporterCell = reporterCell;
	}

	public String getKnownAs() {
		return knownAs;
	}

	public void setKnownAs(String knownAs) {
		this.knownAs = knownAs;
	}

	public User getInvestigator() {
		return investigator;
	}

	public void setInvestigator(User investigator) {
		this.investigator = investigator;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public User getLastUpdateUser() {
		return lastUpdateUser;
	}

	public void setLastUpdateUser(User lastUpdateUser) {
		this.lastUpdateUser = lastUpdateUser;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getAddressLine3() {
		return addressLine3;
	}

	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}

	public String getAddressLine4() {
		return addressLine4;
	}

	public void setAddressLine4(String addressLine4) {
		this.addressLine4 = addressLine4;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public TownCity getTownCityLookup() {
		return townCityLookup;
	}

	public void setTownCityLookup(TownCity townCityLookup) {
		this.townCityLookup = townCityLookup;
	}
	
	



}