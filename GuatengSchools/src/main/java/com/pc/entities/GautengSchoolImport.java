package com.pc.entities;

import com.pc.annotations.CSVAnnotation;
import com.pc.framework.IDataEntity;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "gauteng_school_import")
@Audited
@AuditTable(value = "gauteng_school_import_hist")
public class GautengSchoolImport implements IDataEntity {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;
	
	@Column(name = "last_update_date", length = 19)
	private Date lastUpdateDate;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="last_update_user")
	private User lastUpdateUser;

	@CSVAnnotation(name = "NatEmis", className = String.class)
	@Column(name = "nat_emis")
	private String natEmis;

	@CSVAnnotation(name = "Datayear", className = String.class)
	@Column(name = "data_year")
	private String dataYear;

	@CSVAnnotation(name = "ProvinceCD", className = String.class)
	@Column(name = "province_cd")
	private String provinceCD;

	@CSVAnnotation(name = "Province", className = String.class)
	@Column(name = "province")
	private String province;

	@CSVAnnotation(name = "Official_Institution_Name", className = String.class)
	@Column(name = "official_institution_name")
	private String officialInstitutionName;

	@CSVAnnotation(name = "Status", className = String.class)
	@Column(name = "status")
	private String status;

	@CSVAnnotation(name = "Sector", className = String.class)
	@Column(name = "sector")
	private String sector;

	@CSVAnnotation(name = "Type_DoE", className = String.class)
	@Column(name = "type_doe")
	private String typeDoE;

	@CSVAnnotation(name = "Phase_PED", className = String.class)
	@Column(name = "phase_ped")
	private String phasePED;

	@CSVAnnotation(name = "Specialization", className = String.class)
	@Column(name = "specialization")
	private String specialization;

	@CSVAnnotation(name = "OwnerLand", className = String.class)
	@Column(name = "owner_land")
	private String ownerLand;

	@CSVAnnotation(name = "OwnerBuildings", className = String.class)
	@Column(name = "owner_buildings")
	private String ownerBuildings;

	@CSVAnnotation(name = "ExDept", className = String.class)
	@Column(name = "ex_dept")
	private String exDept;

	@CSVAnnotation(name = "PaypointNo", className = String.class)
	@Column(name = "pay_point_no")
	private String payPointNo;

	@CSVAnnotation(name = "ComponentNo", className = String.class)
	@Column(name = "component_no")
	private String componentNo;

	@CSVAnnotation(name = "ExamNo", className = String.class)
	@Column(name = "exam_no")
	private String examNo;

	@CSVAnnotation(name = "ExamCentre", className = String.class)
	@Column(name = "exam_centre")
	private String examCentre;

	@CSVAnnotation(name = "GIS_Longitude", className = String.class)
	@Column(name = "gis_longitude")
	private String gisLongitude;

	@CSVAnnotation(name = "GIS_Latitude", className = String.class)
	@Column(name = "gis_latitude")
	private String gisLatitude;

	@CSVAnnotation(name = "GISSource", className = String.class)
	@Column(name = "gis_source")
	private String gisSource;

	@CSVAnnotation(name = "DMunName", className = String.class)
	@Column(name = "d_mun_name")
	private String dMunName;

	@CSVAnnotation(name = "LMunName", className = String.class)
	@Column(name = "l_mun_name")
	private String lMunName;

	@CSVAnnotation(name = "Ward_ID", className = String.class)
	@Column(name = "ward_id")
	private String wardId;

	@CSVAnnotation(name = "SP_Code", className = String.class)
	@Column(name = "sp_code")
	private String spCode;

	@CSVAnnotation(name = "SP_Name", className = String.class)
	@Column(name = "sp_name")
	private String spName;

	@CSVAnnotation(name = "EIRegion", className = String.class)
	@Column(name = "ei_region")
	private String eiRegion;

	@CSVAnnotation(name = "EIDistrict", className = String.class)
	@Column(name = "ei_district")
	private String eiDistrict;

	@CSVAnnotation(name = "EICircuit", className = String.class)
	@Column(name = "ei_circuit")
	private String eiCircuit;

	@CSVAnnotation(name = "AddrInit", className = String.class)
	@Column(name = "addr_init")
	private String addrInit;

	@CSVAnnotation(name = "Addressee", className = String.class)
	@Column(name = "addressee")
	private String addressee;

	@CSVAnnotation(name = "Township_Village", className = String.class)
	@Column(name = "township_village")
	private String townshipVillage;

	@CSVAnnotation(name = "Suburb", className = String.class)
	@Column(name = "suburb")
	private String suburb;

	@CSVAnnotation(name = "TownCity", className = String.class)
	@Column(name = "town_city")
	private String townCity;

	@CSVAnnotation(name = "StreetAddress", className = String.class)
	@Column(name = "street_address")
	private String streetAddress;

	@CSVAnnotation(name = "PostalAddress", className = String.class)
	@Column(name = "postal_address")
	private String postalAddress;

	@CSVAnnotation(name = "Telephone", className = String.class)
	@Column(name = "telephone")
	private String telephone;

	@CSVAnnotation(name = "Section21", className = String.class)
	@Column(name = "section21")
	private String section21;

	@CSVAnnotation(name = "Section21_Funct", className = String.class)
	@Column(name = "section21_funct")
	private String section21Funct;

	@CSVAnnotation(name = "Quintile", className = String.class)
	@Column(name = "quintile")
	private String quintile;

	@CSVAnnotation(name = "NAS", className = String.class)
	@Column(name = "nas")
	private String nas;

	@CSVAnnotation(name = "NodalArea", className = String.class)
	@Column(name = "nodal_area")
	private String nodalArea;

	@CSVAnnotation(name = "RegistrationDate", className = String.class)
	@Column(name = "registration_date")
	private String registrationDate;

	@CSVAnnotation(name = "NoFeeSchool", className = String.class)
	@Column(name = "no_fee_school")
	private String noFeeSchool;

	@CSVAnnotation(name = "Allocation", className = String.class)
	@Column(name = "allocation")
	private String allocation;

	@CSVAnnotation(name = "DemarcationFrom", className = String.class)
	@Column(name = "demarcation_from")
	private String demarcationFrom;

	@CSVAnnotation(name = "DemarcationTo", className = String.class)
	@Column(name = "demarcation_to")
	private String demarcationTo;

	@CSVAnnotation(name = "OldNATEMIS", className = String.class)
	@Column(name = "old_natemis")
	private String oldNATEMIS;

	@CSVAnnotation(name = "NewNATEMIS", className = String.class)
	@Column(name = "new_natemis")
	private String newNATEMIS;

	@CSVAnnotation(name = "Urban_Rural", className = String.class)
	@Column(name = "urban_rural")
	private String urbanRural;

	@CSVAnnotation(name = "Learners_2018", className = String.class)
	@Column(name = "learners_2018")
	private String learners2018;

	@CSVAnnotation(name = "Educator_2018", className = String.class)
	@Column(name = "educator_2018")
	private String educator2018;
	

	

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		GautengSchoolImport other = (GautengSchoolImport) obj;
		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;
		return true;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public User getLastUpdateUser() {
		return lastUpdateUser;
	}

	public void setLastUpdateUser(User lastUpdateUser) {
		this.lastUpdateUser = lastUpdateUser;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getNatEmis() {
		return natEmis;
	}

	public void setNatEmis(String natEmis) {
		this.natEmis = natEmis;
	}

	public String getDataYear() {
		return dataYear;
	}

	public void setDataYear(String dataYear) {
		this.dataYear = dataYear;
	}

	public String getProvinceCD() {
		return provinceCD;
	}

	public void setProvinceCD(String provinceCD) {
		this.provinceCD = provinceCD;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getOfficialInstitutionName() {
		return officialInstitutionName;
	}

	public void setOfficialInstitutionName(String officialInstitutionName) {
		this.officialInstitutionName = officialInstitutionName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getTypeDoE() {
		return typeDoE;
	}

	public void setTypeDoE(String typeDoE) {
		this.typeDoE = typeDoE;
	}

	public String getPhasePED() {
		return phasePED;
	}

	public void setPhasePED(String phasePED) {
		this.phasePED = phasePED;
	}

	public String getSpecialization() {
		return specialization;
	}

	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	public String getOwnerLand() {
		return ownerLand;
	}

	public void setOwnerLand(String ownerLand) {
		this.ownerLand = ownerLand;
	}

	public String getOwnerBuildings() {
		return ownerBuildings;
	}

	public void setOwnerBuildings(String ownerBuildings) {
		this.ownerBuildings = ownerBuildings;
	}

	public String getExDept() {
		return exDept;
	}

	public void setExDept(String exDept) {
		this.exDept = exDept;
	}

	public String getPayPointNo() {
		return payPointNo;
	}

	public void setPayPointNo(String payPointNo) {
		this.payPointNo = payPointNo;
	}

	public String getComponentNo() {
		return componentNo;
	}

	public void setComponentNo(String componentNo) {
		this.componentNo = componentNo;
	}

	public String getExamNo() {
		return examNo;
	}

	public void setExamNo(String examNo) {
		this.examNo = examNo;
	}

	public String getExamCentre() {
		return examCentre;
	}

	public void setExamCentre(String examCentre) {
		this.examCentre = examCentre;
	}

	public String getGisLongitude() {
		return gisLongitude;
	}

	public void setGisLongitude(String gisLongitude) {
		this.gisLongitude = gisLongitude;
	}

	public String getGisLatitude() {
		return gisLatitude;
	}

	public void setGisLatitude(String gisLatitude) {
		this.gisLatitude = gisLatitude;
	}

	public String getGisSource() {
		return gisSource;
	}

	public void setGisSource(String gisSource) {
		this.gisSource = gisSource;
	}

	public String getdMunName() {
		return dMunName;
	}

	public void setdMunName(String dMunName) {
		this.dMunName = dMunName;
	}

	public String getlMunName() {
		return lMunName;
	}

	public void setlMunName(String lMunName) {
		this.lMunName = lMunName;
	}

	public String getWardId() {
		return wardId;
	}

	public void setWardId(String wardId) {
		this.wardId = wardId;
	}

	public String getSpCode() {
		return spCode;
	}

	public void setSpCode(String spCode) {
		this.spCode = spCode;
	}

	public String getSpName() {
		return spName;
	}

	public void setSpName(String spName) {
		this.spName = spName;
	}

	public String getEiRegion() {
		return eiRegion;
	}

	public void setEiRegion(String eiRegion) {
		this.eiRegion = eiRegion;
	}

	public String getEiDistrict() {
		return eiDistrict;
	}

	public void setEiDistrict(String eiDistrict) {
		this.eiDistrict = eiDistrict;
	}

	public String getEiCircuit() {
		return eiCircuit;
	}

	public void setEiCircuit(String eiCircuit) {
		this.eiCircuit = eiCircuit;
	}

	public String getAddrInit() {
		return addrInit;
	}

	public void setAddrInit(String addrInit) {
		this.addrInit = addrInit;
	}

	public String getAddressee() {
		return addressee;
	}

	public void setAddressee(String addressee) {
		this.addressee = addressee;
	}

	public String getTownshipVillage() {
		return townshipVillage;
	}

	public void setTownshipVillage(String townshipVillage) {
		this.townshipVillage = townshipVillage;
	}

	public String getSuburb() {
		return suburb;
	}

	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	public String getTownCity() {
		return townCity;
	}

	public void setTownCity(String townCity) {
		this.townCity = townCity;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getPostalAddress() {
		return postalAddress;
	}

	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getSection21() {
		return section21;
	}

	public void setSection21(String section21) {
		this.section21 = section21;
	}

	public String getSection21Funct() {
		return section21Funct;
	}

	public void setSection21Funct(String section21Funct) {
		this.section21Funct = section21Funct;
	}

	public String getQuintile() {
		return quintile;
	}

	public void setQuintile(String quintile) {
		this.quintile = quintile;
	}

	public String getNas() {
		return nas;
	}

	public void setNas(String nas) {
		this.nas = nas;
	}

	public String getNodalArea() {
		return nodalArea;
	}

	public void setNodalArea(String nodalArea) {
		this.nodalArea = nodalArea;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getNoFeeSchool() {
		return noFeeSchool;
	}

	public void setNoFeeSchool(String noFeeSchool) {
		this.noFeeSchool = noFeeSchool;
	}

	public String getAllocation() {
		return allocation;
	}

	public void setAllocation(String allocation) {
		this.allocation = allocation;
	}

	public String getDemarcationFrom() {
		return demarcationFrom;
	}

	public void setDemarcationFrom(String demarcationFrom) {
		this.demarcationFrom = demarcationFrom;
	}

	public String getDemarcationTo() {
		return demarcationTo;
	}

	public void setDemarcationTo(String demarcationTo) {
		this.demarcationTo = demarcationTo;
	}

	public String getOldNATEMIS() {
		return oldNATEMIS;
	}

	public void setOldNATEMIS(String oldNATEMIS) {
		this.oldNATEMIS = oldNATEMIS;
	}

	public String getNewNATEMIS() {
		return newNATEMIS;
	}

	public void setNewNATEMIS(String newNATEMIS) {
		this.newNATEMIS = newNATEMIS;
	}

	public String getUrbanRural() {
		return urbanRural;
	}

	public void setUrbanRural(String urbanRural) {
		this.urbanRural = urbanRural;
	}

	public String getLearners2018() {
		return learners2018;
	}

	public void setLearners2018(String learners2018) {
		this.learners2018 = learners2018;
	}

	public String getEducator2018() {
		return educator2018;
	}

	public void setEducator2018(String educator2018) {
		this.educator2018 = educator2018;
	}

	@Override
	public String toString() {
		return "GautengSchoolImport [id=" + id + ", createDate=" + createDate + ", lastUpdateDate=" + lastUpdateDate
				+ ", lastUpdateUser=" + lastUpdateUser + ", natEmis=" + natEmis + ", dataYear=" + dataYear
				+ ", provinceCD=" + provinceCD + ", province=" + province + ", officialInstitutionName="
				+ officialInstitutionName + ", status=" + status + ", sector=" + sector + ", typeDoE=" + typeDoE
				+ ", phasePED=" + phasePED + ", specialization=" + specialization + ", ownerLand=" + ownerLand
				+ ", ownerBuildings=" + ownerBuildings + ", exDept=" + exDept + ", payPointNo=" + payPointNo
				+ ", componentNo=" + componentNo + ", examNo=" + examNo + ", examCentre=" + examCentre
				+ ", gisLongitude=" + gisLongitude + ", gisLatitude=" + gisLatitude + ", gisSource=" + gisSource
				+ ", dMunName=" + dMunName + ", lMunName=" + lMunName + ", wardId=" + wardId + ", spCode=" + spCode
				+ ", spName=" + spName + ", eiRegion=" + eiRegion + ", eiDistrict=" + eiDistrict + ", eiCircuit="
				+ eiCircuit + ", addrInit=" + addrInit + ", addressee=" + addressee + ", townshipVillage="
				+ townshipVillage + ", suburb=" + suburb + ", townCity=" + townCity + ", streetAddress=" + streetAddress
				+ ", postalAddress=" + postalAddress + ", telephone=" + telephone + ", section21=" + section21
				+ ", section21Funct=" + section21Funct + ", quintile=" + quintile + ", nas=" + nas + ", nodalArea="
				+ nodalArea + ", registrationDate=" + registrationDate + ", noFeeSchool=" + noFeeSchool
				+ ", allocation=" + allocation + ", demarcationFrom=" + demarcationFrom + ", demarcationTo="
				+ demarcationTo + ", oldNATEMIS=" + oldNATEMIS + ", newNATEMIS=" + newNATEMIS + ", urbanRural="
				+ urbanRural + ", learners2018=" + learners2018 + ", educator2018=" + educator2018 + "]";
	}
	
	
}