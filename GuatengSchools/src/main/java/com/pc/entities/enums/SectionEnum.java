package com.pc.entities.enums;

import com.pc.constants.AppConstants;
import com.pc.utils.EnumUtil;

public enum SectionEnum {

	Section21("21") {
		@Override
		public Long getYesNoLookupId() {
			return AppConstants.YES_ID;
		}
	},

	Section22("22") {
		@Override
		public Long getYesNoLookupId() {
			return AppConstants.NO_ID;
		}
	};

	/** The display name. */
	private String displayName;
	private Long id;

	/**
	 * Instantiates a new wsp type enum.
	 *
	 * @param displayNameX
	 *            the display name X
	 */
	private SectionEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/**
	 * Gets the friendly name.
	 *
	 * @return the friendly name
	 */
	public String getFriendlyName() {
		return toString();
	}

	/**
	 * Gets the registration name.
	 *
	 * @return the registration name
	 */
	public Long getYesNoLookupId() {
		return id;
	}

	/**
	 * Gets the id passport enum by value.
	 *
	 * @param value
	 *            the value
	 * @return the id passport enum by value
	 */
	public static final SectionEnum getIdPassportEnumByValue(int value) {
		for (SectionEnum status : SectionEnum.values()) {
			if (status.ordinal() == value) return status;
		}

		return null;
	}
	
	 public static SectionEnum fromString(String enumValue){
		 if(enumValue.equalsIgnoreCase("YES")) {
			 enumValue="21";
		 }
	    for(SectionEnum v : values()){
	        if( v.displayName.equalsIgnoreCase(enumValue)){
	            return v;
	        }
	    }
	    return null;
	 }
}
