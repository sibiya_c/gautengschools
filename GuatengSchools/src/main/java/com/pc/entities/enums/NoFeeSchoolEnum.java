package com.pc.entities.enums;

import com.pc.constants.AppConstants;
import com.pc.utils.EnumUtil;

public enum NoFeeSchoolEnum {

	Yes("Yes") {
		@Override
		public Long getYesNoLookupId() {
			return AppConstants.YES_ID;
		}
	},

	No("No") {
		@Override
		public Long getYesNoLookupId() {
			return AppConstants.NO_ID;
		}
	};

	/** The display name. */
	private String displayName;
	private Long id;

	/**
	 * Instantiates a new wsp type enum.
	 *
	 * @param displayNameX
	 *            the display name X
	 */
	private NoFeeSchoolEnum(String displayNameX) {
		displayName = displayNameX;
	}

	/**
	 * Gets the friendly name.
	 *
	 * @return the friendly name
	 */
	public String getFriendlyName() {
		return toString();
	}

	/**
	 * Gets the registration name.
	 *
	 * @return the registration name
	 */
	public Long getYesNoLookupId() {
		return id;
	}

	/**
	 * Gets the id passport enum by value.
	 *
	 * @param value
	 *            the value
	 * @return the id passport enum by value
	 */
	public static final NoFeeSchoolEnum getIdPassportEnumByValue(int value) {
		for (NoFeeSchoolEnum status : NoFeeSchoolEnum.values()) {
			if (status.ordinal() == value) return status;
		}

		return null;
	}
	
	 public static NoFeeSchoolEnum fromString(String enumValue){
		    for(NoFeeSchoolEnum v : values()){
		        if( v.displayName.equalsIgnoreCase(enumValue)){
		            return v;
		        }
		    }
		    return null;
	 }
}
