package com.pc.entities.enums;
import com.pc.utils.EnumUtil;

public enum InstStatusEnum {

  OPNE("OPEN"), 
  CLOSED("CLOSED");

  private String displayName;

  private InstStatusEnum(String displayNameX)
  {
    displayName = displayNameX;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Enum#toString()
   */
  @Override
  public String toString()
  {
    return displayName;
  }

  public String getFriendlyName()
  {
    return toString();
  }
  
  public static InstStatusEnum fromString(String enumValue){
	    for(InstStatusEnum v : values()){
	        if( v.displayName.equalsIgnoreCase(enumValue)){
	            return v;
	        }
	    }
	    return null;
  }
  
  
  
}
