package com.pc.entities.enums;

import com.pc.constants.AppConstants;
import com.pc.utils.EnumUtil;

// TODO: Auto-generated Javadoc
/**
 * The Enum WspTypeEnum.
 */
public enum UrbanRuralEnum {

	Urban("Urban"), 
	Rural("Rural");

	  private String displayName;

	  private UrbanRuralEnum(String displayNameX)
	  {
	    displayName = displayNameX;
	  }

	  /*
	   * (non-Javadoc)
	   * 
	   * @see java.lang.Enum#toString()
	   */
	  @Override
	  public String toString()
	  {
	    return displayName;
	  }

	  public String getFriendlyName()
	  {
	    return toString();
	  }
	  

	 public static UrbanRuralEnum fromString(String enumValue){
		    for(UrbanRuralEnum v : values()){
		        if( v.displayName.equalsIgnoreCase(enumValue)){
		            return v;
		        }
		    }
		    return null;
	 }
}
