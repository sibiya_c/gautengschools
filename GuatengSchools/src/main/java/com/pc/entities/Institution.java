package com.pc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;

import com.pc.entities.enums.InstStatusEnum;
import com.pc.entities.enums.NoFeeSchoolEnum;
import com.pc.entities.enums.SectionEnum;
import com.pc.entities.enums.UrbanRuralEnum;
import com.pc.entities.enums.YesNoEnum;
import com.pc.entities.lookup.AddrInit;
import com.pc.entities.lookup.Addreessee;
import com.pc.entities.lookup.Circuit;
import com.pc.entities.lookup.DeptMunName;
import com.pc.entities.lookup.District;
import com.pc.entities.lookup.ExDept;
import com.pc.entities.lookup.ExamCentre;
import com.pc.entities.lookup.LocalMunName;
import com.pc.entities.lookup.OwnerBuilding;
import com.pc.entities.lookup.OwnerLand;
import com.pc.entities.lookup.PhasePED;
import com.pc.entities.lookup.Province;
import com.pc.entities.lookup.Region;
import com.pc.entities.lookup.SubPlace;
import com.pc.entities.lookup.Sector;
import com.pc.entities.lookup.Specialisation;
import com.pc.entities.lookup.TownCity;
import com.pc.entities.lookup.TownshipVillage;
import com.pc.entities.lookup.TypeDoE;
import com.pc.entities.lookup.Ward;
import com.pc.framework.IDataEntity;

@Entity
@Table(name = "institution")
@Audited
@AuditTable(value = "institution_hist")
public class Institution implements IDataEntity {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", length = 19)
	private Date createDate;
	
	@Column(name = "last_update_date", length = 19)
	private Date lastUpdateDate;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="last_update_user")
	private User lastUpdateUser;
	
	@Column(name = "official_institution_name", length = 250)
	private String officialInstitutionName;
	
	@Column(name = "data_year", length = 250)
	private String dataYear;
	
	@Column(name = "nat_emis", length = 250)
	private String natEmis;

	@Enumerated
	@Column(name = "inst_status_enum")
	private InstStatusEnum instStatusEnum;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="province_id")
	private Province provinceLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="sector_id")
	private Sector sectorLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="type_doe_id")
	private TypeDoE typeDoELookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="phase_ped_id")
	private PhasePED phasePEDLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="specialisation_id")
	private Specialisation specialisationLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="owner_land_id")
	private OwnerLand ownerLandLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="owner_building_id")
	private OwnerBuilding ownerBuildingLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ex_dept_id")
	private ExDept exDeptLookup;
	
	@Column(name = "paypoint_no", length = 250)
	private String paypointNo;
	
	@Column(name = "componet_no", length = 250)
	private String componetNo;
	
	@Column(name = "exam_no", length = 250)
	private String examNo;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="exam_centre_id")
	private ExamCentre examCentreLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="address_id")
	private Address address;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="dept_mun_name_id")
	private DeptMunName deptMunNameLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="local_mun_name_id")
	private LocalMunName localMunNameLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="ward_id")
	private Ward wardLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="sub_place_id")
	private SubPlace subPlaceLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="region_id")
	private Region regionLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="district_id")
	private District districtLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="circuit_id")
	private Circuit circuitLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="addr_init_id")
	private AddrInit addrInitLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="addreessee_id")
	private Addreessee addreesseeLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="township_village_id")
	private TownshipVillage townshipVillageLookup;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="town_city_id")
	private TownCity townCityLookup;
	
	@Column(name = "tel_num", length = 15)
	private String telNum;
	
	@Enumerated
	@Column(name = "section")
	private SectionEnum section;
	
	@Column(name = "fection_21_funct", length = 250)
	private String section21Funct;
	
	@Column(name = "Quantile", length = 250)
	private String quantile;
	
	@Column(name = "nodal_area", length = 250)
	private String nodalArea;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "registration_date", length = 19)
	private Date registrationDate;
	
	@Enumerated
	@Column(name = "no_fee_school")
	private NoFeeSchoolEnum noFeeSchoolEnum;
	
	@Column(name = "allocation")
	private String allocation;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="demorcation_from_id")
	private Province femorcationFrom;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="demorcation_to_id")
	private Province femorcationTo;
	
	@Column(name = "old_natemis")
	private String oldNATEMIS;
	
	@Column(name = "new_natemis")
	private String newNATEMIS;

	@Enumerated
	@Column(name = "urban_rural")
	private UrbanRuralEnum urbanRuralEnum;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="learners_and_educators_id")
	private LearnersAndEducators learnersAndEducators;
	
	@Fetch(FetchMode.JOIN)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="gauteng_school_import_id")
	private GautengSchoolImport gautengSchoolImport;
	
	
	
	

	

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Institution other = (Institution) obj;
		if (id == null) {
			if (other.id != null) return false;
		} else if (!id.equals(other.id)) return false;
		return true;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public User getLastUpdateUser() {
		return lastUpdateUser;
	}

	public void setLastUpdateUser(User lastUpdateUser) {
		this.lastUpdateUser = lastUpdateUser;
	}

	public String getOfficialInstitutionName() {
		return officialInstitutionName;
	}

	public void setOfficialInstitutionName(String officialInstitutionName) {
		this.officialInstitutionName = officialInstitutionName;
	}

	public String getDataYear() {
		return dataYear;
	}

	public void setDataYear(String dataYear) {
		this.dataYear = dataYear;
	}

	public String getNatEmis() {
		return natEmis;
	}

	public void setNatEmis(String natEmis) {
		this.natEmis = natEmis;
	}

	public InstStatusEnum getInstStatusEnum() {
		return instStatusEnum;
	}

	public void setInstStatusEnum(InstStatusEnum instStatusEnum) {
		this.instStatusEnum = instStatusEnum;
	}

	public Province getProvinceLookup() {
		return provinceLookup;
	}

	public void setProvinceLookup(Province provinceLookup) {
		this.provinceLookup = provinceLookup;
	}

	public Sector getSectorLookup() {
		return sectorLookup;
	}

	public void setSectorLookup(Sector sectorLookup) {
		this.sectorLookup = sectorLookup;
	}

	public TypeDoE getTypeDoELookup() {
		return typeDoELookup;
	}

	public void setTypeDoELookup(TypeDoE typeDoELookup) {
		this.typeDoELookup = typeDoELookup;
	}

	public PhasePED getPhasePEDLookup() {
		return phasePEDLookup;
	}

	public void setPhasePEDLookup(PhasePED phasePEDLookup) {
		this.phasePEDLookup = phasePEDLookup;
	}

	public Specialisation getSpecialisationLookup() {
		return specialisationLookup;
	}

	public void setSpecialisationLookup(Specialisation specialisationLookup) {
		this.specialisationLookup = specialisationLookup;
	}

	public OwnerLand getOwnerLandLookup() {
		return ownerLandLookup;
	}

	public void setOwnerLandLookup(OwnerLand ownerLandLookup) {
		this.ownerLandLookup = ownerLandLookup;
	}

	public OwnerBuilding getOwnerBuildingLookup() {
		return ownerBuildingLookup;
	}

	public void setOwnerBuildingLookup(OwnerBuilding ownerBuildingLookup) {
		this.ownerBuildingLookup = ownerBuildingLookup;
	}

	public ExDept getExDeptLookup() {
		return exDeptLookup;
	}

	public void setExDeptLookup(ExDept exDeptLookup) {
		this.exDeptLookup = exDeptLookup;
	}

	public String getPaypointNo() {
		return paypointNo;
	}

	public void setPaypointNo(String paypointNo) {
		this.paypointNo = paypointNo;
	}

	public String getComponetNo() {
		return componetNo;
	}

	public void setComponetNo(String componetNo) {
		this.componetNo = componetNo;
	}

	public String getExamNo() {
		return examNo;
	}

	public void setExamNo(String examNo) {
		this.examNo = examNo;
	}

	public ExamCentre getExamCentreLookup() {
		return examCentreLookup;
	}

	public void setExamCentreLookup(ExamCentre examCentreLookup) {
		this.examCentreLookup = examCentreLookup;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}


	public Ward getWardLookup() {
		return wardLookup;
	}

	public void setWardLookup(Ward wardLookup) {
		this.wardLookup = wardLookup;
	}


	public Region getRegionLookup() {
		return regionLookup;
	}

	public void setRegionLookup(Region regionLookup) {
		this.regionLookup = regionLookup;
	}

	public District getDistrictLookup() {
		return districtLookup;
	}

	public void setDistrictLookup(District districtLookup) {
		this.districtLookup = districtLookup;
	}

	public Circuit getCircuitLookup() {
		return circuitLookup;
	}

	public void setCircuitLookup(Circuit circuitLookup) {
		this.circuitLookup = circuitLookup;
	}

	public AddrInit getAddrInitLookup() {
		return addrInitLookup;
	}

	public void setAddrInitLookup(AddrInit addrInitLookup) {
		this.addrInitLookup = addrInitLookup;
	}

	public Addreessee getAddreesseeLookup() {
		return addreesseeLookup;
	}

	public void setAddreesseeLookup(Addreessee addreesseeLookup) {
		this.addreesseeLookup = addreesseeLookup;
	}

	public TownshipVillage getTownshipVillageLookup() {
		return townshipVillageLookup;
	}

	public void setTownshipVillageLookup(TownshipVillage townshipVillageLookup) {
		this.townshipVillageLookup = townshipVillageLookup;
	}

	public TownCity getTownCityLookup() {
		return townCityLookup;
	}

	public void setTownCityLookup(TownCity townCityLookup) {
		this.townCityLookup = townCityLookup;
	}

	public String getTelNum() {
		return telNum;
	}

	public void setTelNum(String telNum) {
		this.telNum = telNum;
	}

	public SectionEnum getSection() {
		return section;
	}

	public void setSection(SectionEnum section) {
		this.section = section;
	}

	public String getSection21Funct() {
		return section21Funct;
	}

	public void setSection21Funct(String section21Funct) {
		this.section21Funct = section21Funct;
	}

	public String getQuantile() {
		return quantile;
	}

	public void setQuantile(String quantile) {
		this.quantile = quantile;
	}

	public String getNodalArea() {
		return nodalArea;
	}

	public void setNodalArea(String nodalArea) {
		this.nodalArea = nodalArea;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public NoFeeSchoolEnum getNoFeeSchoolEnum() {
		return noFeeSchoolEnum;
	}

	public void setNoFeeSchoolEnum(NoFeeSchoolEnum noFeeSchoolEnum) {
		this.noFeeSchoolEnum = noFeeSchoolEnum;
	}

	public String getAllocation() {
		return allocation;
	}

	public void setAllocation(String allocation) {
		this.allocation = allocation;
	}

	public Province getFemorcationFrom() {
		return femorcationFrom;
	}

	public void setFemorcationFrom(Province femorcationFrom) {
		this.femorcationFrom = femorcationFrom;
	}

	public Province getFemorcationTo() {
		return femorcationTo;
	}

	public void setFemorcationTo(Province femorcationTo) {
		this.femorcationTo = femorcationTo;
	}

	public String getOldNATEMIS() {
		return oldNATEMIS;
	}

	public void setOldNATEMIS(String oldNATEMIS) {
		this.oldNATEMIS = oldNATEMIS;
	}

	public String getNewNATEMIS() {
		return newNATEMIS;
	}

	public void setNewNATEMIS(String newNATEMIS) {
		this.newNATEMIS = newNATEMIS;
	}

	public UrbanRuralEnum getUrbanRuralEnum() {
		return urbanRuralEnum;
	}

	public void setUrbanRuralEnum(UrbanRuralEnum urbanRuralEnum) {
		this.urbanRuralEnum = urbanRuralEnum;
	}

	

	public DeptMunName getDeptMunNameLookup() {
		return deptMunNameLookup;
	}

	public void setDeptMunNameLookup(DeptMunName deptMunNameLookup) {
		this.deptMunNameLookup = deptMunNameLookup;
	}

	public LocalMunName getLocalMunNameLookup() {
		return localMunNameLookup;
	}

	public void setLocalMunNameLookup(LocalMunName localMunNameLookup) {
		this.localMunNameLookup = localMunNameLookup;
	}

	public SubPlace getSubPlaceLookup() {
		return subPlaceLookup;
	}

	public void setSubPlaceLookup(SubPlace subPlaceLookup) {
		this.subPlaceLookup = subPlaceLookup;
	}

	public LearnersAndEducators getLearnersAndEducators() {
		return learnersAndEducators;
	}

	public void setLearnersAndEducators(LearnersAndEducators learnersAndEducators) {
		this.learnersAndEducators = learnersAndEducators;
	}

	public GautengSchoolImport getGautengSchoolImport() {
		return gautengSchoolImport;
	}

	public void setGautengSchoolImport(GautengSchoolImport gautengSchoolImport) {
		this.gautengSchoolImport = gautengSchoolImport;
	}
	
	
	


}